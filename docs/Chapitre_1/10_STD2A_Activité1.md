---
title : "Activité"
correctionvisible : True
pdf : "activite1"
---


<div class="noprint" style="text-align:right;width:100%;background-color:white">
    <a href="../pdf/{{page.meta.pdf}}.pdf" 
    style="display:inline-block" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="#a3a3a3" stroke-width="1.5" stroke-linecap="butt" stroke-linejoin="arcs"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg>
    </a>
    <button id="bt_test">
        <img src="../../assets_extra/images/cravateseule.svg" width="30"/>
    </button>
</div>


<div id="masque"  style="display:none;text-align:center;font-size: 25px;font-weight: bold;">
    <a href="../pages_cache/enavance/">Page cachée</a>
</div>

<script>
    let bt1 = document.getElementById("bt_test");
    let d2 = document.getElementById("masque");


    function identification(){
        if (d2.style.display!= "block"){
            var reponse = prompt("Es-tu réellement un porteur de cravate ?","<saisir un identifiant>");
            if(reponse==45){
                d2.style.display = "block";
            }
        }
    };

    bt1.onclick = identification;
</script>






# Activité 1 : Les éléments constitutifs d’un appareil photographique



### **Document 1**
<center>
![doc 1](images/activite1/doc1.png){ width="900" loading=lazy}
</center>


### **Document 2**
<center>
![doc 2](images/activite1/doc2.png){ width="900" loading=lazy}
</center>

<center>
![doc 2 suite](images/activite1/doc2_suite.png){ width="900" loading=lazy}
</center>


### **Document 3**
<center>
![doc 3](images/activite1/doc3.png){ width="900" loading=lazy}
</center>


### **Questions**

!!! faq "Questions"
    À partir des documents précédents répondre aux questions suivantes :

    1. Quels sont les composants en commun entre les appareils photographiques numérique et argentique ?
    2. Quel est le rôle de chacun de ces éléments ?
    3. Quelle est la principale différence entre les appareils photographiques numérique et argentique ?
    4. Décrire l’acquisition d'une photographie pour chacun de ces appareils.



??? faq correction "Correction"
    1. 
    *à faire*
    <br>
    2. Les appareils photographiques ont généralement en commun les éléments suivants :
        <ol>
            <li>une surface photosensible (plaque de verre, plaque de métal, papier, pellicule ou capteur numérique)</li>
            <li>un objectif</li>
            <li>un diaphragme</li>
            <li>un obturateur (remplacer par la technique du chapeau ou du bouchon sur les très vieux objectifs)</li>
        </ol>
    <br>
    3. Chacun de ces éléments joue un roles bien déterminé dans la construction d'une photographie.</br>
        <ol>
            <li>**Une surface photosensible :** </li>
            <li>**L'objectif :**</li>
            <li>**Le diaphragme :** permet de régler la quantité de lumière entrant dans l'appareil.</li>
            <li>**l’obturateur :** permet de régler la durée pendant laquelle la lumière vas pouvoir impressionner la surface photosensible.</li>
        </ol>




   



<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>