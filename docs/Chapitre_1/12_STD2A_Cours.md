---
title : "Cours"
pdf : 
---


<div class="noprint" style="text-align:right;width:100%;background-color:white">
    <a href="../pdf/{{page.meta.pdf}}.pdf" 
    style="display:inline-block" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="#a3a3a3" stroke-width="1.5" stroke-linecap="butt" stroke-linejoin="arcs"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg>
    </a>
</div>




# Chapitre 1 : Appareils photographiques




??? info "Dans le programme"
    **Thème 2** : Voir et faire voir des objets

    | **Notions et Contenus**                | **Capacités exigibles**                            |   
    | -------------------------------------- | -------------------------------------------------- |
    | L’appareil photographique              | - Compléter la légende du schéma d’un appareil photographique à visée « reflex » (objectif, diaphragme, miroir, prisme, obturateur, capteur) 
    |      |- Comparer le modèle de l’œil réduit avec le modèle de l’appareil photographique|
    |      |- Extraire et exploiter des informations sur la photographie numérique et la photographie argentique.|
  



<br>

## 1. Les appareils photographiques
### 1.1 Quelques appareils photographiques

<center>
    <div class="grand noprint">
        <figure>
            <img src="../images/cours_appareil/appareils/camera.jpg" />
            <img src="../images/cours_appareil/appareils/Camera4.jpg" />
            <figcaption>Appareils photographiques dans l'histoire</figcaption>
        </figure>
    </div>
</center>

Quelques soit l'appareil photo, on retrouve dans sa constitutions les éléments essentiel à son fonctionnement. Mais quels sont ces éléments ?


### 1.2 Les éléments essentiels d'un appareil photograpique
<center>
    <div class="grand">
        <figure>
            <img src="../images/cours_appareil/appareils/appareil_photo.png" />
            <figcaption>Manuel numérique hilbrt p140</figcaption>
        </figure>
    </div>
</center>

<br/>

#### A. Objectif
C'est un système optique composé de lentilles et d'un diaphragme.<br/>
Un objectif est caractérisé par sa distance focale, son ouverture maximale et l'angle de champs maximal qu'il est capable de couvrir.

<br/>

<center>
![Image title](../images/cours_appareil/objectif/cut-lens.jpg){class="petit" loading=lazy }
![Image title](../images/cours_appareil/objectif/ouverture-explication-940x448.jpg){class="grand" loading=lazy }
</center>

<br/>


#### B. Diaphragme
Le diaphragme conditionne la quantité de lumière transmise par son ouverture. Plus celle-ci sera importante, plus la quantité de lumière transmise sera importante.<br>


<center>
![Image title](../images/cours_appareil/ouverture/360_F_246628538_rbBI1Zi0WsDhNj01PXlldnddKmsdPPi7.jpg){class="petit" loading=lazy }
![Image title](../images/cours_appareil/ouverture/500px-Blende.jpg){class="grand" loading=lazy }
</center>

<br/>


#### C. Obturateur
Il masque la surface photosensible, contrôlant le temps d'exposition de ce dernier et donc la quantité de lumière reçu.<br>


<center>
![Image title](../images/cours_appareil/opturateur/page16pm.jpg){class="petit" loading=lazy }
![Image title](../images/cours_appareil/opturateur/obturateur-lent-940x533.jpg){class="petit" loading=lazy }
![Image title](../images/cours_appareil/opturateur/obturateur-rapide-940x533.jpg){class="petit" loading=lazy }
</center>




??? note "Autres liens" 

    - [phototrend](https://phototrend.fr/2016/03/mp-170-lobturateur-photo-mecanique-electronique/)

    - [les numériques](https://www.lesnumeriques.com/photo/comprendre-l-obturation-electronique-pu120475.html)

    - [vivre de la photo](http://vivre-de-la-photo.fr/que-choisir-entre-obturateur-mecanique-et-electronique/)

    <center>
    ![Image title](../images/cours_appareil/opturateur/3_Obturateurs.jpg){width="800" loading=lazy }
    <figcaption>Autres type d'obturateur</figcaption>
    </center>


<br>

#### D. Surface photosensible
Dans les temps ancien, on utilisé des plaques de verre ou de métal sur lesquelles une émulsion photosensible était appliquée.<br>
Un peu plus tard, la surface photosensible à été appliquée sur un support de triacétate de cellulose, la pellicule photographique.<br>


<center>
![Image title](../images/cours_appareil/surface_photosensible/developper-ses-photos.jpg){class="petit" loading=lazy }
![Image title](../images/cours_appareil/surface_photosensible/couches-film-noir-et-blanc.jpg){class="moyen" loading=lazy }
<figcaption> Pellicule photo</figcaption>
</center>


<br>


De nos jours, les appareils photographiques utilisent un capteur.<br>
Le capteur est composé d'une matrice de cellules photoélectriques qui convertissent l'énergie électromagnétique de la lumière en un signal électrique qui est ensuite traité pour obtenir une image.<br>

<center>
![Image title](../images/cours_appareil/surface_photosensible/2-fnx-snt-c19-img03.png){class="grand" loading=lazy }
![Image title](../images/cours_appareil/surface_photosensible/2-fnx-snt-c19-img02.png){class="grand" loading=lazy }
<figcaption> Capteur photo</figcaption>
</center>


<br>

#### E. Éléments propre aux appareils photos réflexes

*Miroir :* baissé, il redirige les rayons lumineux vers le viseur par l'intermédiaire du prisme; levé il laisse passer les rayons lumineux directement vers le capteur. Ainsi, durant l'acquisition, il est
impossible d'utiliser le viseur.<br>

*Prisme :* il redirige les rayons lumineux vers le viseur.<br>

*Viseur :* <br>


<center>
![Image title](../images/cours_appareil/appareils/pentaprism-pentax-forum-co-uk.webp){class="moyen" loading=lazy }
![Image title](../images/cours_appareil/prisme/viseur-optique-oculaire-canon-7D2.jpg){class="moyen" loading=lazy }
![Image title](../images/cours_appareil/viseur/viseur.jpg){class="mini" loading=lazy }
<figcaption> Prisme et viseur</figcaption>
</center>


<br>

## 2. L'œil fonctionnent t'il comme un appareils photographiques ?

<center>
![Image title](../images/cours_appareil/œil/camera-eye-comparison.jpg){class="moyen" loading=lazy }
![Image title](../images/cours_appareil/œil/oeuil_camera1.jpg){class="moyen" loading=lazy }
<figcaption> Parallèle œil, appareil photo</figcaption>
</center>


