---
title : "Exercices 1 : Appareil photographique "
correctionvisible : false
pdf : "exercices"
---


<div class="noprint" style="text-align:right;width:100%;background-color:white">
    <a href="../pdf/{{page.meta.pdf}}.pdf" 
    style="display:inline-block" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="#a3a3a3" stroke-width="1.5" stroke-linecap="butt" stroke-linejoin="arcs"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg>
    </a>
</div>



# Exercices : Les appareils photographiques

### Exercice 8 : 
<center>
<div class="grand">
    <figure>
        <img src="../images/exercices/exo8.png" />
        <figcaption>Manuel Numérique Hilbrt p147</figcaption>
    </figure>
</div>
</center>

??? faq correction "Correction"
    1.Dans un appareil photo, le rôle du diaphragme est moduler la quantité de lumière transmise à la surface photosensible, en modifiant l'ouverture.
    <center>
        <div class="grand">
            <figure>
                <img src="../images/exercices/diaphragme.png" />
                <figcaption>Cours manuel numérique Hilbrt p138</figcaption>
            </figure>
        </div>
    </center>

    2.Dans un appareil photo le contrôle du temps d'exposition se fait à l'aide de l'obturateur.

    3.L’exposition c’est la quantité de lumière que reçoit la surface photosensible de votre appareil lors de la prise de vue d’une photographie.<br>
    Une photo sur-exposée est une photo où trop de lumière a été captée : elle est *“trop claire“*.<br>
    À l’inverse, une photo sous-exposée n’a capté que peu de lumière : elle est *“trop sombre“*.<br>
    <center>
        <div class="grand">
            <figure>
                <img src="../images/exercices/Venise2016_laurent-breillat_surex-822x548.webp" />
                <figcaption>Sur-exposé : trop sombre _apprendre-la-photo.fr</figcaption>
                <img src="../images/exercices/Venise2016_laurent-breillat_sousex-822x548.webp" />
                <figcaption>Sous-exposé : trop sombre _apprendre-la-photo.fr</figcaption>
                <img src="../images/exercices/Venise2016_laurent-breillat-822x548.webp"/>
                <figcaption>Exposition normale_apprendre-la-photo.fr</figcaption>
            </figure>
        </div>
    </center>

    <br>

    **Remarque :** On à bien utilisé le therme “normale” et pas “correcte“. En effet, il est possible de volontairement décider de sous-exposer ou de sur-exposer une photo, légèrement ou même fortement, de façon à créer un effet : une ambiance sombre demandera logiquement de sous-exposer votre image par exemple.<br>

    4.Pour modifier l'exposition, il est possible de jouer avec : l'ouverture (le diaphragme), le temps d'exposition (la vitesse d'obturation) et la sensibilité de la surface photosensible.<br>

    5.Pour une même surface photosensible, un grand angles couvrira un angle de champs important (supérieur à 45°), c'est à dire une partie du paysage importante.
    Un téléobjectif quand à lui couvre un angle de champs étrois (inférieur à 45°), c'est à dire un petite partie du paysage.<br>
    <center>
        <div class="grand">
            <figure>
                <img src="../images/exercices/focale_angledechamp.png" />
                <figcaption>Cours manuel numérique Hilbrt p140</figcaption>
            </figure>
        </div>
    </center>

<br>


### Exercice 9 : 
<center>
<div class="grand">
    <figure>
        <img src="../images/exercices/exo9.png" />
        <figcaption>Manuel Numérique Hilbrt p147</figcaption>
    </figure>
</div>
</center>

??? faq correction "Correction"
    1. Lentilles<br>
    2. Diaphragme<br>
    3. Objectif<br>
    4. Le miroir<br>
    5. Pentaprisme<br>
    6. Obturateur<br>
    7. Surface photosensible<br>
  


### Exercice 10 : 
<center>
<div class="grand">
    <figure>
        <img src="../images/exercices/exo10.png" />
        <figcaption>Manuel Numérique Hilbrt p147</figcaption>
    </figure>
</div>
</center>

??? faq correction "Correction"
    1.Au moment de l'ouverture, la lumière doit atteindre le capteur, le miroir doit donc être relevé. Il s'agit du premier schéma.<br>

    2.<br>
        1.Objectif<br>
        2.Lentilles<br>
        3.Miroir<br>
        4.Obturateur<br>
        5.Capteur(surface photosensible)<br>
        6.Pentaprisme<br>
        7.Diaphragme<br>
        8.Viseur<br>

    3.Lors de la prise d'une photographie, le miroir se rabat pour permettre à la lumière d'arriver jusqu'à la surface photosensible et l'obturateur s'ouvre (pendant la prise de vue).<br>

    4.

    <center>
        <div class="grand">
            <figure>
                <img src="../images/exercices/exo10_correction.png" />
            </figure>
        </div>
    </center>









<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  

