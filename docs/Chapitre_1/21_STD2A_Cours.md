---
title : "Cours"
pdf : 
---


<div class="noprint" style="text-align:right;width:100%;background-color:white">
    <a href="../pdf/{{page.meta.pdf}}.pdf" 
    style="display:inline-block" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="#a3a3a3" stroke-width="1.5" stroke-linecap="butt" stroke-linejoin="arcs"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg>
    </a>
</div>




# Chapitre 2 : Exposition, réglages et rendu




??? info "Dans le programme"
    **Thème 1** : Connaître et transformer les matériaux

    | **Notions et contenus**                                                                                         | **Capacités exigibles**                                                                                 |
    | --------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------- |
    | Familles de matériaux.                                                                                          | - Distinguer les grandes classes de matériaux utilisés dans les domaines du design et des métiers d’art. |
    | Propriétés physiques des matériaux : masse volumique, élasticité, conductivité thermique,absorption acoustique. | - Comparer expérimentalement des *caractéristiques physiques de différents matériaux*.                  |
 

## 1. L'exposition

### 1.1 Qu'est ce que l'exposition ?

#### A. Définitions

!!! def "Qu'est ce que l'exposition"
    C'est la quantité totale de lumière reçu par la surface photosensible de l'appareil pendant la prise de vue.



**Le problème :**<br>
Une surface sensible a une « latitude de pose », on parle également d'une « gamme dynamique », qui est limitée.<br>
Si la luminosité d’une partie de la scène est au-delà ou en deçà de cette gamme, elle ne peut pas être restituée et la portion d’image sera uniformément noire (sous-exposition) ou blanche (surexposition), sans rendu des détails.

<center>
![Image title](../images/cours_expo_regl/exposition/contraste.webp){class="grand" loading=lazy }
<figcaption> La scène photographiée est trop contrastée pour la surface photosensible, les parties noires sont sous-exposées et les parties blanches sont sur-exposées, notons cependant qu'ici c'est ce qui rend l'image intéressante !</figcaption>
</center>




Notre but en photo est de régler cette quantité de lumière afin d'avoir une bonne exposition.<br><br>
D’un point de vue artistique, l’exposition « correcte » est celle qui restitue au mieux l’effet recherché par le photographe : c'est à dire une image équilibrée, ni trop sombre, ni trop claire. Une exposition qui a tous les détails que l'on souhaite là où l'on souhaite.


!!! def "Surexposée"
    Une photo est dite surexposée lorsqu'elle à reçu une quantité de lumière trop importante.<br>
    Une photo surexposée est trop claire, les hautes lumières apparaissent uniformément blanches, ou « brûlées, l'information y est perdu.


!!! def "sous-exposée"
    Une photo est dite sous-exposée lorsqu'elle à reçu une quantité de lumière trop faible.<br>
    Une photo surexposée est trop sombre, elle un manque de détail dans les ombres, qui apparaissent noires ou « bouchées » de l'information (du détail) est perdu dans les basses lumières.


<center>
![Image title](../images/cours_expo_regl/exposition/exposition.jpg){class="grand" loading=lazy }
</center>



Si, comme dans les photos de droites et de gauche dans l'exemple ci-dessus, la luminosité dépasse la gamme dynamique du support à une seule extrémité (scène globalement trop sombre ou très claire), le photographe peut choisir de corriger l’exposition, en modifiant les réglages sur le boîtier afin de la ramener à l’intérieur de la gamme.




Si la scène est très contrastée, ses valeurs extrêmes seront respectivement inférieures au minimum et supérieures au maximum que la surface sensible est capable d’enregistrer. Le photographe devra alors choisir de privilégier les ombres ou les hautes lumières au détriment les unes des autres.



<center>
![Image title](../images/cours_expo_regl/exposition/Bracketed-Exposures-1024x1024.jpg){class="grand" loading=lazy }
<figcaption>Il faudra choisir ! Du détail dans le ciel ou dans les plantes ?</figcaption>
</center>



??? info "Palier au problème d'une scène trop contrasté"
    **Méthode 1: Réduire le contraste de la scène**

    - *Technique 1 :* Utiliser un filtre gris gradué pour diminuer la luminosité des hautes lumières, et les ramener dans la plage dynamique de la surface photosensible.


    - *Technique 2 :* Employer des flashs (fill-in) pour remonter la luminosité des basses lumières.
    
    <br>

    **Méthode 2 : Augmenter artificiellement la plage dynamique de la surface photosensible (HDR)**<br>
    On réalise plusieurs prise de vue en variant l’exposition (bracketing) pour les combiner et obtenir ainsi une image à grande dynamique.<br>

    <center>
    ![Image title](../images/cours_expo_regl/exposition/Firey-Dawn-1024x682.jpg){class="grand" loading=lazy }
    <figcaption>HDR réalisé grâce à la série montré précédemment dans le cour</figcaption>
    </center>

    <br/>

    Il faut être léger avec les réglages sinon on arrive vite à des rendu peu naturels *(voir horribles)*!

    <center>
    ![Image title](../images/cours_expo_regl/exposition/hdr_deg.jpg){class="grand" loading=lazy }
    <figcaption> HDR mal réglé, ça pique les yeux ! </figcaption>
    </center>

    **Un exemple de ce que l'on est capable de faire !**

    <center>
    ![Image title](../images/cours_expo_regl/exposition/lune-cendree-1024x682.jpg){class="grand" loading=lazy }
    <figcaption> [HDR bien fait !](https://www.youtube.com/embed/3f_21N3wcX8) </figcaption>
    </center>
    
    
    Dans ces photos, la plage dynamique du sujets est en réalité extrême ! Faite une photo de la lune et comparer, vous verrez la différence ! *On ne parle bien sur pas de la taille, ni de la définition, uniquement de la plage dynamique !*<br/>


    *Et si vous voulez réaliser la même chose, un petit tuto : [https://www.youtube.com/watch?v=AUqVmW-HFh0](https://www.youtube.com/watch?v=AUqVmW-HFh0)*



??? tip "Exposition et esthétique"
    Attention une image très claire n'est pas nécessairement une image sur-exposée, de même qu'une image très sombre n'est pas nécessairement sous-exposée.

    Il existe deux techniques photographique, nommé *high key* et *low key*, qui jouent avec ce principe.

    **High key : photo dont les tons prédominants sont très claires mais pas cramé !**
    <center>
    ![Image title](../images/cours_expo_regl/exposition/high_key.jpg){class="grand" loading=lazy }
    <figcaption>High key, l'image n'est pas sur-exposée !</figcaption>
    </center>

    <br/>


    **Low key : photo dont les tons prédominants sont très sombres mais pas bouché !**
    <center>
    ![Image title](../images/cours_expo_regl/exposition/low_key.jpg){class="grand" loading=lazy }
    <figcaption>Low key, l'image n'est pas sous-exposée !</figcaption>
    </center>


    Lien vidéo :

    - [STUDIO - High Key / Low Key, créer votre éclairage - S02E34 - F/1.4 ](https://www.youtube.com/watch?v=4StAdubAXS8)
    - [Comment réussir le low key, avec Laurent Dubois](https://www.youtube.com/watch?v=dZQinR76Yng)
    - [High Key Black and White Landscapes -](https://www.youtube.com/watch?v=JAAOEC2fV_8)
    - [Landscape Photography | A complete guide to High Key Processing](https://www.youtube.com/watch?v=OQxyCPTwXdc)

<br/>


#### B. De quels paramètres dépend l'exposition ?

L'exposition dépend de quatre paramètres :

- La luminosité de la scène que l'on souhaite photographier.
<br/>
  
- La sensibilité de la surface photosensible, les ISO (pour un capteur numérique).
- L'ouverture.
- La vitesse d'obturation (ou le temps de pause).



Sur le boîtier, il est possible d'intervenir uniquement sur les trois derniers paramètres !

<br/>

#### C. L'indice de luminance (ou lumination), une notion bien pratique pour comprendre l'exposition

L'exposition est affectée par trois facteurs : la vitesse d’obturation, l’ouverture et la sensibilité ISO, or tous ces paramètres sont mesurés dans des unités différentes, c’est pourquoi le concept de « stops » a été inventé comme moyen pratique de les comparer.

!!! remarque "Qu'est ce que l'indice de luminance ?"
    L'indice de luminance d'exprime en «stop», ou encore en «IL» ou «EV».

    Quand on indique « +1 stop » (Il ou EV), cela correspond à une augmentation de deux fois la quantité de lumière recueillie pendant l’exposition.

La notion de d'indice de luminance (les "stop") met l’accent non pas sur les unités de mesure, mais sur l’effet sur l’exposition.

Nous pouvons alors facilement jouer sur : la vitesse d’obturation, l’ouverture et la sensibilité ISO, tout en conservant la même exposition globale.

??? note "lien vidéo"
    - [https://www.youtube.com/watch?v=ZZRq8Lvgqxg](https://www.youtube.com/watch?v=ZZRq8Lvgqxg)


<br/>

### 1.2 Comprendre l'exposition
#### A. La luminosité de la scène à photographier
La luminosité de la scène à photographier dépend de deux choses. La lumière incidente et la capacité de réflexion de la lumière des objets constituant la scène.

Pour être en mesure d'effectuer les réglages permettant d'obtenir une photographie correctement exposée, il est fondamental de réaliser une mesure de la luminosité.

??? tip "Mesure de la lumière"
    Il existe deux possibilité :

    - **La mesure de la lumière incidente *(la lumière qui arrive sur la scène à photographier)***
    - **La mesure de lumière réfléchie *(la lumière réfléchie par la scène photographié)***
  
    <br/>

    La mesure de la lumière incidente doit se faire avec un luxmètre,(flashmètre). La mesure faite par le boîtier est une mesure de la lumière réfléchie. 

    Il faut savoir que le boîtier est conçu pour restituer correctement la luminosité d'un objets aillant une réflectance de 18%, c'est à dire un objets qui renverrais vers l'appareil 18% de la lumière reçu.
    Ce fait peut imposer d'avoir besoin d'effectuer des corrections d'expositions. 

    <center><iframe width="560" height="315" src="https://www.youtube.com/embed/5C49gnbzT_Y" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center>



??? info "Autres liens vidéo :"
    - [Tuto Mesure et maîtrise de la lumière et de l'exposition avec le posemètre externe](https://www.youtube.com/watch?v=vZo-FykYWj0)
    - [clic-clac_comment fonctionne un Posemètre](https://www.youtube.com/watch?v=8onRJ6qJdpU)

    
    - [Objectif Photographe_La mesure d'exposition](https://www.youtube.com/watch?v=FTEU1tCGjfE)

<br/>


#### B. Sensibilité ISO :
!!! def "Définition"
    La sensibilité ISO, c’est la sensibilité du capteur à la lumière. Plus la valeur est grande plus la surface photosensible est sensible.<br>

Pour une même quantité de lumière reçu, c'est à dire un même couple ouverture et vitesse d’obturation, une sensibilité ISO plus élevée augmentera l’exposition du cliché, l’image sera plus lumineuse.


<center>
![Image title](../images/cours_expo_regl/iso/ISO-brightness-chart-960x448.jpg){class="grand" loading=lazy }
<figcaption> Lien entre les ISO et l'exposition </figcaption>
</center>



Les ISO se mesurent sur une échelle simple et facile à comprendre. Les valeurs d’ISO standard en photo que l’on trouve sur les appareils photos en règle générale, sont :


<center>
![Image title](../images/cours_expo_regl/iso/echelle_iso.png){class="tresgrand" loading=lazy }
<figcaption>Échelle de valeurs ISO</figcaption>
</center>



Doubler ou diviser par deux la sensibilité ISO, produit une augmentation ou une diminution de 1 stop.

<br>
Par exemple, passer de  400 ISO  à 800 ISO, signifie que le capteur aura besoin de deux fois moins de lumière pour être correctement exposé, on peut donc dire que nous avons réduit l’exposition d’un stop. De même, passer de 800ISO à 1600ISO, permet de réduire les besoins en lumière de 1stop, pour que l'image soit correctement exposé.


<center>
![Image title](../images/cours_expo_regl/exposition/400-800.webp){class="grand" loading=lazy }
<figcaption>Ces deux photo sont exposée de marnière identique, le passage de 400iso à 800iso, augmentation de 1stop, à du être compensé par la diminutions de la vitesse de 1/5 de seconde à 1/10 de seconde, soit de 1 stop</figcaption>
</center>



??? remarque "Remarques"
    **En photographie argentique :** <br>
    La sensibilité est déterminée essentiellement par le choix du film photographique.

    **En numérique :**<br>
    La sensibilité est un réglage de l'appareil qui n'agit pas sur le capteur lui-même (la matrice de photodiodes) mais sur l'amplification du signal produit par celui-ci.



??? info "Autres liens vidéo :" 
    - [Apprendre la photo](https://www.youtube.com/watch?v=EwHdCWmojDU&list=PLVGCy9QVeRNXyWXamf9h5fbFslJnaD2C9)
    - [YouTube](https://www.youtube.com/watch?v=EBO3B0OTTtI)
    - [Objectif photographe_les iso](https://www.youtube.com/watch?v=EBO3B0OTTtI)
    - [Tuto Photo - Comprendre les ISO](https://www.youtube.com/watch?v=2_91VFeoRBM)


<br>

#### C. La vitesse d'obturation

*Ou régler la vitesse d'obturation sur le boîtier ?*

<center>
![Image title](../images/cours_expo_regl/vitesse/nikon_d7000_reglages.jpg){class="moyen" loading=lazy }
![Image title](../images/cours_expo_regl/vitesse/Nikon_Z_fc_ergonomie.jpg){class="moyen" loading=lazy }
![Image title](../images/cours_expo_regl/tps_pause/x-pro2_feature06-03.jpg){class="petit" loading=lazy }
<figcaption>Régler de la vitesse, en 1 et/ou grâce à la molette généralement présente à coté du déclencheur</figcaption>
</center>

<br/>

*Qu'est ce que la vitesse d'obturation ?*

!!! Remarque "Vitesse d'obturation"
    La vitesse d’obturation mesure la durée pendant laquelle l’obturateur de l'appareil photo reste ouvert lors d'une prise de vue. On parle alors de *temps d'exposition* ou de *temps de pose*.



Plus le temps d'expositions sera important, plus l'obturateur reste ouvert longtemps, plus il laisse entrer de lumière et plus votre photo sera lumineuse.

Un temps d’exposition trop faible donnera une photo sous-exposée, un temps d’exposition trop long donnera une image surexposée.

<br/>

La vitesse d’obturation se mesure en secondes ou, le plus souvent, en fractions de seconde. Ainsi une vitesse réglée à 2'' aura un temps d'exposition de 2 secondes. Une vitesse réglée à 1/5 aura un temps d'exposition d'un cinquième de seconde, soit 200ms.


<center>
![Image title](../images/cours_expo_regl/tps_pause/temps.png){class="tresgrand" loading=lazy }
</center>

<br/>

Doubler ou diviser par deux la vitesse d’obturation produit une augmentation ou une diminution de 1 stop.

Par exemple, passer de 1/100 de seconde à 1/200 laisse passer deux fois moins de lumière, on peut donc dire que nous avons réduit l’exposition d’un stop. De même, passer de 1/60 à 1/30 laisse passer deux fois plus de lumière, ce qui donne une augmentation de 1 stop.


<center>
![Image title](../images/cours_expo_regl/tps_pause/vitesse.jpg){class="grand" loading=lazy }
<figcaption>Lien entre changement de vitesse et stop</figcaption>
</center>



??? info "Autres liens vidéo :"
    - <a href="https://www.youtube.com/watch?v=pD45CIQx3Ks" >Apprendre la photo</a>
    - <a href="https://www.youtube.com/watch?v=_WPJJYFNtRg" >Cours-de-photographie</a>


<br/>


#### D. L'ouverture

*Ou régler l'ouverture sur le boîtier ?*

<center>
![Image title](../images/cours_expo_regl/vitesse/nikon_d7000_reglages.jpg){class="moyen" loading=lazy }
![Image title](../images/cours_expo_regl/ouverture/D3S_7458-top.png){class="moyen" loading=lazy }
![Image title](../images/cours_expo_regl/ouverture/Lens_aperture_side_crop.jpg){class="petit" loading=lazy }
<figcaption>Régler l'ouverture, en 2 et/ou grâce à une bague présente sur l'objectif. Sur le gros plan l'ouverture est de 11.</figcaption>
</center>

<br/>

*Qu'est ce que l'ouverture ?*

!!! remarque "Ouverture"
    L’ouverture est l’orifice par lequel la lumière passe avant d’atteindre la surface photosensible. Elle est caractérisé par le nombre d'ouverture.
    
    
!!! remarque "Nombre d'ouverture"
    Le nombre d'ouverture, noté sous la forme \(f/\) ou \(N=\) suivi d'un nombre, caractérise l'ouverture.<br>
    Plus le nombre d'ouverture est petit, plus l'ouverture sera grande et plus la lumière passe.
    

<center>
![Image title](../images/cours_expo_regl/ouverture/810px-ouvertures_standard.svg){class="tresgrand" loading=lazy }
<figcaption>Taille de l'ouverture et nombre d'ouverture</figcaption>
</center>

En clair, f/2 désigne une ouverture beaucoup plus grande que f/11.

<br/>

Sur les appareils photo actuel, la taille de l’ouverture est généralement réglable.
Lorsque l'on tourne le bouton, ou la bague de réglage correspondante à l'ouverture, on modifie la position des lames du diaphragme.

<center>
![Image title](../images/cours_expo_regl/ouverture/Lenses_with_different_apetures.jpg){class="grand" loading=lazy }
</center>


    


??? info "Petit point Histoire"
    Avant l’invention des diaphragmes, on utilisé des surfaces percée de trou de différent diamètre que l'on venait placer devant la lentille avant la prise de vue.

    <center>
    ![Image title](../images/cours_expo_regl/ouverture/Dallmeyer_waterhouse.jpg){class="moyen" loading=lazy }
    <figcaption>Utilisation d'insert à trou servant de diaphragme</figcaption>
    </center>


    **Autres liens :**<br/>
    - [wiki diaphragme](https://fr.wikipedia.org/wiki/Diaphragme_(optique))


??? info "Il y a toujours une ouverture !"
    Même s'il n'y a pas de réglage de l'ouverture, il y a obligatoirement une ouverture.
    Ainsi, un sténopé possède une ouverture, il en est de même pour un télescope ou une lunette !

    Cette ouverture peut alors être calculer avec la relation présenté dans le cadre info ci-dessous.

??? info "Calcul du nombre d'ouverture"
    Le nombre d'ouverture est un nombre sans dimension, défini comme le rapport du diamètre `d` de la pupille d'entrée à la focale `f` (distance focale image, positive, ici notée `f` pour simplifier les écritures).

    $$ N = \frac{f}{d} $$

<br/>

Attention, pour doubler la quantité de lumière qui passe, il faut doubler la surface de l’ouverture et non le diamètre !<br/>
C’est pourquoi le calcul des valeurs numériques d’ouverture est un peu plus complexe que celui de la vitesse d’obturation ou de la sensibilité ISO.

Par convention, il a été établi une suite de nombres d'ouverture pour lesquels le passage à la valeur supérieure entraîne une division par deux de l'éclairement, c'est à dire qu'il y a un stop entre chacune des valeurs.

<center> <strong>f/1 – f/1,4 – f/2 – f/2,8 – f/4 – f/5,6 – f/8 – f/11 – f/16 – f/22 – f/32</strong></center>


**Quelques exemples :**

- Passer de `F/4` à `F/5,6` = diviser la quantité de lumière par deux (- 1 Il)
- Passer de `F/11` à `F/8` = on double la quantité de lumière (+1 IL)
- Passer de `F/8` à `F/4` = quadrupler la quantité de lumière (+2 IL)




??? info "Comprendre la progression des nombres d'ouverture"
    En raison de la façon dont les « nombres f » sont calculés, les valeurs successive correspondent aux valeurs approchées d'une suite géométrique de raison √2, c'est à dire que pour passer d'un terme au suivant on multiplie par √2=1,41.
    
    <center> 0,5 – 0,7 – 1 – <strong> 1,4 – 2 – 2,8 – 4 – 5,6 – 8 – 11 – 16 – 22 – 32 </strong>– 45 – 64 – 90 – 128 – 181 – 256 – 362 – …</center>

    Par exemple, passer de `f/2,8` à `f/4` est une diminution de 1 stop, car `4 = 2,8 × 1,41`.
    Le passage de `f/16` à `f/11` est une augmentation de 1 stop, car `11 = 16/1,41`.

    <center>
    ![Image title](../images/cours_expo_regl/.png){class="moyen" loading=lazy }
    <figcaption>À faire image lien entre surface et diamètre, de deux trou consécutifs</figcaption>
    </center>






??? info "Autres liens vidéo :"
    - <a href="https://www.youtube.com/watch?v=v_W8O1U4qRU" >Apprendre la photo</a>
    - <a href="https://phototrend.fr/2016/03/mp-169-ouverture-et-fstop-en-finir-avec-la-confusion/" >phototrend</a>
    - [YouTube](https://www.youtube.com/watch?v=9B5dwfhNtAk)

  
<br>


### 1.3 Bonne exposition

??? remarque "Simulateurs"
    - [Simulateur Canon](http://www.canonoutsideofauto.ca/play/)
    - [Simulateur Moulin à vent](https://camerasim.com/camerasim-free-web-app/)
    - [Simulateur Lampe](http://www.andersenimages.com/tutorials/exposure-simulator/)
    - [Simulateur peluche](https://www.bettertechtips.com/photography/camera-simulator/)



#### A. L'analogie du verre d'eau

Pour mieux comprendre l'exposition et le lien entre les différents paramètres, on peut recourir à  l'analogie du remplissage d'un verre d'eau. Dans cette analogie :

- L'eau représente la lumière.
- La taille du verre représente la sensibilité de la surface photosensible, plus le verre est petit plus la quantité d'eau pour le remplir est faible, ce qui correspond à une sensibilité plus importante, soit une valeur d'ISO plus importante.
- Le temps pendant lequel le robinet sera ouvert (l’eau coule) va représenter le temps de pose (également appelée vitesse d’obturation), la durée pendant laquelle la lumière passe par l’obturateur pour impressionner le capteur.
- L’ouverture plus ou moins grande du robinet (le débit d’écoulement) représente l’ouverture du diaphragme (le trou plus ou moins grand par lequel passe la lumière lorsque l’obturateur est ouvert).


Dans cette analogie, une exposition correcte correspond à un verre rempli normalement, un verre peu rempli correspond alors à une sous-exposition et un verre qui déborde à une sur exposition.


<center>
    <div class="grand">
        <figure>
            <img src="../images/cours_expo_regl/analogie/exposition.png" />
            <figcaption>L'exposition dans l'analogie du verre d'eau</figcaption>
        </figure>
    </div>
</center>


Une même quantité d’eau pourra être obtenue soit en ouvrant grand le robinet pendant peu de temps (le fort débit permet de rapidement fermer le robinet), soit en ouvrant très peu le robinet mais pendant plus longtemps (un faible débit nécessitera plus de temps pour remplir le verre).
En traduisant en termes photographiques, une même exposition pourra être obtenue soit en ouvrant grand le diaphragme pendant un temps de pose court, soit en ouvrant très peu le diaphragme pendant un temps de pose plus long.


<center>
    <div class="grand">
        <figure>
            <img src="../images/cours_expo_regl/analogie/source.gif" />
            <figcaption>L'analogie du verre d'eau</figcaption>
        </figure>
    </div>
</center>

??? info "Autres liens vidéo :" 
    - <a href="https://www.youtube.com/watch?v=V20odwEd5jE" >Apprendre la photo</a>
    - <a href="https://www.youtube.com/watch?v=WYhxNaQ4n-k" >Les Essentiels PHOTO</a>
    - <a href="https://www.youtube.com/watch?v=jzjxWMV_Srw" >Les Essentiels PHOTO_mode manuel</a>
    - https://www.youtube.com/watch?v=7K_tHmjNhac
    - https://www.youtube.com/watch?v=aaHrtSMDXdk
    - https://www.youtube.com/watch?v=4INmE42yAZk
    - [blogphotographie](https://blogphotographie.com/comprendre-et-maitriser-le-triangle-dexposition/)
    - [joannietherrien.com](https://joannietherrien.com/triangle-dexposition/)


<br>

#### B. Réciprocité, l'indice de luminance à la rescousse
L’exposition fonctionne selon le principe de réciprocité : augmenter la durée de l’exposition nécessite de fermer le diaphragme pour que la quantité de lumière atteignant la surface sensible reste identique.

C'est ici que la notion d'indice de luminance (les stops), démontre son intérêt, elle nous permet de facilement jouer sur : la vitesse d’obturation, l’ouverture et la sensibilité ISO, tout en conservant la même exposition globale.<br/>
En effet, si l'on augmente un paramètre d'une valeur de un stop, alors l'exposition sera conservé, en diminuant un autre paramètre d'un valeur de un stop.

<br/>

#### C. Plusieurs choix possibles !
D'après le principe de réciprocité vue précédemment, on comprend que pour exposé correctement une photographie, plusieurs trio (la vitesse d’obturation, l’ouverture et la sensibilité ISO) sont envisageables.

*Dès lors que l'on connaît une exposition correcte, la notion de stop nous permet de trouver toutes les autres combinaisons possibles !*


??? info "Règles du f/16 :"
    La règle de f/16 est une méthode empirique d’estimation de l’exposition sans posemètre. Cette règles était très utilisée en photo argentique lorsque les appareils n'étaient pas encore pourvue de dispositif de mesure de la lumière, ou que ceux-ci manqué de fiabilité. C'est une règle qui reste intéressante à connaître de nos jours car elle se base sur la lumière incidente.<br>

    Selon cette règle :<br>
    *Une scène ensoleillée, sera correctement exposé si pour une ouverture de f/16, on choisi  une vitesse équivalente à la sensibilité ISO de la surface sensible.*<br><br>
    **Exemple :** Pour une sensibilité de 100 ISO, si la scène est ensoleillé alors pour une bonne exposition, à une ouverture de f/16 correspond une vitesse de 1/125 seconde *(qui est généralement la vitesse disponible la plus proche du centième de seconde)*.

    En réalité, cette règle était généralement fournit dans les notices des appareils photo sous la forme d'un tableau :
    <center>
        <div class="grand">
            <figure>
                <img src="../images/cours_expo_regl/luxmetre/Sunny_16.svg" />
                <figcaption>À modifier : tableau règle f16</figcaption>
            </figure>
        </div>
    </center>

    Les autres couples possibles été déterminés à l'aide de la notions de stop.<br>
    *(À l'époque certaines notices ressemblait d'avantage à un cours de photo qu'à une notice, il faut dire que les appareils étaient beaucoup plus simples et qu'internet n'existait pas ! )*

  
  
Cependant, bien que l'exposition soit la même pour l'ensemble des couples possibles, le rendue de l'image lui sera différents !

<center>
    <div class="grand">
        <figure>
            <img src="../images/cours_expo_regl/tps_pause/image.jpg" />
            <figcaption>Les deux photo sont exposées correctement, mais le rendu est différent !</figcaption>
        </figure>
    </div>
</center>


*C'est le rendu, c'est-à-dire le choix esthétique fait par le photographe qui permet de sélectionner le bon trio !*

<br>

### 1.3 Influances des choix effectué pour l'exposition sur le rendu de l'image
#### A. Influence de la sensibilité


Une sensibilité élevée facilite la photographie en basse lumière, mais le rendu de l'image est lui aussi modifié.



**En argentique :**<br>
Les films plus sensibles (ou poussés) ont un grain beaucoup plus marqué. L'image apparaît alors plus granuleuse.

<center>
    <div class="moyen">
        <figure>
            <img src="../images/cours_expo_regl/iso/selec-expo-4.webp" />
            <figcaption>Photo argentique poussée</figcaption>
        </figure>
    </div>
</center>


??? info "Poussé un film argentique"
    - [La photo argentique](https://www.la-photo-argentique.com/pousser-ou-retenir-un-film-une-pellicule-argentique/)

**En numérique :**<br>
La sensibilité est un réglage de l'appareil qui n'agit pas sur le capteur lui-même (la matrice de photodiodes) mais sur l'amplification du signal produit par celui-ci. L'amplification rend le bruit du capteur plus visible et aboutit à des images plus bruitées.


<center>
    <div class="grand">
        <figure>
            <img src="../images/cours_expo_regl/iso/Sensibilite-ISO-Photo-de-nuit.jpg" />
            <figcaption>Monté du bruit numérique avec l'élévation des ISO</figcaption>
        </figure>
    </div>
</center>



??? info "Remarque :"
    Bien que le bruit numérique n'est pas l'esthétique du grain argentique, les capteurs numériques permettent des montées en ISO inimaginables jusque alors. D'année en année, le bruit numérique est de mieux en mieux géré, ce qui change totalement la pratique de la photographie en basse lumière !

    <center>
    <div class="grand">
        <figure>
            <img src="../images/cours_expo_regl/iso/Sensibilite-ISO-Ph" />
            <figcaption>Exemple - à faire</figcaption>
        </figure>
    </div>
</center>


<br>



#### C. Influence de l'ouverture



##### a. La profondeur de champ

<center>
    <div class="grand">
        <figure>
            <img src="../images/cours_expo_regl/ouverture/prodondeur-de-champ-exemples.jpg" />
            <figcaption>Notion de profondeur de champ</figcaption>
        </figure>
    </div>
</center>
<br>

##### a. Effet sur la profondeur de champ
À focale et distance de mise au point constantes, la fermeture du diaphragme, c'est à dire l'augmentation du nombre d'ouverture, permet d'augmenter la profondeur de champ.

<center>
    <div class="grand">
        <figure>
            <img src="../images/cours_expo_regl/ouverture/ouverture-prof2.webp" />
            <figcaption>La distance de mise au point est la mème distance, mais l'ouverture et par conséquent la distance de profondeur de champs sont différentes</figcaption>
        </figure>
    </div>
</center>



[Maîtrisez l'ouverture et transformez vos photos - Genaro Bardy](https://www.youtube.com/watch?v=7hHEnyUnlNU)



??? remarque "simulation"
    -[lens simulation](https://www.samyanglens.com/en/product/simulator/lens.php)


??? info " D'autres effets de l'ouverture"
    La modification de l'ouverture n'a pas que des effets sur la profondeur de champ, elle en a également dans une moindre mesure sur les performances optiques, le piquet !

    **Effet sur le vignettage**
    Le vignettage désigne l'assombrissement de la périphérie d'une image.


    **Effet sur les aberrations géométriques et chromatiques**<br>
    Une diminution de l'ouverture réduit les aberrations géométriques et chromatiques.<br>

    **Effet sur le phénomène de diffraction**<br>
    Une diminution de l'ouverture augmente l'influence de la diffraction.<br>

    **

    La combinaison des ces effets, explique pourquoi le piquet optimal d'une optique est généralement obtenue pour une ouverture comprise entre et .(en 24x36)<br>



??? info "L'hyperfocale"
    L’hyperfocale est la distance minimum à laquelle il est possible de faire la mise au point tout en gardant les objets situés à l'infini avec une netteté acceptable. La mise au point à cette distance permet d'obtenir la plus grande plage de netteté acceptable qui s'étend alors de la moitié de cette distance à l'infini.

    **Utilisations :**
    *photo de paysage :* L'intérêt est ici d'avoir la plus grande profondeur de champ pour une ouverture donnée.
    *photo documentaire ou de rue :*
    Souvent en photographie documentaire ou en photographie de rue, la distance au sujet n'est pas a priori connue alors que, simultanément, il est nécessaire de pouvoir opérer avec rapidité, L'hyperfocale permet alors de prédéterminer une mise au point permettant d'obtenir une plage de netteté suffisamment étendue pour couvrir les sujets prévisibles. Cela est particulièrement utile pour la mise au point manuelle, en l'absence d'auto-focus ou s'il on veut s'en dispenser.

    Pour le même boîtier 24×36, cette fois équipé d’un objectif 35 mm, l'hyperfocale à f/8 est à environ 5 m. Une mise au point réglée aux environs de 5 m assurera que les sujets principaux situés à une distance supérieure à 2,5 m seront nets. 
    - [L’hyperfocale : définition, calcul, réelle utilité et usage pratique](https://apprendre-la-photo.fr/lhyperfocale/)



??? remarques "Liens :"
    - <a href="https://phototrend.fr/2012/02/mp-100-la-profondeur-de-champ/" >Phototrend</a>
    - <a href="https://www.youtube.com/watch?v=RX2HuS0i6ys" >Les Essentiels PHOTO_La profondeur de champ</a>


<br>



#### D. Influence de la vitesse d'obturation
Si l'appareil, ou les objects photographié se déplace suffisamment pendant le temps de pose, alors il seront flou.
Pour fixer le mouvement le temps de pose doit être suffisamment cours !

Cependant, ce flou est un bon moyen de restitution l'impression du mouvement et par conséquent il peut être recherché.


[Maîtrisez la vitesse d'obturation en photographie - Genaro Bardy](https://www.youtube.com/watch?v=C07c4ahKntw)


##### a. Flou cinétique du aux mouvements du sujet

<center>
    <div class="grand">
        <figure>
            <img src="../images/Cours_photo/shema-profondeur-de-champ-focales-e1490008578199.jpg" />
            <figcaption> À faire_photo cascade d'eau, mouvement de foule, mouvement raquette tennis, mouvement lightpainting, etc</figcaption>
        </figure>
    </div>
</center>

##### b. Flou cinétique du aux mouvements du photographe
En général, on essai d'éviter ce type de flou cinétique, pour cela on utilise un trépied.
Cependant, il peut rendre compte d'une impression de risque pris par le photographe et en ce sens servir l'image.
<center>
    <div class="grand">
        <figure>
            <img src="../images/Cours_photo/shema-profondeur-de-champ-focales-e1490008578199.jpg" />
            <figcaption> à faire_photo chien, débarquement capa</figcaption>
        </figure>
    </div>
</center>




##### c. Flou cinétique du aux mouvements combiner du sujet et du photographe

<center>
    <div class="grand">
        <figure>
            <img src="../images/Cours_photo/shema-profondeur-de-champ-focales-e1490008578199.jpg" />
            <figcaption> à faire_photo de véhicule</figcaption>
        </figure>
    </div>
</center>


??? info "D'autres exemples :"
    <center>
        <div class="grand">
            <figure>
                <img src="../images/Cours_photo/ima654654ge.jpg" />
                <img src="../images/Cours_photo/" /
                <figcaption> à faire</figcaption>
            </figure>
        </div>
    </center>



<br>


### 1.4 Ordre des réglages

Nous avons vue que bien que l'exposition soit la même pour de multiple trio : *sensibilité, ouverture, temps de pause*, le rendue de l'image lui est différents. Le choix du trio a utiliser est guidé par l'esthétique attendue par le photographe.

Ainsi l'ordre des réglages est généralement le suivant :<br>

- On choisi les ISO, de façon à ne pas avoir de bruit numérique visible sur l'image.
  
- Si le sujet photographié est en mouvement et que c'est ce que l'on veut restituer, on choisi la vitesse. Puis on règle l'ouverture correspondante.

- Si le sujet photographié, n'a pas un mouvement important, et que l'on veux isoler grâce a un faible profondeur de champ ou au contraire une très grande profondeur de champ, comme en photo de paysage, on choisi l'ouverture. Puis on règle la vitesse correspondante.



??? note "Ressources"
    lien
    https://emmanuelgeorjon.com/photo/technique-photo-vitesse-dobturation/


    mesurer une vitesse d'obturation
    http://t.hacquard.free.fr/site2/testobtu1.html


    cercle image et angles de champs
    https://galerie-photo.com/cercle-image-et-angle-de-champ.html


    même grandissement angle de champ différent
    http://www.1point2vue.com/focale-cadrage/



??? note "Autres ressources"
    - démarche photographique-lumière : https://www.youtube.com/watch?v=L0pS-JpFfcA
    - utiliser le flashmètre/posemètre : https://www.youtube.com/watch?v=t5J7DukCago


