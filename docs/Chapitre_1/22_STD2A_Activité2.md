---
title : "Activité"
correctionvisible : False
pdf : ""
---


<div class="noprint" style="text-align:right;width:100%;background-color:white">
    <a href="../pdf/{{page.meta.pdf}}.pdf" 
    style="display:inline-block" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="#a3a3a3" stroke-width="1.5" stroke-linecap="butt" stroke-linejoin="arcs"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg>
    </a>
    <button id="bt_test">
        <img src="../../assets_extra/images/cravateseule.svg" width="30"/>
    </button>
</div>

<div id="masque"  style="display:none;text-align:center;font-size: 25px;font-weight: bold;">
    <a href="../pages_cache/enavance/">Page cachée</a>
</div>

<script>
    let bt1 = document.getElementById("bt_test");
    let d2 = document.getElementById("masque");


    function identification(){
        if (d2.style.display!= "block"){
            var reponse = prompt("Es-tu réellement un porteur de cravate ?","<saisir un identifiant>");
            if(reponse==45){
                d2.style.display = "block";
            }
        }
    };

    bt1.onclick = identification;
</script>






# Activité 2 : Réglages et rendu photographique


??? info "Compétences travaillées"
    Rechercher et extraire des informations

<br>








## 2. Influance de chacun des paramètres sur le rendu de l'image




### 1. Influance de la sensibilité de la surface sensible
- Se mètre en mode automatique.
- Choisir un sujet.
- Choisir un cadrage.
*(pendant toute la suite de cette partie, le sujet et le cadrage ne devrons pas changer !)*

**Expérience**
- Régler la sensibilité iso la plus basse possible (50 ou 100).
- Prendre un photo.
- Modifier la sensibilité iso (sauter trois valeurs)
- Prendre un photo (vérifier son exposition)
- Reproduire les étapes précédentes jusqu'à ce que l'appareil n'arrive plus à expossé correctement la photographie.


**Exploitation**
- Faire défiler les images, zoomer à fond sur l'image.
- Faire défiler les images et observer les valeurs de la vitesse et de l'ouverture.

- Quelle est l'influance de la valeur iso sur l'image et sur la prise de vue.








### 2. Influance de l'ouverture
- Se mètre en mode : priorité à l'ouverture.
- Choisir un sujet.
- Choisir un cadrage.
  
*(pendant toute la suite de cette partie, le sujet et le cadrage ne devrons pas changer !)*

**Réalisation de la première photo de référence.**

Choisir l'ouverture la plus petite possible, permattant d'obtenir une photo correctement exposée.

**Expérience**

- Prendre un photo.
- Modifier l'ouverture (prendre la valeur au dessus)
- Prendre un photo (vérifier son exposition)
- Reproduire les étapes précédentes jusqu'à ce que l'appareil n'arrive plus à expossé correctement la photographie.

**Exploitation**
- Faire défiler les images et observer l'influance de l'ouverture sur l'image.






### 3. Influance de la vitesse d'obturation
#### Test 1
- Se mètre en mode : priorité à la vitesse.
- Choisir un sujet immobile.
- Choisir un cadrage.
  
*(pendant toute la suite de cette partie, le sujet et le cadrage ne devrons pas changer !)*


**Réalisation de la première photo de référence.**

Choisir la vitesse la plus grande possible, permattant d'obtenir une photo correctement exposée.

**Expérience**
- Prendre un photo.
- Diminuer la vitesse.
- Prendre un photo (vérifier son exposition)
- Reproduire les étapes précédentes jusqu'à ce que l'appareil n'arrive plus à expossé correctement la photographie.

**Exploitation**
- Faire défiler les images et observer l'influance de la vitesse sur l'image.



#### Test 2
- Se mètre en mode : priorité à la vitesse.
- Choisir un sujet en mouvement (camarade qui marche, …).

*(pendant toute la suite de cette partie, le sujet, les mouvements du sujet, la distance au sujet, et le zoom ne devrons pas changer !)*

**Réalisation de la première photo de référence.**

Choisir la vitesse la plus grande possible, permattant d'obtenir une photo correctement exposée.

**Expérience**

- Prendre un photo.
- Diminuer la vitesse.
- Prendre un photo (vérifier son exposition)
- Reproduire les étapes précédentes jusqu'à ce que l'appareil n'arrive plus à expossé correctement la photographie.

**Exploitation**
- Faire défiler les images et observer l'influance de la vitesse sur l'image.











**Vidéo pour régler l'appareil du Lycée.**
https://www.sony.fr/electronics/appareils-photo-cyber-shot-compacts/dsc-h300

https://www.youtube.com/watch?v=aCEkhwAM7T8

https://www.youtube.com/watch?v=9kPrCqq2wTs

