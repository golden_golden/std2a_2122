---
title : "Exercices"
correctionvisible : True
pdf : ""

---


<div class="noprint" style="text-align:right;width:100%;background-color:white">
    <a href="../pdf/{{page.meta.pdf}}.pdf" 
    style="display:inline-block" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="#a3a3a3" stroke-width="1.5" stroke-linecap="butt" stroke-linejoin="arcs"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg>
    </a>
</div>


### Exercice 12 : 
<center>
<div class="grand">
    <figure>
        <img src="../images/exercices/exo12.png" />
        <img src="../images/exercices/exo12_2.png" />
        <figcaption>Manuel Numérique Hilbrt p147</figcaption>
    </figure>
</div>
</center>

??? faq correction "Correction"
    1. Pour réaliser cette photographie :

        - Si l'on regarde les zones sombres, on constate que le bruit numérique reste relativement faible, on peut donc en déduire que les ISO resterons relativement faibles *(inférieur à 1600iso)*, cependant ils sont pas à la valeur la plus faible car la scène à photographier est très sombre.<br>
        - La profondeur de champ est importante : on en déduit que l'ouverture sera faible, c'est à dire que le nombre d'ouverture sera grand.
        - On constate que les étoiles ont réalisés des traînés importantes sur l'image. Le temps d'ouverture est très long, la vitesse d'obturation sera lente.*(On peut même ici déterminer, en mesurant la taille des traînée, que la durée de l'ouverture est de plusieurs heures.)*<br>
        *(ce qui est cohérent avec le fait que la luminosité est faible et que le nombre d'ouverture est grand.)*<br>

    2. Le temps d'ouverture important, fait que les étoiles (très lumineuses) ont le temps de se déplacé pendant la prise de vue. Ce mouvement apparent des étoiles est du à la rotation de la Terre, l'étoile qui se déplace le moins est celle qui est le plus proche de l'axe de rotation de la Terre, dans l'hémisphère sud (Namibie), il s'agit de L'étoile Sigma Octantis.
<br>


### Exercice 13 : 
<center>
<div class="grand">
    <figure>
        <img src="../images/exercices/exo13.png" />
        <figcaption>Manuel Numérique Hilbrt p147</figcaption>
    </figure>
</div>
</center>

??? faq correction "Correction"
    1. Pour que la profondeur de champ soit plus faibles, afin que le fond soit floue, il est nécessaire de modifier l'ouverture, il faut choisir un nombre d'ouverture plus petit.<br>
    2. Si on conserve le reste des réglages, alors la photo sera surexposé, car un nombre d'ouverture plus faible correspond à une ouverture plus grande, qui fait donc entrer plus de lumière.<br>
    3. Pour compenser, il est nécessaire de réduire la quantité de lumière, il faut augmenter la vitesse d'ouverture (réduire le temps de pause).
  
  


### Exercice 14 : 
<center>
<div class="grand">
    <figure>
        <img src="../images/exercices/exo14.png" />
        <img src="../images/exercices/exo14_2.png" />
        <figcaption>Manuel Numérique Hilbrt p147</figcaption>
    </figure>
</div>
</center>

??? faq correction "Correction"
    1. Un objectif grand angles est un objectif dont l'angle de champ est supérieur à 45°, alors qu'un téléobjectif est un objectif dont l'angle de champ est inférieur à 45°.<br>
    2. Les principales différences physiques entre ces deux objectifs, sont :<br>
    <ul>
        <li>la distance focale.</li>
        <li>l'angle de champ couvert.</li>
    </ul>
    3. Sur ces deux photographies, le grandissement du sujet est globalement identique (le portrait occupe la même place sur la photo, pour cela la distance entre le sujet et l'objectif n'est pas le même.), l'angle de champ et la profondeur de champ n'est pas la même.
    4. La photo 1 a été prise avec un téléobjectif (faible angle de champ), tandis que la seconde a été prise avec un objectif grand angles (grand angle de champ).


### Exercice 15 : 
<center>
<div class="grand">
    <figure>
        <img src="../images/exercices/exo15.png" />
        <figcaption>Manuel Numérique Hilbrt p147</figcaption>
    </figure>
</div>
</center>

??? faq correction "Correction"
    1. Un objectif grand angle est un objectif qui couvre un angle de champs supérieur à 45°.
    Un téléobjectif est un objectif qui couvre un angle de champs inférieur à 45°.

    2. L’objectif le plus adapter pour prendre des photos de loin est celui équipé d'un téléobjectif(, car une petite partie de ce que l'on voie occupera alors la totalité de l'image !).<br/>
    Plus la distance focale est grande plus l'angle de champs est petit.<br/>
    Ainsi, l'appareil qui possède un téléobjectif est l'appareil B (avec une focale à 150mm).

    3. Afin d'obtenir un arrière-plan flou, il faut diminuer la profondeur de champ, pour cela il faut fermer le diaphragme, c'est à dire augmenter le nombre d'ouverture.
   
    4. La fermeture du diaphragme, va réduire la quantité de lumière rentrant. Si l'on ne change aucun autre réglage, la photo va manquer de lumière, elle sera sous-exposé (sombre).
   
    5. Si l'on ne change pas la sensibilité, (c'est à dire le besoin en lumière), il faut s'arranger pour retrouver la quantité de lumière initiale, c'est à dire augmenter la quantité de lumière entrante. On ne peut jouer que sur la vitesse, il faut donc mettre un temps d'exposition plus long.


### Exercice 16 : 
<center>
<div class="grand">
    <figure>
        <img src="../images/exercices/exo16.png" />
        <img src="../images/exercices/exo16_2.png" />
        <figcaption>Manuel Numérique Hilbrt p147</figcaption>
    </figure>
</div>
</center>

??? faq correction "Correction"
    1. On nous indique que le nombre d'ouverture passe de N=4 à N=8. On augmente le nombre d'ouverture, ce qui correspond fermer un peu plus le diaphragme, les conséquences sont une diminution de la quantité de lumière entrante, l'image est sous exposée et une augmentation de la profondeur de champ.
    2. Si l'on ne change pas la sensibilité, (c'est à dire le besoin en lumière), il faut s'arranger pour retrouver la quantité de lumière initiale, c'est à dire augmenter la quantité de lumière entrante. On ne peut jouer que sur la vitesse, il faut donc mettre un temps d'exposition plus long.
    3. Si le photographe décide au contraire de jouer sur la sensibilité, il devra s'arrangé pour que cette quantité de lumière plus faible corresponde au besoin du capteur, le capteur devra donc être plus sensible, il faut augmenter la valeur ISO de la sensibilité.


### Exercice 17 : 
<center>
<div class="grand">
    <figure>
        <img src="../images/exercices/exo17.png" />
        <figcaption>Manuel Numérique Hilbrt p147</figcaption>
    </figure>
</div>
</center>

??? faq correction "Correction"
    1. Pour augmenter l'angle de champ, il faut diminuer la distance focale de l'objectif.<br/>
    2. Lorsque l'on augmente le nombre d'ouverture, on ferme le diaphragme, ce qui a pour effet d'augmenter la profondeur de champ.<br/>
    3. Lors du réglage, le photographe est passé d'un nombre d'ouverture de N=8 à N=11, on constate grâce au tableau que cela correspond a fermer le diaphragme d'une valeur de un stop.<br/>
    Pour compenser cette perte de luminosité, et obtenir la même exposition qu'initialement, le photographe devra augmenter le temps de pause d'une valeur de un stop. Initialement, il est au T = 1/125, il devra alors choisir le T=1/60.


### Exercice 18 : 
<center>
<div class="grand">
    <figure>
        <img src="../images/exercices/exo18.png" />
        <figcaption>Manuel Numérique Hilbrt p147</figcaption>
    </figure>
</div>
</center>

??? faq correction "Correction"
    1. (cf schéma cours)
    2. (cf schéma cours)
    3. Les questions restantes seront traitées dans le chapitre sur l'optique plus tard dans l'année.



### Exercice 19 : 
<center>
<div class="grand">
    <figure>
        <img src="../images/exercices/exo19.png" />
        <figcaption>Manuel Numérique Hilbrt p147</figcaption>
    </figure>
</div>
</center>

??? faq correction "Correction"
    1. Cette image est beaucoup trop *claire*, la quantité de lumière est trop importante, par rapport à la sensibilité de la surface photosensible, l'image est *sur-exposée*.
    2. Pour corriger ce manque de lumière, il faut diminuer la quantité de lumière entrante, le temps de pause devra être plus court.
    3. Pour corriger cette sur-exposition, le photographe peut aussi jouer sur l'ouverture.
    
        - En fermant le diaphragme (càd augmenter le nombre d'ouverture), mais dans ce cas la profondeur de champ sera augmenté.<br/>

        - En diminuant la sensibilité du capteur (valeur d'iso plus faible), la quantité de lumière reçu restera la même, mais le capteur étant moins sensible, la photo sera correctement exposé. L'effet sur l'image sera une diminution du bruit.<br/>





<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  

