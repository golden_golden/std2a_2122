---
title : "Révision"
pdf : "synthese"

---


<div class="noprint" style="text-align:right;width:100%;background-color:white">
    <a href="../pdf/{{page.meta.pdf}}.pdf" 
    style="display:inline-block" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="#a3a3a3" stroke-width="1.5" stroke-linecap="butt" stroke-linejoin="arcs"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg>
    </a>
</div>




# Chapitre 1 : Photographie


## 1. Les éléments essentiels d'un appareil photographique

<center>
    <div class="grand">
        <figure>
            <img src="../images/cours_appareil/appareils/appareil_photo.png" />
            <figcaption>Manuel numérique hilbrt p140</figcaption>
        </figure>
    </div>
</center>


### A. Éléments propre aux appareils photos réflexes

*Miroir :* baissé, il redirige les rayons lumineux vers le viseur par l'intermédiaire du prisme; levé il laisse passer les rayons lumineux directement vers le capteur. Ainsi, durant l'acquisition, il est
impossible d'utiliser le viseur.<br>

*Prisme :* il redirige les rayons lumineux vers le viseur.<br>

*Viseur :* il permet d'observer la scène.<br>

<center>
![Image title](../images/cours_appareil/appareils/pentaprism-pentax-forum-co-uk.webp){class="petit" loading=lazy }
![Image title](../images/cours_appareil/prisme/viseur-optique-oculaire-canon-7D2.jpg){class="petit" loading=lazy }
![Image title](../images/cours_appareil/viseur/viseur.jpg){class="mini" loading=lazy }
<figcaption> Prisme et viseur</figcaption>
</center>


### B. Objectif
C'est un système optique composé de lentilles et d'un diaphragme.<br/>
Un objectif est caractérisé par sa distance focale, son ouverture maximale et l'angle de champs maximal qu'il est capable de couvrir.

<br/>

<center>
![Image title](../images/cours_appareil/objectif/ouverture-explication-940x448.jpg){class="moyen" loading=lazy }
</center>

<br/>

#### B.1 Angle de champ
Focale et angle de champ sont interdépendants !
La distance focale comme l’angle de champ permettent de connaître la capacité d’un objectif à couvrir un sujet donné.

<center>
![Image title](../images/cours_objectif/2-schema-champ-de-vision.png){class="grand" loading=lazy }
![Image title](../images/cours_objectif/niveau-1-exemples-focale-2.jpg){class="grand" loading=lazy }
</center>

Quelque soit le format de l’appareil photo, un angle de champ de 45° correspond toujours à la focale normale !<br/>
Tous les objectifs d’angle de champ supérieurs à 45° sont des objectifs « Grand-Angle ».
Les objectifs d’un angle de champ inférieur à 45° correspondent à des longues focales ou « Télé-Objectif ».

<br/>

#### B.2 Distance de mise au point
L'objectif permet également de faire la mise au point. En tournant la bague de mise au point, on modifie le système optique de façon à ce que la distance entre le plans dans lequel est le sujet et la surface photosensible garantisse une image nette.*(Quelque soit l'ouverture choisit et la profondeur de champ)*.


<center>
![Image title](../images/cours_objectif/Croquis_PDC.jpg){class="grand" loading=lazy }
</center >



### C. Diaphragme
#### C.1 Définition
Le diaphragme permet de modifier l'ouverture de l'objectif.

!!! remarque "Ouverture"
    L’ouverture est l’orifice par lequel la lumière passe avant d’atteindre la surface photosensible. Elle est caractérisé par le nombre d'ouverture.<br/>
    Plus le diaphragme est fermé *(petit trou)*, plus la quantité de lumière transmise sera faible et plus le nombre d'ouverture est grand.

Par convention, la liste des nombres d'ouverture que l'on trouve sur les appareils photo est :

<center>
![Image title](../images/cours_expo_regl/ouverture/ouverture-de-diaphragme-2000x264.jpg){class="tresgrand" loading=lazy }
</center>


**Remarque :** Le passage d'une valeur à la suivante entraîne une division par deux de l'éclairement. *Il y a un stop entre chacune des valeurs !*.

<br>

#### C.2 Conséquences sur l'esthétique de l'image

!!! note "Ouverture et profondeur de champ"
    À focale et distance de mise au point constantes, la fermeture du diaphragme, c'est à dire l'augmentation du nombre d'ouverture, permet d'augmenter la profondeur de champ.
    
    <center>
    ![Image title](../images/cours_expo_regl/ouverture/reglage-ouverture-appareil-photo.JPG){class="grand" loading=lazy }
    </center>

<center>
![Image title](../images/cours_expo_regl/ouverture/prodondeur-de-champ-exemples.jpg){class="moyen" loading=lazy }
![Image title](../images/cours_expo_regl/ouverture/ouverture-prof2.webp){class="moyen" loading=lazy }
</center>









### D. Obturateur
#### D.1 Définition

Il masque la surface photosensible, contrôlant le temps d'exposition de ce dernier et donc la quantité de lumière reçu.<br>


<center>
![Image title](../images/cours_appareil/opturateur/page16pm.jpg){class="mini" loading=lazy }
![Image title](../images/cours_appareil/opturateur/obturateur-lent-940x533.jpg){class="mini" loading=lazy }
![Image title](../images/cours_appareil/opturateur/obturateur-rapide-940x533.jpg){class="mini" loading=lazy }
</center>



!!! Remarque "Vitesse d'obturation"
    La vitesse d’obturation mesure la durée pendant laquelle l’obturateur de l'appareil photo reste ouvert lors d'une prise de vue. On parle alors de *temps d'exposition* ou de *temps de pose*.<br/>
    Plus le temps d'expositions sera important, plus l'obturateur reste ouvert longtemps.


La vitesse d’obturation se mesure en secondes ou le plus souvent, en fractions de seconde.
Les valeurs de vitesse d'obturation couramment rencontrer sur un boîtier sont :

<center>
![Image title](../images/cours_expo_regl/tps_pause/temps.png){class="grand" loading=lazy }
</center>


**Remarque :** Le passage d'une valeur à la suivante entraîne une augmentation par deux de l'éclairement. *Il y a un stop entre chacune des valeurs !*.

<br>

#### D.2 Conséquences sur l'esthétique de l'image

Si l'appareil, ou les objects photographié se déplace suffisamment pendant le temps de pose, alors il seront flou.<br/>
Pour fixer le mouvement le temps de pose doit être suffisamment cours !<br/>
Cependant, le flou est un bon moyen de restitution l'impression du mouvement et par conséquent il peut être recherché.<br/>

On distingue différents flou liée au mouvement :

- le flou due au mouvement du sujet
- le flou due au mouvement de l’appareil
- une combinaison de mouvement du sujet et de l’appareil


<br>


### E. Surface photosensible
#### E.1 Définition

Quelques soit la nature de la surface photosensible (capteur, pellicule…) elle est caractérisées par une sensibilité :

!!! def "Définition"
    La sensibilité ISO, c’est la sensibilité du capteur à la lumière.<br>
    Plus la valeur est grande plus la surface photosensible est sensible, elle aura besoin d'une moins grande quantité de lumière pour donner une exposition correcte.



Les valeurs d’ISO standard en photo que l’on trouve sur les appareils photos en règle générale, sont :


<center>
![Image title](../images/cours_expo_regl/iso/echelle_iso.png){class="grand" loading=lazy }
<figcaption>Échelle de valeurs ISO</figcaption>
</center>


**Remarque :** Le passage d'une valeur à la suivante entraîne une division par deux de l'éclairement. *Il y a un stop entre chacune des valeurs !*.

<br>

#### E.2 Conséquences sur l'esthétique de l'image

Une sensibilité élevée facilite la photographie en basse lumière, mais le rendu de l'image est lui aussi modifié. En argentique, le grain de l'image est plus prononcé et en Numérique le bruit est également plus prononcé.

<center>
![Image title](../images/cours_expo_regl/iso/Sensibilite-ISO-Photo-de-nuit.jpg){class="moyen" loading=lazy }
<figcaption>Monté du bruit numérique avec l'élévation des ISO</figcaption>
</center>




## 2. L'exposition
### 2.1 Définitions

!!! def "Qu'est ce que l'exposition"
    C'est la quantité totale de lumière reçu par la surface photosensible de l'appareil pendant la prise de vue.



Notre but en photo est de régler cette quantité de lumière afin d'avoir une bonne exposition.<br><br>


!!! def "Surexposée"
    Une photo est dite surexposée lorsqu'elle à reçu une quantité de lumière trop importante.<br>
    Une photo surexposée est trop claire, les hautes lumières apparaissent uniformément blanches, ou « brûlées, l'information y est perdu.


!!! def "sous-exposée"
    Une photo est dite sous-exposée lorsqu'elle à reçu une quantité de lumière trop faible.<br>
    Une photo surexposée est trop sombre, elle un manque de détail dans les ombres, qui apparaissent noires ou « bouchées » de l'information (du détail) est perdu dans les basses lumières.


<center>
![Image title](../images/cours_expo_regl/exposition/exposition.jpg){class="grand" loading=lazy }
</center>


L'exposition dépend de quatre paramètres :

- La luminosité de la scène que l'on souhaite photographier.
<br/>
  
- La sensibilité de la surface photosensible, les ISO (pour un capteur numérique).
- L'ouverture.
- La vitesse d'obturation (ou le temps de pause).



Sur le boîtier, il est possible d'intervenir uniquement sur les trois derniers paramètres !


### 2.2 Bonne exposition, l'analogie du verre d'eau

Pour mieux comprendre l'exposition et le lien entre les différents paramètres, on peut recourir à  l'analogie du remplissage d'un verre d'eau. Dans cette analogie :

- L'eau représente la lumière.
- La taille du verre représente la sensibilité de la surface photosensible, plus le verre est petit plus la quantité d'eau pour le remplir est faible, ce qui correspond à une sensibilité plus importante, soit une valeur d'ISO plus importante.
- Le temps pendant lequel le robinet sera ouvert (l’eau coule) va représenter le temps de pose (également appelée vitesse d’obturation), la durée pendant laquelle la lumière passe par l’obturateur pour impressionner le capteur.
- L’ouverture plus ou moins grande du robinet (le débit d’écoulement) représente l’ouverture du diaphragme (le trou plus ou moins grand par lequel passe la lumière lorsque l’obturateur est ouvert).

Dans cette analogie, une exposition correcte correspond à un verre rempli normalement, un verre peu rempli correspond alors à une sous-exposition et un verre qui déborde à une sur exposition.


<br/>

Une même quantité d’eau pourra être obtenue soit en ouvrant grand le robinet pendant peu de temps (le fort débit permet de rapidement fermer le robinet), soit en ouvrant très peu le robinet mais pendant plus longtemps (un faible débit nécessitera plus de temps pour remplir le verre).

En traduisant en termes photographiques, une même exposition pourra être obtenue soit en ouvrant grand le diaphragme pendant un temps de pose court, soit en ouvrant très peu le diaphragme pendant un temps de pose plus long.

<center>
![Image title](../images/cours_expo_regl/analogie/exposition.png){class="moyen" loading=lazy }
![Image title](../images/cours_expo_regl/analogie/source.gif){class="moyen" loading=lazy }

</center>

<br>

### 2.3 Plusieurs choix possibles ! 
L’exposition fonctionne selon le principe de réciprocité : augmenter la durée de l’exposition nécessite de fermer le diaphragme pour que la quantité de lumière atteignant la surface sensible reste identique.

Si l'on augmente un paramètre d'une valeur de un stop, alors l'exposition sera conservé, en diminuant un autre paramètre d'un valeur de un stop.

On comprend alors que pour exposé correctement une photographie, plusieurs trio (la vitesse d’obturation, l’ouverture et la sensibilité ISO) sont envisageables.<br>
*Dès lors que l'on connaît une exposition correcte, la notion de stop nous permet de trouver toutes les autres combinaisons possibles !*

Cependant, bien que l'exposition soit la même pour l'ensemble des couples possibles, le rendue de l'image lui sera différents !<br>
*C'est le rendu, c'est-à-dire le choix esthétique fait par le photographe qui permet de sélectionner le bon trio !*

Ainsi l'ordre des réglages est généralement le suivant :<br>

- On choisi les ISO, de façon à ne pas avoir de bruit numérique visible sur l'image.
  
- Si le sujet photographié est en mouvement et que c'est ce que l'on veut restituer, on choisi la vitesse. Puis on règle l'ouverture correspondante.

- Si le sujet photographié, n'a pas un mouvement important, et que l'on veux isoler grâce a un faible profondeur de champ ou au contraire une très grande profondeur de champ, comme en photo de paysage, on choisi l'ouverture. Puis on règle la vitesse correspondante.






## 3. L'œil fonctionnent t'il comme un appareils photographiques ?

<center>
![Image title](../images/cours_appareil/œil/camera-eye-comparison.jpg){class="moyen" loading=lazy }
![Image title](../images/cours_appareil/œil/oeuil_camera1.jpg){class="moyen" loading=lazy }
<figcaption> Parallèle œil, appareil photo</figcaption>
</center>


