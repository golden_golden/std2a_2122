---
title : Exercices photographie
correctionvisible : false
pdf : "exercices"
---


<div class="noprint" style="text-align:right;width:100%;background-color:white">
    <a href="../pdf/{{page.meta.pdf}}.pdf" 
    style="display:inline-block" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="#a3a3a3" stroke-width="1.5" stroke-linecap="butt" stroke-linejoin="arcs"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg>
    </a>
</div>



# Exercices sur la photographie


## L'exposition en photographiques, et ses conséquences sur l'image


### Exercice 11 : 
<center>
<div class="grand">
    <figure>
        <img src="../images_exercices/exo11.png" />
        <figcaption>Manuel Numérique Hilbrt p147</figcaption>
    </figure>
</div>
</center>

!!! faq correction "Correction"
    1.  
  

### Exercice 12 : 
<center>
<div class="grand">
    <figure>
        <img src="../images_exercices/exo12.png" />
        <img src="../images_exercices/exo12_2.png" />
        <figcaption>Manuel Numérique Hilbrt p147</figcaption>
    </figure>
</div>
</center>

!!! faq correction "Correction"
    1. On c


### Exercice 13 : 
<center>
<div class="grand">
    <figure>
        <img src="../images_exercices/exo13.png" />
        <figcaption>Manuel Numérique Hilbrt p148</figcaption>
    </figure>
</div>
</center>

!!! faq correction "Correction"
    1. On 
  

### Exercice 14 : 
<center>
<div class="grand">
    <figure>
        <img src="../images_exercices/exo14.png" />
        <img src="../images_exercices/exo14_2.png" />
        <figcaption>Manuel Numérique Hilbrt p148</figcaption>
    </figure>
</div>
</center>

!!! faq correction "Correction"
    1.


### Exercice 15 : 
<center>
<div class="grand">
    <figure>
        <img src="../images_exercices/exo15.png" />
        <figcaption>Manuel Numérique Hilbrt p148</figcaption>
    </figure>
</div>
</center>

!!! faq correction "Correction"
    1. 
  
  

### Exercice 16 : 
<center>
<div class="grand">
    <figure>
        <img src="../images_exercices/exo16.png" />
        <img src="../images_exercices/exo16_2.png" />
        <figcaption>Manuel Numérique Hilbrt p148</figcaption>
    </figure>
</div>
</center>

!!! faq correction "Correction"
    1. "L


### Exercice 17 : 
<center>
<div class="grand">
    <figure>
        <img src="../images_exercices/exo17.png" />
        <figcaption>Manuel Numérique Hilbrt p149</figcaption>
    </figure>
</div>
</center>

!!! faq correction "Correction"
    1. "L


### Exercice 18 : 
<center>
<div class="grand">
    <figure>
        <img src="../images_exercices/exo18.png" />
        <figcaption>Manuel Numérique Hilbrt p149</figcaption>
    </figure>
</div>
</center>

!!! faq correction "Correction"
    1. "L


### Exercice 19 : 
<center>
<div class="grand">
    <figure>
        <img src="../images_exercices/exo19.png" />
        <figcaption>Manuel Numérique Hilbrt p149</figcaption>
    </figure>
</div>
</center>

!!! faq correction "Correction"
    1. "L



















<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  

