---
title : "Cours : La balance des blanc"
pdf : 
---


<div class="noprint" style="text-align:right;width:100%;background-color:white">
    <a href="../pdf/{{page.meta.pdf}}.pdf" 
    style="display:inline-block" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="#a3a3a3" stroke-width="1.5" stroke-linecap="butt" stroke-linejoin="arcs"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg>
    </a>
</div>




# Chapitre 5 : La balance des blancs



??? note "Ressources"
    - [Théo Mandard_La BALANCE des BLANCS : comment la RÉGLER ? (1/2)](https://www.youtube.com/watch?v=7jpSMMq7KBM)
    - [Théo Mandard_La BALANCE des BLANCS : Comment la CORRIGER en post-traitement ? (2/2) - Tuto Premiere Pro](https://www.youtube.com/watch?v=Gqx6Z-NkxgU)
    - [Déclic numérique photographie_Parlon-en : la balance des blancs](https://www.youtube.com/watch?v=D6z5wNGVLJQ)
    - [ObjecifPhotographe _ Photo Numérique - La Balance des Blancs](https://www.youtube.com/watch?v=GorywvVJzw0)
    - [Sonick photographie_jouez avec la balance des blancs !](https://www.youtube.com/watch?v=0vj2lpCM9iE)
    - [Cineastuces_la température de couleur_la balance des blancs](https://www.youtube.com/watch?v=25tigZhY6Wk)



[darktable TUTO N° 23 : Balance des blancs et Calibration des couleurs.](https://www.youtube.com/watch?v=tK2Bjn8FZfQ)
    