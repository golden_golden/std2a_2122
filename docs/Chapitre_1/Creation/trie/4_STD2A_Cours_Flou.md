---
title : "Cours : Le flou en photographie"
pdf : cours_photographie
---


<div class="noprint" style="text-align:right;width:100%;background-color:white">
    <a href="../pdf/{{page.meta.pdf}}.pdf" 
    style="display:inline-block" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="#a3a3a3" stroke-width="1.5" stroke-linecap="butt" stroke-linejoin="arcs"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg>
    </a>
</div>




# Chapitre 5 : Le flou en photographie




## 1. L'importance du flou en photographie


## 2. Flou de profondeur de champs
### 2.1 
boeked tournant,
### 2.2 
#### A. Quelques exemples
#### B. Comment l'obtenir

## 3. Flous cinétiques
### 3.1 Flou cinétique, due aux mouvements des objets photgraphié.
#### A. Quelques exemples
Robert doisneau : baiser de l'Hotel de ville https://www.superprof.fr/blog/images-legendaires-et-grands-photographes/
J Day in Times Square d’Alfred Eisenstaedt . https://www.superprof.fr/blog/images-legendaires-et-grands-photographes/
#### B. Comment l'obtenir

### 3.2 Flou cinétique, due aux mouvements du photographe.
#### A. Quelques exemples
Robert capa, D-Day
David siodos : http://davidsiodos.com/fr/accueil
Emanuele Scorcelletti : https://crowdfunding.hemeria.com/fr/project/elegia-fantastica-emanuele-scorcelletti/



#### B. Comment l'obtenir


### 4. Flou due à des filtres optiques
#### A. Quelques exemples
(pluie,brouillard, morceaux de verre, gras sur l'objectif,)
pluie : https://crowdfunding.hemeria.com/fr/project/eaux-fortes-christophe-jacrot/
Morceaux de verre : David siodos http://davidsiodos.com/fr/portfolio-29946-peripherique
toile : David siodos http://davidsiodos.com/fr/portfolio-29946-peripherique
Vitre 
#### B. Comment l'obtenir


### 5. Flou liée aux support d'expositions
#### A. Quelques exemples
Laurent lafolie : https://www.laurentlafolie.photography/
#### B. Comment l'obtenir


### 6. L'illusion du flou
#### A. Quelques exemples
œil canard (vincent munier)


### 7. Absence de flou
Thomas Crauwels : https://hemeria.com/produit/above-thomas-crauwels/
Inside Views – Floriane de Lassée : https://crowdfunding.hemeria.com/fr/project/inside-views-floriane-de-lassee/



### 1.1 Quelques appareils photographiques
<center>
    <div class="grand">
        <figure>
            <img src="../images/Cours_photo/camera.jpg" />
            <figcaption>Appareils photographiques dans l'histoire</figcaption>
        </figure>
    </div>
</center>


??? note "Ressources"
    lien
    https://www.ericcanto.com/photographe-celebre-14-exemples/

    