---
title : "Cours : La photographie"
pdf : cours_photographie
---


<div class="noprint" style="text-align:right;width:100%;background-color:white">
    <a href="../pdf/{{page.meta.pdf}}.pdf" 
    style="display:inline-block" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="#a3a3a3" stroke-width="1.5" stroke-linecap="butt" stroke-linejoin="arcs"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg>
    </a>
</div>




# Chapitre 5 : La photographie




??? info "Dans le programme"
    **Thème 1** : Connaître et transformer les matériaux

    | **Notions et contenus**                                                                                         | **Capacités exigibles**                                                                                 |
    | --------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------- |
    | Familles de matériaux.                                                                                          | - Distinguer les grandes classes de matériaux utilisés dans lesdomaines du design et des métiers d’art. |
    | Propriétés physiques des matériaux : masse volumique, élasticité, conductivité thermique,absorption acoustique. | - Comparer expérimentalement des *caractéristiques physiques de différents matériaux*.                  |
 

??? info "Compétences travaillées"
    




<br>

## 1. Les appareils photographiques
### 1.1 Quelques appareils photographiques
<center>
    <div class="grand">
        <figure>
            <img src="../images/Cours_photo/camera.jpg" />
            <img src="../images/Cours_photo/Camera4.jpg" />
            <figcaption>Appareils photographiques dans l'histoire</figcaption>
        </figure>
    </div>
</center>

Quelques soit l'appareil photo, on retrouve dans sa constitutions les éléments essentiel à son fonctionnement. Mais quels sont ces élements ?


### 1.2 Les éléments essentiels d'un appareil photograpique
<center>
    <div class="grand">
        <figure>
            <img src="../images/Cours_photo/appareil_photo.png" />
            <figcaption>Manuel numérique hilbrt p140</figcaption>
        </figure>
    </div>
</center>


#### A. Objectif
C'est un système optique composé de lentilles et d'un diaphragme.<br>
Un objectif est caractérisé par sa distance focale, son ouverture maximale et l'angle de champs maximal qu'il est capable de couvrir.<br>

<br>

#### B. Diaphragme
Le diaphragme conditionne la quantité de lumière transmise par son ouverture. Plus celle-ci sera importate, plus la quantité de lumière transmise sera importante.<br>

<center>
    <div class="moyen">
        <figure>
            <img src="../images/Cours_photo/diaf.png" />
            <img src="../images/Cours_photo/diaf1.png" />
            <img src="../images/Cours_photo/diaf2.jpg" />
            <img src="../images/Cours_photo/diaf5.jpg" />
            <img src="../images/Cours_photo/schema-ouverture-phototrend-940x340.jpg" />
            <img src="../images/Cours_photo/" />
            <img src="../images/Cours_photo/" />
            <figcaption>Diaphragme</figcaption>
        </figure>
    </div>
</center>

<br>

#### C. Obturateur
Il masque la surface photosensible, contrôlant le temps d'exposition de ce dernier et donc la quantité de lumière reçu.<br>

https://phototrend.fr/2016/03/mp-170-lobturateur-photo-mecanique-electronique/

https://www.lesnumeriques.com/photo/comprendre-l-obturation-electronique-pu120475.html


http://vivre-de-la-photo.fr/que-choisir-entre-obturateur-mecanique-et-electronique/
<br>

#### D. Surface photosensible
Dans les temps ancien, on utilisé des plaques de verre ou de métal sur lesquelles une émulsion photosensible était appliquée.<br>
Un peu plus tard, la surface photosensible à été appliquée sur un support de triacétate de cellulose, la pellicule photographique.<br>

<center>
    <div class="moyen">
        <figure>
            <img src="../images/Cours_photo/couches-film-noir-et-blanc.jpg" />
            <figcaption> Composition d'une pélicule photo</figcaption>
        </figure>
    </div>
</center>

Désormé, les appareils photographiques utilisent un capteur.<br>
Le capteur est composé d'une matrice de cellules photoélectriques qui convertissent l'énergie électromagnétique de la lumière en un signal électrique qui est ensuite traité pour obtenir une image.<br>


<br>

#### E. Éléments propre aux appareils photos réflexes
*Miroir :* baissé, il redirige les rayons lumineux vers le viseur par l'intermédiaire du prisme; levé il laisse passer les rayons lumineux directement vers le capteur. Ainsi, durant l'acquisition, il est
impossible d'utiliser le viseur.<br>

<br>

*Prisme :* il redirige les rayons lumineux vers le viseur.<br>

<br>

*Viseur :* <br>

<br>

## 2. Objectifs 
### 2.1 Qu'est ce qu'un objectifs
<center>
    <div class="moyen">
        <figure>
            <img src="../images/Cours_photo/cut-lens.jpg" />
            <img src="../images/Cours_photo/pro-infobank-lenses-multi-layer-do-element-6_3338977736f746a7ae61a4f1d8deff77.jpg" />
            <img src="../images/Cours_photo/pro-infobank-lenses-multi-layer-do-element-8_5c849a87943343b39a292628b0717968.jpg" />
            <figcaption> Coupe d'un objectif</figcaption>
        </figure>
    </div>
</center>
<center>
    <div class="moyen">
        <figure>
            <img src="../images/Cours_photo/" />
            <figcaption> Schéma d'une lentille convergente</figcaption>
        </figure>
    </div>
</center>

<br>

Un objectif peut être modélisé par une lentille convergente unique.<br>

Mais pourquoi est-il constitué d'autant de lentille ?

Un objectif est composé de plusieurs lentilles, généralement assemblés en groupe pour :

- Corriger les abérations optiques.
- Réduire l'encombrement (téléobjectif).



??? note "Les abérations des système optiques"
    **Qu'est ce que l'abération chromatique ?**
    <center>
    <div class="tresgrand">
        <figure>
            <img src="../images/en cour/Abération_Chromatique.png" />
        </figure>
    </div>
    </center>

    <br>


    **Qu'est ce que l'abération géométrique ?**
    <center>
    <div class="tresgrand">
        <figure>
            <img src="../images/en cour/Abération_géométrique.png" />
        </figure>
    </div>
    </center>


### 2.2 Angle de champ
#### A. Notion d'angle de champ
<center>
    <div class="grand">
        <figure>
            <img src="../images/Cours_photo/focale_et_angle_de_champ.png" />
            <img src="../images/Cours_photo/649467.webp" />
*           <img src="../images/Cours_photo/niveau-1-exemples-focale-2.jpg" />
            <figcaption> à faire</figcaption>
        </figure>
    </div>
</center>

#### B. Influance de la focale

<center>
    <div class="grand">
        <figure>
            <img src="../images/Cours_photo/2-schema-champ-de-vision.png" />
            <img src="../images/Cours_photo/angle de champ.jpg" />
            <img src="../images/Cours_photo/aov.png" />
            <img src="../images/Cours_photo/longeur-focale-mm.jpg" />
            <img src="../images/Cours_photo/focale-et-angle-cadrage-par-focale-en-24x36-1.jpg" />
            <img src="../images/Cours_photo/15_objectifs.jpg" />
            <img src="../images/Cours_photo/Comment-choisir-un-objectif-photo-en-5-étapes2.png" />
            <img src="../images/Cours_photo/Focale_champs-1024x444.jpg" />
            <img src="../images/Cours_photo/Focales1.jpg" />
            <img src="../images/Cours_photo/focal-length-mm3.webp" />
            <img src="../images/Cours_photo/formation_prof_de_champ_bm_7.gif" />
            <img src="../images/Cours_photo/objectif02.jpg" />
            <img src="../images/Cours_photo/" />
            <figcaption> à faire</figcaption>
        </figure>
    </div>
</center>


#### C. Influance de taille de la surface photosensible

<center>
    <div class="grand">
        <figure>
            <img src="../images/Cours_photo/artfichier_805457_5621925_201604144023694.jpg" />
            <img src="../images/Cours_photo/capteurs.jpg" />
            <img src="../images/Cours_photo/Capteurs.jpg" />
            <img src="../images/Cours_photo/ind6546ex.jpg" />
            <img src="../images/Cours_photo/les-tailles-de-capteurs-et-les-focales-equivalentes-2fa74ed6__w380.jpg" />
            <img src="../images/Cours_photo/schema-angle-de-champ.png" />
            <figcaption> à faire</figcaption>
        </figure>
    </div>
</center>


## 3. L'exposition

### 3.1 Comprendre l'exposition
<center>
    <div class="tresgrand">
        <figure>
            <img src="../images/Cours_photo/diafragma-3.jpg" />
            <figcaption> à faire</figcaption>
        </figure>
    </div>
</center>


<a href="https://www.youtube.com/watch?v=V20odwEd5jE" >Apprendre la photo</a>

<a href="https://www.youtube.com/watch?v=WYhxNaQ4n-k" >Les Essentiels PHOTO</a>

<a href="https://www.youtube.com/watch?v=jzjxWMV_Srw" >Les Essentiels PHOTO_mode manuel</a>

https://www.youtube.com/watch?v=7K_tHmjNhac
https://www.youtube.com/watch?v=aaHrtSMDXdk

https://www.youtube.com/watch?v=4INmE42yAZk
<br>

#### A. La scéne à photographier
La mesure de lumière
<a href="https://www.youtube.com/watch?v=5C49gnbzT_">Les essentiels Photo_La mesure de lumière simplement</a>
<a href="https://www.youtube.com/watch?v=8onRJ6qJdpU">clic-clac_comment fonctionne un Posemètre</a>
https://www.youtube.com/watch?v=vZo-FykYWj0


#### B. Sensibilité ISO :
<a href="https://www.youtube.com/watch?v=EwHdCWmojDU&list=PLVGCy9QVeRNXyWXamf9h5fbFslJnaD2C9" >Apprendre la photo</a>

https://www.youtube.com/watch?v=EBO3B0OTTtI

<br>

#### C. L'ouverture
<a href="https://www.youtube.com/watch?v=v_W8O1U4qRU" >Apprendre la photo</a>

<a href="https://phototrend.fr/2016/03/mp-169-ouverture-et-fstop-en-finir-avec-la-confusion/" >phototrend</a>

https://www.youtube.com/watch?v=9B5dwfhNtAk

<center>
    <div class="grand">
        <figure>
            <img src="../images/Cours_photo/Ouverture-photo-le-diaphragme.png" />
            <img src="../images/Cours_photo/" />
            <img src="../images/Cours_photo/" />
            <img src="../images/Cours_photo/" />
            <img src="../images/Cours_photo/" />
            <figcaption> à faire</figcaption>
        </figure>
    </div>
</center>

<br>

#### D. La vitesse d'obturation
<a href="https://www.youtube.com/watch?v=pD45CIQx3Ks" >Apprendre la photo</a>

<a href="https://www.youtube.com/watch?v=_WPJJYFNtRg" >Cours-de-photographie</a>


<br>




### 3.2 Influances des choix effectué pour l'exposition sur le rendu de l'image
#### A. Plusieurs choix possibles !
<center>
    <div class="grand">
        <figure>
            <img src="../images/Cours_photo/fb736a89993f8ddfd43b9e85732ee38d.jpg" />
            <img src="../images/Cours_photo/ouverture-vitesse-dobturation-iso.jpg" />
            <img src="../images/Cours_photo/" />
            <img src="../images/Cours_photo/" />
            <img src="../images/Cours_photo/" />
            <img src="../images/Cours_photo/" />
            <figcaption> à faire</figcaption>
        </figure>
    </div>
</center>




#### B. Influence de la sensibilité



#### C. Influence de l'ouverture
***La profondeur de champ***

<a href="https://phototrend.fr/2012/02/mp-100-la-profondeur-de-champ/" >Phototrend</a>

<a href="https://www.youtube.com/watch?v=RX2HuS0i6ys" >Les Essentiels PHOTO_La profondeur de champ</a>

<center>
    <div class="grand">
        <figure>
            <img src="../images/Cours_photo/anti-sèche_ouverture_profondeur.png" />
            <img src="../images/Cours_photo/distance-focus-profondeur-de-champ.jpg" />
            <img src="../images/Cours_photo/instru-ap-profondeur.png" />
            <img src="../images/Cours_photo/la-distance-influence-la-profondeur-de-champ-1.jpg" />
            <img src="../images/Cours_photo/prodondeur-de-champ-exemples.jpg" />
            <img src="../images/Cours_photo/profondeur-de-champ-comparaison-selon-ouvertures-731x548.jpg" />
            <img src="../images/Cours_photo/profondeur-de-champ-f11-768x254-1.jpg" />
            <img src="../images/Cours_photo/profondeur-de-champ-ouverture-de-diaphragme.jpg" />
            <img src="../images/Cours_photo/profondeur-d-ouverture-de-champ-de-explication-infographic-75823732.jpg" />
            <img src="../images/Cours_photo/reglage-ouverture-appareil-photo.JPG" />
            <figcaption> à faire</figcaption>
        </figure>
    </div>
</center>

<center>
    <div class="grand">
        <figure>
            <img src="../images/Cours_photo/shema-profondeur-de-champ-focales-e1490008578199.jpg" />
            <figcaption> à faire</figcaption>
        </figure>
    </div>
</center>


#### D. Influence de la vitesse d'obturation
<center>
    <div class="grand">
        <figure>
            <img src="../images/Cours_photo/ima654654ge.jpg" />
            <img src="../images/Cours_photo/" />
            <img src="../images/Cours_photo/" />
            <img src="../images/Cours_photo/" />
            <img src="../images/Cours_photo/" />
            <figcaption> à faire</figcaption>
        </figure>
    </div>
</center>



??? note "Ressources"
    lien
    https://emmanuelgeorjon.com/photo/technique-photo-vitesse-dobturation/


    mesurer une vitesse d'obturation
    http://t.hacquard.free.fr/site2/testobtu1.html


    cercle image et angles de champs
    https://galerie-photo.com/cercle-image-et-angle-de-champ.html


    même grandissement angle de champ différent
    http://www.1point2vue.com/focale-cadrage/



??? note "Autres ressources"
    -démarche photographique-lumière : https://www.youtube.com/watch?v=L0pS-JpFfcA
    - utiliser le flashmètre/posemètre : https://www.youtube.com/watch?v=t5J7DukCago