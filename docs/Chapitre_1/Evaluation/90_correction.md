---
title : "Correction Éval"
correctionvisible : true
pdf : "Correction éval"
---


<div class="noprint" style="text-align:right;width:100%;background-color:white">
    <a href="../pdf/{{page.meta.pdf}}.pdf" 
    style="display:inline-block" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="#a3a3a3" stroke-width="1.5" stroke-linecap="butt" stroke-linejoin="arcs"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg>
    </a>
</div>



# Correction évaluation



<center>
<div class="tresgrand">
    <figure>
        <img src="../Evaluation/Éval_2/correction/1.png" />
        <img src="../Evaluation/Éval_2/correction/2.png" />
        <img src="../Evaluation/Éval_2/correction/3.png" />
        <img src="../Evaluation/Éval_2/correction/4.png" />
    </figure>
</div>
</center>

### Exercice 4 : 
<center>
<div class="grand">
    <figure>
        <img src="../images/exercices/exo14.png" />
        <img src="../images/exercices/exo14_2.png" />
        <figcaption>Manuel Numérique Hilbrt p147</figcaption>
    </figure>
</div>
</center>


1. Un objectif grand angles est un objectif dont l'angle de champ est supérieur à 45°, alors qu'un téléobjectif est un objectif dont l'angle de champ est inférieur à 45°C.<br><br>
2. Les principales différences physiques entre ces deux objectifs, sont :<br>
    <ul>
        <li>la distance focale.</li>
        <li>l'angle de champ couvert.</li>
    </ul>
    <br>
3. Sur ces deux photographies, le grandissement du sujet est globalement identique (le portrait occupe la même place sur la photo, pour cela la distance entre le sujet et l'objectif n'est pas le même.).<br><br>
4. La photo 1 a été prise avec un téléobjectif (faible angle de champ) tandis que la seconde a été prise avec un objectif grand angles (grand angle de champ).






<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  


