---
title : "Cours 0 : Rappels de seconde"
pdf : 
---


<div class="noprint" style="text-align:right;width:100%;background-color:white">
    <a href="../pdf/{{page.meta.pdf}}.pdf" 
    style="display:inline-block" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="#a3a3a3" stroke-width="1.5" stroke-linecap="butt" stroke-linejoin="arcs"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg>
    </a>
</div>





# Chapitre 5 :  Lumière et diôptres

    
!!! note "Pas au controle !"
    Cette partie reprend en grande partie les notions de cours vue en seconde, et permet de comprendre comment fonctionne une lentille ! <br>
    *Cepandant, elle n'est pas vraiment au programme de 1<sup>er</sup> STD2A, par conséquent, il n'y aurra pas de questions relatives à cette partie à l'évaluation !*

<br>

## 1. Nature de la lumière
    
## 2. Notion de rayon de lumière

## 3. Indice de réfraction

## 4. Lois de Snell-Descartes
### 4.1 Mise en évidence du phénomène et vocabulaire
### 4.2 Réflexion
### 4.3 Réfraction
### 4.4 Dispersion



??? note "Ressources"
    -----
    - <a href="https://dlatreyte.github.io/premieres-pc/chap-5/4-formation-images/">*https://dlatreyte.github.io/*</a><br>
    - <a href="https://femto-physique.fr/optique/pdf/book-optgeo.pdf">*femto-physique.fr*</a>
    ----


    ## Cours préalable
    -[loi optique géo](https://www.physagreg.fr/optique-11-lois-optique-geometrique.php)<br>

    ## cours complet
    -[cours d'optique](https://dlatreyte.github.io/premieres-pc/chap-5/4-formation-images/)<br>
    -[cours d'optique_femto](https://femto-physique.fr/optique/pdf/book-optgeo.pdf)<br>
    -[fiche révision lycée militaire aix](https://www.lycee-militaire-aix.fr/site/images/conseils%20CPGE/mpsi/physique/revision_optique_cours.pdf)<br>
    -[cours l1 et l2](https://www.univ-usto.dz/images/coursenligne/Cours_Optique-Beldjilali.pdf)<br>
    -[lien optique lentille convergente](https://coyote-physique.e-monsite.com/pages/specialite-1ere/lentille.html)<br>


    ## vidéo
    ### cours complet<br>
    -[Cours prépa scientifique. L'Optique géométrique.](https://www.youtube.com/watch?v=tPDmwX7g72U)<br>
    -[Introduction à l'optique géométrique en L1 - cours 1_Richard Taillet](https://www.youtube.com/watch?v=dckvoph10p4)<br>
    -[Vidéo - Chapitre 01 Rappels elementaire optique](https://www.youtube.com/watch?v=xDF4U_74aTc)<br>

    ### morceau choisi
    -[Qu'est-ce qu'une lentille convergente ? Divergente ? | Physique-Chimie](https://www.youtube.com/watch?v=ei6JE-Pte1s)<br>

    -[Lentille mince convergente | Seconde | Physique-Chimie](https://www.youtube.com/watch?v=7rjeXimQGfk)<br>

    -[Méthode : connaître les lentilles minces convergentes en optique](https://www.youtube.com/watch?v=c9XsoKxKqek)<br>
    -[Les lentilles : introduction](https://www.youtube.com/watch?v=B1_UM3LQ0s4)<br>

