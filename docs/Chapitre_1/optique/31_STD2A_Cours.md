﻿---
title : "Cours 3 : Optique géométrique"
pdf : 
---


<div class="noprint" style="text-align:right;width:100%;background-color:white">
    <a href="../pdf/{{page.meta.pdf}}.pdf" 
    style="display:inline-block" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="#a3a3a3" stroke-width="1.5" stroke-linecap="butt" stroke-linejoin="arcs"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg>
    </a>
</div>



# Chapitre 5 : Optique géométrique



??? info "Dans le programme"
    **Thème 1** : Connaître et transformer les matériaux

    | **Notions et contenus**                                                                                         | **Capacités exigibles**                                                                                 |
    | --------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------- |
    | Familles de matériaux.                                                                                          | - Distinguer les grandes classes de matériaux utilisés dans lesdomaines du design et des métiers d’art. |
    | Propriétés physiques des matériaux : masse volumique, élasticité, conductivité thermique,absorption acoustique. | - Comparer expérimentalement des *caractéristiques physiques de différents matériaux*.                  |
 

??? info "Compétences travaillées"
    


## 1. Les lentilles minces
### 1.1 Qu'est ce qu'une lentille mince ?
<center>
    <div class="grand">
        <figure>
            <img src="../images/cours_optique/lentilles.png" />
            <figcaption>Photo de différentes lentilles</figcaption>
        </figure>
    </div>
</center>

!!! def "Définition : Lentille mince"
    Une lentille mince est formée par l’association de deux diôptres sphériques de grand rayon de courbure par rapport à l’épaisseur de la lentille.<br>

??? note "Remarques"
    Un des diôptres peut être plan, il s'agit alors d'un diôptre sphérique de rayon de courbure infini.<br>



??? note "Complément"
    Si l’on note $R_1$, $R_2$ les rayons de courbure, $C_1$, $C_2$ les centres de courbure et $e$ l’épaisseur de la lentille.
    <center>
        <div class="grand">
            <figure>
                <img src="../images/cours_optique/definitionLentille.png" />
                <figcaption>Lentille mince - <a href="https://femto-physique.fr/optique/pdf/book-optgeo.pdf"  target="_blank">*femto-physique.fr*</a></figcaption>
            </figure>
        </div>
    </center>
    <br>
    <center>$e \ll R_1$ &nbsp;, &nbsp;  $e \ll R_2$ &nbsp;  et  &nbsp; $e \ll C_1C_2$</center>
    <br>
    Dans l’approximation des lentilles minces, les sommets $S_1$ et $S_2$ sont considérés confondus en un point $O$ appelé centre optique.<br>


Une lentille mince possède un axe de symétrie appelé axe optique.

<br>

### 1.2 Effet d'une lentille sur le trajet des rayons de lumières


??? note "Complément"
    On note $n$ l’indice du milieu constituant la lentille, $n>1$. On considèrera que les lentilles sont plongées dans l’air d’indice $n'\simeq 1$.<br>

    À chacun de ses surfaces la lumière est réfractée, une lentille réfracte donc la lumière deux fois :<br>

    - à l’interface *air-matériau* (lorsque la lumière entre dans la lentille) ;<br>
    - à l’interface *matériau-air* (lorsque la lumière sort de la lentille).<br>

    <br>


En fonction de la forme de la surface de la lentille, la lumière est réfractée différemment.<br>
On distingue deux types de lentilles :<br>

- Les lentilles dont les bords sont plus fin que le centre, qui sont *convergentes*.<br>
- Les lentilles dont les bords sont plus épais que le centre, qui sont *divergentes*.

<center>
    <div class="grand">
        <figure>
            <img src="../images/cours_optique/refraction_lentille.png" />
            <figcaption>Réfractions des rayons sur les diôptres d'une lentilles</figcaption>
        </figure>
    </div>
</center>

<center>
    <div class="grand">
        <figure>
            <img src="../images/cours_optique/LENTILLES.jpg" />
            <figcaption>Deux types de lentilles</figcaption>
        </figure>
    </div>
</center>

<br>

### 1.3 Vocabulaire : Image, objet,…

#### b. Point objet et objet
!!! def "Définition : Point objet"
    Un point objet est l’intersection des rayons incidents.<br>

    ??? note "Remarque : Rayons incidents ?"
        Les rayons incidents sont les rayons lumineux qui arrivent sur un système optique *(lentille, association de lentilles, miroirs, etc.)*.<br>


On rencontre deux cas :<br>

- Le point objet est à distance finie du système optique, les rayons émergents ou leurs prolongements se coupent.<br>
- Le point image est très éloigné du système optique, il est dit à l'infini : les rayons émergents sont parallèles.<br>


<center>
    <div class="grand">
        <figure>
            <img src="../images/cours_optique/objet.png" />
            <figcaption>Point objet à distance finie et point objet à distance infinie.</figcaption>
        </figure>
    </div>
</center>

<br>

!!! def "Définition : Objet"
    L'objet est modélisé comme un ensemble de points objets.<br>

 <br>


#### c. Point image et image
!!! def "Définition : Point image"
    Un point image est l'intersection des rayons émergents.<br>
    ??? note "Remarque : Rayons émergents ?"
        Les rayons émergents sont les rayons lumineux qui quittent le système optique.<br>


On rencontre également deux cas :

- le point image est à distance finie du système optique, les rayons émergents ou leurs prolongements se coupent.
- le point image est très éloigné du système optique , il est dit à l'infini : les rayons émergents sont parallèles.


<center>
    <div class="grand">
        <figure>
            <img src="../images/cours_optique/image.png" />
            <figcaption>Point image à distance finie et point image à distance infinie.</figcaption>
        </figure>
    </div>
</center>


<br>

!!! def "Définition : Image"
    L'image de l’objet est l'ensemble des points images.<br>

??? note "Complément : Stigmatisme"
    Considérons un point source A envoyant des rayons lumineux sur un système optique. On dira que A est un objet ponctuel réel. Le système est stigmatique, si les rayons émergeant ou leurs prolongements se coupent tous en *un même point*.



### 1.4 Notions de grandeurs algébriques
#### a. Différentes notations en optiques géométrique
En optique, on utilise généralement des grandeurs algébrique, pour indiquer la nature algébrique d'une grandeur. Afin d'indiquer qu'une grandeur est algébrique, on utilise une notations surmonté d'une barre, comme par exemple $\overline{OF'}$.

**Remarque :** Si on utilise la notation $OF'$, sans la barre alors la grandeur n'est pas algébrique ! Il faut être vigilant car les deux notations seront courament utilisées en optique.<br>

<br>

#### b. Mais qu'est ce qu'une grandeur algébrique ? Et ça change quoi ?

!!! def "grandeurs algébriques"
    Une grandeurs algébriques est une grandeur qui posséde un signe. Ce signe est définit par rapport à une référence.


En optique, par convention :

- le sens *positif horizontal* est de *gauche à droite*.
- le sens *positif vertical* est de *bas en haut*.


<br>

Ainsi, si on n'utilise pas les grandeurs algébriques :
<center>
        <div class="moyen">
            <figure>
                <img src="../images/cours_optique/mesures_non_algébriques.png" />
                <figcaption>Les grandeurs ne sont pas algébriques !</figcaption>
            </figure>
        </div>
</center>

Les notations $CD$ et $DC$ sont strictement équivalentes.<br>

<br>

Si on utilise les grandeurs algébriques :
<center>
    <div class="moyen">
        <figure>
            <img src="../images/cours_optique/mesures_algébriques.png" />
            <figcaption>Utilisation de grandeurs algébriques</figcaption>
        </figure>
    </div>
</center>

Les notations $CD$ et $DC$ ne sont pas équivalentes !<br>
Lorsque l'on vas de $C$ vers $D$, on est dans le même sens que le sens positif préalablement défini, la grandeur $\overline{CD}$ est positive !<br>
Lorsque l'on vas de $D$ vers $C$, on est dans le sens opposé au sens positif préalablement défini, la grandeur $\overline{DC}$ est négative !<br>

<br>

#### c. Pourquoi utiliser des notations algébriques ?
L'utilisation des grandeurs algébriques permet d'utiliser exactement les mêmes relations *(conjugaisons, grandissement…)* quelque soit la situations et la nature de la lentille utillisée *(convergente, divergente…)* !<br>

<br>

#### d. Synthèse en vidéo !

<center><iframe width="560" height="315" src="https://www.youtube.com/embed/ENSlg-Lz07Y" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>



## 2. Lentilles convergentes
### 2.1 Modélisation et vocabulaire
#### A. Modélisation d'une lentille convergent

<center>
    <div class="grand">
        <figure>
            <img src="../images/cours_optique/Symbole_lentille_convergente.png" />
            <figcaption>Représentation des lentilles convergentes</figcaption>
        </figure>
    </div>
</center>

<br>


#### B. Vocabulaire

<center>
    <div class="moyen">
        <figure>
            <img src="../images/cours_optique/symboleLentilleConvergente.png" />
            <figcaption>Symbole d'une lentilles convergentes</figcaption>
        </figure>
    </div>
</center>


!!! def "Centre optique"
    Le centres des deux diôptres sphériques est nommé centre optique.

!!! def "Axe optique"
    L’axe optique de la lentille est l’axe passant par les centres des deux diôptres sphériques.


!!! def "Foyer image"
    Par définition, l’image d’un point à l’infini sur l’axe optique est le foyer image F’.<br>
    <center>
        <div class="moyen">
            <figure>
                <img src="../images/cours_optique/foyer_objets.png" />
                <figcaption>Localisation du foyer image d'une lentille convergente</figcaption>
            </figure>
        </div>
    </center>
    <br>
    **Remarque :** Dans le cas d’une lentille convergente, le foyer image est réel *(alors qu’il a le statut d’image virtuelle pour une lentille divergente.)*<br>


!!! def "Foyer objet"
    Par définition, un objet lumineux placé au foyer objet F aura pour image un point à l’infini sur l’axe.<br>
    <center>
        <div class="moyen">
            <figure>
                <img src="../images/cours_optique/distance_focale.png" />
                <figcaption>Localisation du foyer objet d'une lentille convergente</figcaption>
            </figure>
        </div>
    </center>
    <br>
    **Remarque :** Dans le cas d’une lentille convergente, le foyer objet est réel *(alors qu’il a le statut d’objet virtuel pour une lentille divergente).*<br>

    Une lentille convergente peut être utilisée indifféremmemt dans les deux sens. Ainsi, les points focal image et objet sont symétrique par rapport au centre optique de la lentille.
    <center>$OF'= OF$.</center>


!!! def "Distance focale"
    On appelle distance focale, la distance focale image $f'$, soit la longueur $OF'$.
    <center>$f' = \overline{OF'}= - \overline{OF}$.</center>



!!! def "Vergence"
    La vergence d’une lentille est définit par le relation :<br>
    <center>$C = \frac{1}{f'}$</center><br>
    La vergence s’exprime en $m^{−1}$ ou en dioptrie $δ$.

<br>

**Remarque :** $f'$ et $C$ sont positives pour une lentille convergente.

<br>


### 2.2 Effet d'une lentille convergente sur le trajet des rayons de lumières


En arrivant à la lentille : 

- Les rayons passant par le centre optique continuent leur trajet dans la même direction;
- Les rayons parallèles à l'axe optique  seront déviés et passeront par le foyer image de la lentille.
- Si les rayons sont passés par le foyer objets, ils sortent parralèle à l'axe optique.<br>


<center>
    <div class="tresgrand">
        <figure>
            <img src="../images/cours_optique/effet_lentille_convergente.png" />
            <figcaption>Modification du trajet des rayons par une lentille convergente</figcaption>
        </figure>
    </div>
</center>


<br>

### 2.3 Position et taille de l'image d'un objet

La position, la taille et le sens de l'image sont les caractéristiques de l'image. Elles dépendent de la lentile et de la position de l'objets. Pour obtenir ces caractéristiques, il existe deux grandes méthodes : un méthodes graphique et une méthodes calculatoire.<br>

<br>

#### A. Détermination graphique de l’image d’un objet à travers une lentille convergente.

Un objet est représenté par un segment vertical $AB$. On cherche à déterminer son image conjuguée  $A'B'$.<br>

!!! important ""
    L'image d'un objet plan et perpandiculaire à l'axe optique est également plan et perpendiculaire à l'axe optique *(aplanétisme)*.<br>
    L'image d'un point objets appartenant à l'axe optique est sur l'axe optique.<br>
    
Ainsi pour déterminer la position de l'image conjuguée $A'B'$ de l'objet $AB$, il suffit de déterminer l'image $B'$ correspondant au point $B$.<br>

Une infinité de rayon parte du point objet B, dans toutes les directions, parmis l'ensemble de ces rayons, nous nous concentrerons sur trois rayon particuliers dont le trajet à travers la lentille est facilement déterminé.<br>

- Le rayon qui passe par les points $B$ et $O$ n’est pas dévié.<br>
- Le rayon qui passe par le point $B$ et parallèle à l’axe optique émerge en passant par $F'$.<br>
- Le rayon qui passe par les points $B$ et $F$ émerge parallèlement à l’axe optique.<br>


<center>
    <div class="tresgrand">
        <figure>
            <img src="../images/cours_optique/rayon_construction.png" />
            <figcaption>Détermination graphique de l'image A'B' de l'objet AB</figcaption>
        </figure>
    </div>
</center>



??? note "Remarque :"
    Pour trouver la positions du point $B'$, seul deux rayons sont nécéssaire ! Le troisième sert de confirmations.<br>
    Il arrive également que suivant les cas et les individut certains rayon soient plus facile à construirent que d'autres, il est donc important de travailler avec les trois rayons !

<center><iframe width="560" height="315" src="https://www.youtube.com/embed/VKbnvYfm3JM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center>


??? note "Autres vidéos :"
    **Cas d'images réelles :**<br>
    -[Construction de l'image d'un objet par une lentille | Seconde | Physique-Chimie](https://www.youtube.com/watch?v=VzKBIcvKuJc){:target="_blank"}<br>
    -[Méthode : comment construire une image avec une lentille convergente ?](https://www.youtube.com/watch?v=gis7PRQPTCY){:target="_blank"}<br>
    -[Détermination graphique d'une image au travers d'une lentille_profroques](https://www.youtube.com/watch?v=DPjoYcgcspQ){:target="_blank"}<br>
    <br>
    **Cas d'images virtuelles :**<br>
    -[Construction graphique d'une image virtuelle - lentille convergente](https://www.youtube.com/watch?v=FftpaV1-86c){:target="_blank"}<br>



<br>

#### B. Utilisations de la relation de conjugaison

!!! def "Relation de conjugaison"
    Il est possible de déterminer la position d'une image en utilisant la relation de conjugaison :
    <center>$\frac{1}{\overline{OA'}} - \frac{1}{\overline{OA}}=\frac{1}{f'}$</center>


??? note "vidéo :"
    -[Relation de conjugaison et grandissement - Les Bons Profs](https://www.youtube.com/watch?v=n0i4WXW0uIg){:target="_blank"}<br>
    -[Relation de CONJUGAISON : calculer la distance focale](https://www.youtube.com/watch?v=rBXHw1L-6y0){:target="_blank"}<br>
    -[Relation de Conjugaison et Grandissement - Mathrix](https://www.youtube.com/watch?v=0W6DbnOrhpM){:target="_blank"}<br>
    -[La relation de conjugaison des lentilles](https://www.youtube.com/watch?v=MZgNhFsjwLs){:target="_blank"}<br>

<br>

#### C. Le grandissement

!!! def "Relation de grandissement"
    Le grandissement est donnée par :
    <center>$γ=\frac{\overline{A'B'}}{\overline{AB}}=\frac{\overline{OA'}}{\overline{OA}}$</center>



**Sens de l’image :**<br>

Le signe du grandissement $γ$  indique le sens de l’image par rapport à celui de l’objet.<br>

- Si $γ > 0$ , l’image et l’objet sont dans le même sens, on dit que l’image est *droite*.<br>
- Si $γ < 0$  , l’image et l’objet n’ont pas le même sens, on dit que l’image est *renversée*.<br>

<br>

**Taille de l’image :**

La valeur  du grandissement γ  permet de déterminer la taille de l’image par rapport à celle de l’objet.<br>

- Si $|γ|> 1$ , l’image est plus grande que l’objet.<br>
- Si $|γ|< 1$ , l’image est plus petite que l’objet.<br>


??? note "Complément"
    La relation du grandissement découle directement de l’utilisation du théorème de Thales.
    <center>
        <div class="grand">
            <figure>
                <img src="../images/cours_optique/grandissement2.png" />
                <figcaption>Théorème de Thales - Formule du grandissement</figcaption>
            </figure>
        </div>
    </center>
    D'après le théorème de Thales :
    <center>$\frac{\overline{OA'}}{\overline{OA}}=\frac{\overline{A'B'}}{\overline{AB}}=\frac{\overline{OB'}}{\overline{OB}}$</center>


??? note "Vidéos"
    -[Grandissement algébrique : optique 1ère spécialité. Lentille convergente.](https://www.youtube.com/watch?v=Htzb7pN1jSY){:target="_blank"}<br>
    -[Lentille mince convergente - 2/3 : GRANDISSEMENT | Physique-Chimie SECONDE](https://www.youtube.com/watch?v=F6jC-0ZQ1eY){:target="_blank"}<br>
    -[Conjugaison et grandisement](https://www.youtube.com/watch?v=l36nJJ1agDA){:target="_blank"}<br>
    -[Relation de conjugaison et grandissement - Les Bons Profs](https://www.youtube.com/watch?v=n0i4WXW0uIg){:target="_blank"}<br>
    -[Relation de Conjugaison et Grandissement - Mathrix](https://www.youtube.com/watch?v=0W6DbnOrhpM){:target="_blank"}<br>
 


<br>


#### D. Différents cas possibles !


##### Cas 1 : Objet réel à distance fini
<center>
        <div class="tresgrand">
            <figure>
                <img src="../images/cours_optique/objet_réel.png" />
            </figure>
        </div>
    </center>

 Le grandissement $γ$ est *inférieur à 1* et *négatif*.<br>
 
 <br>


##### Cas 2 : Objet réel à distance fini
<center>
    <div class="tresgrand">
        <figure>
            <img src="../images/cours_optique/.png" />
            <figcaption>??? à faire</figcaption>
        </figure>
    </div>
</center>

Le grandissement $γ$ est *supérieur à 1* et *négatif*.<br>

<br>

##### Cas 3 : Objet réel à l'infini
<center>
    <div class="tresgrand">
        <figure>
             <img src="../images/cours_optique/.png" />
            <figcaption>??? à faire</figcaption>
        </figure>
    </div>
</center>


##### Cas 4 : Objet réel, entre les points F et O.
<center>
    <div class="tresgrand">
        <figure>
            <img src="../images/cours_optique/objet_réel2.png" />
            <figcaption>???</figcaption>
        </figure>
    </div>
</center>

Les rayons emmergants ne se coupent pas !<br>
Par contre, il est possible de prolonger les rayons en amont de la lentille.<br>
Si les rayons émergant pénètre dans l'œil, le cerveau aurra l'impression que les rayons proviennent de $A'B'$. Il s'agit d'une *image virtuelle.*

??? note "Image virtuelle"
    Une image virtuelle ne peut pas être recueilli sur un écran.<br>

Le grandissement $γ$ est *supérieur à 1* et *positif*.<br>

Il s'agit ici du cas d'une loupe !<br>


??? note "Vidéo"
    -[Construction graphique d'une image virtuelle - lentille convergente - | 1ère spé](https://www.youtube.com/watch?v=FftpaV1-86c){:target="_blank"}<br>



<br>

##### Cas 5 : Objet réel, sur le point F

<center>
    <div class="tresgrand">
        <figure>
            <img src="../images/cours_optique/.png" />
            <figcaption>??? à faire</figcaption>
        </figure>
    </div>
</center>

<br>

#### E. S'exercer

<center><iframe scrolling="no" title="Lentille convergente" src="https://www.geogebra.org/material/iframe/id/y5ctz3ba/width/1366/height/551/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/true/rc/false/ld/false/sdz/true/ctl/false" width="1366px" height="400px" style="border:0px;"> </iframe></center>


??? note "Vidéos : corrections d'exercices"   
    -[Exercice LENTILLE : construction image et grandissement | Seconde | Physique-Chimie?](https://www.youtube.com/watch?v=kjxgnWVZa6I){:target="_blank"}<br>
    
    -[Méthode : utiliser correctement les relations de conjugaison et de grandissement en optique](https://www.youtube.com/watch?v=l36nJJ1agDA){:target="_blank"}<br>
    -[Exercice Relation de conjugaison et grandissement : Exemple 1 - Physique-Chimie - 1ère - Les Bons Profs](https://www.youtube.com/watch?v=T52yJG2KNzg){:target="_blank"}<br>
    -[Exercice Relation de conjugaison et grandissement : Exemples 2&3 - Physique-Chimie - 1ère - Les Bons Profs](https://www.youtube.com/watch?v=IhdBTvlmkuk){:target="_blank"}<br>




??? note "Le cours en vidéo"
    -[Cours de Physique 1S Lentilles minces](https://www.youtube.com/watch?v=tBywMwD87yo){:target="_blank"}<br>
    -[Cours de Physique 1S Lentilles minces](https://www.youtube.com/watch?v=s5dNfonHCdA){:target="_blank"}<br>


??? note "Ressources"
    -----
    - <a href="https://dlatreyte.github.io/premieres-pc/chap-5/4-formation-images/">*https://dlatreyte.github.io/*</a><br>
    - <a href="https://femto-physique.fr/optique/pdf/book-optgeo.pdf">*femto-physique.fr*</a>
    ----


    ## Cours préalable
    -[loi optique géo](https://www.physagreg.fr/optique-11-lois-optique-geometrique.php){:target="_blank"}<br>

    ## cours complet
    -[cours d'optique](https://dlatreyte.github.io/premieres-pc/chap-5/4-formation-images/){:target="_blank"}<br>
    -[cours d'optique_femto](https://femto-physique.fr/optique/pdf/book-optgeo.pdf){:target="_blank"}<br>
    -[fiche révision lycée militaire aix](https://www.lycee-militaire-aix.fr/site/images/conseils%20CPGE/mpsi/physique/revision_optique_cours.pdf){:target="_blank"}<br>
    -[cours l1 et l2](https://www.univ-usto.dz/images/coursenligne/Cours_Optique-Beldjilali.pdf){:target="_blank"}<br>
    -[lien optique lentille convergente](https://coyote-physique.e-monsite.com/pages/specialite-1ere/lentille.html){:target="_blank"}<br>







    ## vidéo
    ### cours complet<br>
    -[Cours prépa scientifique. L'Optique géométrique.](https://www.youtube.com/watch?v=tPDmwX7g72U){:target="_blank"}<br>
    -[Introduction à l'optique géométrique en L1 - cours 1_Richard Taillet](https://www.youtube.com/watch?v=dckvoph10p4){:target="_blank"}<br>
    -[Vidéo - Chapitre 01 Rappels elementaire optique](https://www.youtube.com/watch?v=xDF4U_74aTc){:target="_blank"}<br>

    ### morceau choisi
    -[Qu'est-ce qu'une lentille convergente ? Divergente ? | Physique-Chimie](https://www.youtube.com/watch?v=ei6JE-Pte1s){:target="_blank"}<br>

    -[Lentille mince convergente | Seconde | Physique-Chimie](https://www.youtube.com/watch?v=7rjeXimQGfk){:target="_blank"}<br>

    -[Méthode : connaître les lentilles minces convergentes en optique](https://www.youtube.com/watch?v=c9XsoKxKqek){:target="_blank"}<br>
    -[Les lentilles : introduction](https://www.youtube.com/watch?v=B1_UM3LQ0s4){:target="_blank"}<br>





    -[Distance algébrique : optique (1ère spécialité)](https://www.youtube.com/watch?v=ENSlg-Lz07Y){:target="_blank"}<br>






    ## Tracé
    -[Construction de l'image d'un objet par une lentille | Seconde | Physique-Chimie](https://www.youtube.com/watch?v=VzKBIcvKuJc){:target="_blank"}<br>
    -[Méthode : comment construire une image avec une lentille convergente ?](https://www.youtube.com/watch?v=gis7PRQPTCY){:target="_blank"}<br>
    -[Détermination graphique d'une image au travers d'une lentille_profroques](https://www.youtube.com/watch?v=DPjoYcgcspQ){:target="_blank"}<br>
    -[Construction graphique d'une image virtuelle - lentille convergente - | 1ère spé](https://www.youtube.com/watch?v=FftpaV1-86c){:target="_blank"}<br>



    -[Exercice LENTILLE : construction image et grandissement | Seconde | Physique-Chimie?](https://www.youtube.com/watch?v=kjxgnWVZa6I){:target="_blank"}<br>


    ## conjugaison et grandissement
    -[Grandissement algébrique : optique 1ère spécialité. Lentille convergente.](https://www.youtube.com/watch?v=Htzb7pN1jSY){:target="_blank"}<br>

    -[conjug et grandisement](https://www.youtube.com/watch?v=l36nJJ1agDA){:target="_blank"}<br>
    -[Relation de conjugaison et grandissement Les Bons Profs](https://www.youtube.com/watch?v=n0i4WXW0uIg){:target="_blank"}<br>
    -[Exercice Relation de conjugaison et grandissement : Exemple 1 - Physique-Chimie - 1ère - Les Bons Profs](https://www.youtube.com/watch?v=T52yJG2KNzg){:target="_blank"}<br>
    -[Exercice Relation de conjugaison et grandissement : Exemples 2&3 - Physique-Chimie - 1ère - Les Bons Profs](https://www.youtube.com/watch?v=IhdBTvlmkuk){:target="_blank"}<br>

    -[Relation de Conjugaison et Grandissement - Mathrix](https://www.youtube.com/watch?v=0W6DbnOrhpM){:target="_blank"}<br>

    -[Lentille mince convergente - 2/3 : GRANDISSEMENT | Physique-Chimie SECONDE](https://www.youtube.com/watch?v=F6jC-0ZQ1eY){:target="_blank"}<br>

    -[La relation de conjugaison des lentilles](https://www.youtube.com/watch?v=MZgNhFsjwLs){:target="_blank"}<br>


    <br>


    relation de conjugaison démo<br>
    -[Optique géométrique : démonstration de la formule de conjugaison de Newton](https://www.youtube.com/watch?v=oB_eeX36UnQ){:target="_blank"}<br>



    <br>




    -[exercices Lentille convergente : GRANDISSEMENT | Seconde | Physique Chimie](https://www.youtube.com/watch?v=qZojqZQpf8I){:target="_blank"}<br>

    -[LENTILLE : construction image et grandissement | Seconde | Physique-Chimie](https://www.youtube.com/watch?v=kjxgnWVZa6I){:target="_blank"}<br>





    ## TP
    -[Spécialité 1ère TP OPTIQUE «  recherche d’une image sur banc d’optique ](https://www.youtube.com/watch?v=tpK5bIPbAwg){:target="_blank"}<br>


    ## pixel résolution
    -[pixel,résolution,…](http://physique.ostralo.net/images/){:target="_blank"}<br>
