---
title : "Exercices 3 : Optique géométrique"
correctionvisible : true
pdf : "exercices"
---


<div class="noprint" style="text-align:right;width:100%;background-color:white">
    <a href="../pdf/{{page.meta.pdf}}.pdf" 
    style="display:inline-block" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="#a3a3a3" stroke-width="1.5" stroke-linecap="butt" stroke-linejoin="arcs"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg>
    </a>
</div>



# Exercices sur l'optique géométriques


### Exercice 1 : 
<center>
<div class="grand">
    <figure>
        <img src="../images/exercices/exo1.png" />
        <figcaption>Manuel Numérique Hilbrt p145</figcaption>
    </figure>
</div>
</center>

??? faq correction "Correction"
    1.
    <center>
        <div class="grand">
            <figure>
                <img src="../images/exercices/exo1_correction.png" />
            </figure>
        </div>
    </center>

    <br>

    2.Les rayons qui passent par le centre optique d'une lentille ne sont pas déviés.
    <center>
        <div class="grand">
            <figure>
                <img src="../images/exercices/exo1_2_correction.png" />
            </figure>
        </div>
    </center>

    <br>

    3.Lorsque les rayons proviennent d'une source à l'infinit et qu'ils sont parralèles à l'axe optique, ils convergent en un point, que l'on nomme le foyer image de la lentille, noté F'.
    <center>
        <div class="moyen">
            <figure>
                <img src="../images/exercices/foyer_image.png" />
            </figure>
        </div>
    </center>

    <br>

    4.*"Si les rayons sont passés par un point particulier, appelé foyer objet F, ils sont transformés en rayons parallèles à l'axe optique."*
    <center>
        <div class="moyen">
            <figure>
                <img src="../images/exercices/foyer_objet.png" />
            </figure>
        </div>
    </center>

    <br>

    5."La position des points foyers caractérise la lentille. On appelle distance focale f' la longueur OF :<br>
    <center>$f'=OF=OF'$</center>

    <br>

    6.On définit la vergence à l'aide le la relation :
    <center>$C=\frac{1}{f'}$</center>
    Si la distance focale f' est exprimée en mètre (m), la vergence est donnée en dioptrie (δ).<br>
    *La vergence comme la distance focale caractérise la lentille !*



### Exercice 2 : 
<center>
<div class="grand">
    <figure>
        <img src="../images/exercices/exo2.png" />
        <figcaption>Manuel Numérique Hilbrt p145</figcaption>
    </figure>
</div>
</center>

??? faq correction "Correction"
    1.
    <center>
        <div class="grand">
            <figure>
                <img src="../images/exercices/exo2_correction.png" />
            </figure>
        </div>
    </center>

   
### Exercice 3 : 
<center>
<div class="grand">
    <figure>
        <img src="../images/exercices/exo3.png" />
        <img src="../images/exercices/exo3_2.png" />
        <figcaption>Manuel Numérique Hilbrt p145</figcaption>
    </figure>
</div>
</center>

??? faq correction "Correction"
    <center>
        <div class="grand">
            <figure>
                <img src="../images/exercices/exo3_correction.png" />
            </figure>
        </div>
    </center>

    voir cours : Détermination d'une image par méthode graphique (lien)


### Exercice 4 : 
<center>
<div class="grand">
    <figure>
        <img src="../images/exercices/exo4.png" />
        <figcaption>Manuel Numérique Hilbrt p146</figcaption>
    </figure>
</div>
</center>

??? faq correction "Correction"
    1.**Méthode graphique :**<br>
    <center>
        <div class="grand">
            <figure>
                <img src="../images/exercices/exo4_correction2.png" />
            </figure>
        </div>
    </center>
    <br>
    Par définition :<br>
    <center>$f'=OF=OF'$</center><br>
    Par lecture sur la représentation à l'échelle ci-dessus, on peut affirmer que la distance focale de cette lentille est donc de $f'=13,25 cm$.

    <br><br>

    **Méthode calculatoire *(version 1)*:**<br>
    On cherche la distance focale, noté $f'$:<br>
    D'après le cours :<br>
    <center>$\frac{1}{\overline{OA'}} - \frac{1}{\overline{OA}}=\frac{1}{f'}$</center>
    ainsi
    <center>$\frac{1}{f'}=\frac{1}{\overline{OA'}} - \frac{1}{\overline{OA}}$</center>
    **Application numérique :**<br>
    <center>$\frac{1}{f'}=\frac{1}{20cm} - \frac{1}{-40cm}=0,075 cm^{-1}$</center>
    On en déduit donc la valeur de f':
    <center>$f'=\frac{1}{\frac{1}{f'}}=\frac{1}{0,075cm^{-1}}=13,33 cm$</center>
    La distance focale de cette lentille est donc de $f'= 13,33 cm$.

    <br><br>



    **Méthode calculatoire *(version 2)*:**<br>
    On cherche la distance focale, noté $f'$:<br>
    D'après le cours<br>
    <center>$\frac{1}{\overline{OA'}} - \frac{1}{\overline{OA}}=\frac{1}{f'}$</center>


    On en déduit que :<br>

    $\frac{1}{f'} = \frac{1}{\overline{OA'}} × \frac{\overline{OA}}{\overline{OA}} - \frac{1}{\overline{OA}}×\frac{\overline{OA'}}{\overline{OA'}}$

    $\frac{1}{f'} = \frac{\overline{OA}}{\overline{OA'}×\overline{OA}} - \frac{\overline{OA'}}{\overline{OA}×\overline{OA'}}$

    $\frac{1}{f'} = \frac{\overline{OA} -\overline{OA'}}{\overline{OA'}×\overline{OA}}$

    <center>
        $f'= \frac{\overline{OA'}×\overline{OA}}{\overline{OA} -\overline{OA'}}$
    </center>

    **Application numérique :**<br>
    $f'= \frac{20cm×-40cm}{-40cm -20cm}= 13,33cm$

    La distance focale de cette lentille est donc de $f'= 13,33cm$.

    <br><br>

    2.On cherche la vergence de la lentille,notée $C$.<br>
    Par définition, la vergence est donnée par :
    <center>$C=\frac{1}{f'}$</center>
    **Application numérique :**<br>
    *Pour que le résultat soit exprimée en dioptrie δ, la distance focale f' doit être en m.*<br>
    $C=\frac{1}{13,33.10^{-2}}=7,5δ$<br>
    La vergence de cette lentille est de $V=7,5δ$.

    <br>

    3.On cherche le grandissement, noté γ <br>
    Par définition, le grandissement est donnée par :
    <center>$γ=\frac{\overline{A'B'}}{\overline{AB}}=\frac{\overline{OA'}}{\overline{OA}}$</center>
    **Application numérique :**<br>
    $γ=\frac{20cm}{-40cm}=-0,5$<br>
    Dans cette configuration, le grandissement est de $γ=-0,5$<br>.

    <br>


### Exercice 5 : 
<center>
<div class="grand">
    <figure>
        <img src="../images/exercices/exo5.png" />
        <figcaption>Manuel Numérique Hilbrt p146</figcaption>
    </figure>
</div>
</center>

??? faq correction "Correction"
    1.*On choisi comme échelle 1 carreau = 12,5 cm*
    <center>
        <div class="grand">
            <figure>
                <img src="../images/exercices/exo5_correction.png" />
                <figcaption>Représentation à l'échelle de la situation</figcaption>
            </figure>
        </div>
    </center>

    <br>

    2.On cherche la distance séparant la lentille de l'image.<br>
    Il s'agit de la distance OA'.
    Par lecture sur la représentation, on obtient OA'=1m.

    <br>

    3.On cherche le grandissement dans cette situation, noté γ.<br>
    Par définition le grandissement est donnés par la relations:<br>
    <center>$γ=\frac{\overline{A'B'}}{\overline{AB}}=\frac{\overline{OA'}}{\overline{OA}}$</center>
    Soit ici :
    <center>$γ=\frac{-50cm}{50cm}=-1$</center>
    le grandissement dan cette situation est de $γ=-1$

    <br>

    4.Calcul de la distance OA'.<br>
    D'après la relation de conjugaison :<br>
    <center>$\frac{1}{\overline{OA'}} - \frac{1}{\overline{OA}}=\frac{1}{f'}$</center>
    Donc : <br>
    $\frac{1}{\overline{OA'}} =\frac{1}{f'}+\frac{1}{\overline{OA}}$</center>

    $\frac{1}{\overline{OA'}} = \frac{1}{f'} × \frac{\overline{OA}}{\overline{OA}} + \frac{1}{\overline{OA}}×\frac{f'}{f'}$

    $\frac{1}{\overline{OA'}} = \frac{\overline{OA}}{f'×\overline{OA}} + \frac{f'}{\overline{OA}×f'}$

    $\frac{1}{\overline{OA'}} = \frac{\overline{OA}+f'}{f'×\overline{OA}}$

    <center>
        $\overline{OA'}= \frac{f'×\overline{OA}}{\overline{OA}+f'}$
    </center>

    **Application numérique :**<br>
    $\overline{OA'}= \frac{50.10^{-2}m×-1m}{-1m+50.10^{-2}m}=1m$


    L'utilisation de la formule de conjugaison, nous donne le même résultat que précédement, soit  $\overline{OA'}=1m$.

    <br>
    
    Calcul du grandissement, noté γ.<br>
    <center>$γ=\frac{\overline{A'B'}}{\overline{AB}}=\frac{\overline{OA'}}{\overline{OA}}$</center>
    **Application numérique :**<br>
    $γ=\frac{1m}{-1m}=-1$<br>
    On retrouve bien un grandissement de $γ=-1$






### Exercice 6 : 
<center>
<div class="grand">
    <figure>
        <img src="../images/exercices/exo6_1.png" />
        <img src="../images/exercices/exo6_2.png" />
        <figcaption>Manuel Numérique Hilbrt p146</figcaption>
    </figure>
</div>
</center>

??? faq correction "Correction"
    1.*On choisi comme échelle 1 grand carreau = 50 mm*
    <center>
        <div class="grand">
            <figure>
                <img src="../images/exercices/exo6_correction.png" />
            </figure>
        </div>
    </center>

    <br>

    2.La représentation ci-dessu, nous permet de dire que la position de l'image à travers la lentille est OA'=300mm.

    <br>
    
    3.On cherche l'agrandissement, noté γ.
    <center>$γ=\frac{\overline{A'B'}}{\overline{AB}}=\frac{\overline{OA'}}{\overline{OA}}$</center>
    **Application numérique :**<br>
    $γ=\frac{300m}{-100mm}=-3$<br>
    L'agrandissement de ce dispositif dans ces conditions est de $γ=-3$, la valeur négative indique que l'image est dans le sens inverse de l'objet, et la valeur 3 nous indique que l'image est trois fois plus grande que l'objet.<br>

    <br>

    4.Si l'on place un écran à 25cm=250mm du projecteur, l'écran est trop près, les rayons ne convergent pas en un point mais forme une tache ce qui explique le flou.<br>
    <center>
        <div class="grand">
            <figure>
                <img src="../images/exercices/exo6_correction1.png" />
                <figcaption>Écran trop près, les rayons ne convergent pas en un points, l'image est flou.</figcaption>
            </figure>
        </div>
    </center>


    Pour que l'image soit nette, l'écran doit se situé dans le plans ou les rayon issus de l'objets convergent, c'est à dire à 300mm de la lentille.<br>
    <center>
        <div class="grand">
            <figure>
                <img src="../images/exercices/exo6_correction2.png" />
                <figcaption>Écran bien placé, les rayons convergent en un points, l'image est nette.</figcaption>
            </figure>
        </div>
    </center>



### Exercice 7 : 
<center>
<div class="grand">
    <figure>
        <img src="../images/exercices/exo7_texte.png" />
        <img src="../images/exercices/exo7_dessin.png" />
        <img src="../images/exercices/exo7_photo.png" />
        <figcaption>Manuel Numérique Hilbrt p146</figcaption>
    </figure>
</div>
</center>

??? faq correction "Correction"
    1. Il est dit que les rayons proviennent de l'infini, or les rayons incident doivent donc être parallèles, les schémas qui correspondent à cette situations sont les schémas 1 et 2.
    <center>
        <div class="grand">
            <figure>
                <img src="../images/exercices/exo7_correction.png" />
                <figcaption>Des rayons provenant de l'infinit sont parallèles</figcaption>
            </figure>
        </div>
    </center>






<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  


