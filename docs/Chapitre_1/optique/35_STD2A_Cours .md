---
title : "Cours 4"
pdf : 
---


<div class="noprint" style="text-align:right;width:100%;background-color:white">
    <a href="../pdf/{{page.meta.pdf}}.pdf" 
    style="display:inline-block" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="#a3a3a3" stroke-width="1.5" stroke-linecap="butt" stroke-linejoin="arcs"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg>
    </a>
</div>



# Chapitre 5 : Objectif photographique





??? info "Dans le programme"
    **Thème 1** : Connaître et transformer les matériaux

    | **Notions et contenus**                                                                                         | **Capacités exigibles**                                                                                 |
    | --------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------- |
    | Familles de matériaux.                                                                                          | - Distinguer les grandes classes de matériaux utilisés dans lesdomaines du design et des métiers d’art. |
    | Propriétés physiques des matériaux : masse volumique, élasticité, conductivité thermique,absorption acoustique. | - Comparer expérimentalement des *caractéristiques physiques de différents matériaux*.                  |
 

??? info "Compétences travaillées"
    




<br>

### 2.1 Qu'est ce qu'un objectif photo

Un objectif peut être modélisé par une lentille convergente unique.<br>

??? remarque "Pourquoi un objectif photo est-il constitué d'autant de lentille"
    Un objectif est composé de plusieurs lentilles, généralement assemblés en groupe pour :

    - Corriger les abérations optiques.
    - Réduire l'encombrement (téléobjectif).



??? note "Les abérations des système optiques"
    **Qu'est ce que l'abération chromatique ?**
    <center>
    <div class="tresgrand">
        <figure>
            <img src="../images/cours_appareil/Abération_Chromatique.png" />
        </figure>
    </div>
    </center>

    <br>


    **Qu'est ce que l'abération géométrique ?**
    <center>
    <div class="tresgrand">
        <figure>
            <img src="../images/cours_appareil/Abération_géométrique.png" />
        </figure>
    </div>
    </center>





### 2.2 Angle de champ
#### A. Notion d'angle de champ
<center>
    <div class="grand">
        <figure>
            <img src="../images/cours_objectif/focale_et_angle_de_champ.png" />
            <img src="../images/cours_objectif/649467.webp" />
*           <img src="../images/cours_objectif/niveau-1-exemples-focale-2.jpg" />
            <figcaption> à faire</figcaption>
        </figure>
    </div>
</center>

-[Nikon lens simulation](https://imaging.nikon.com/lineup/lens/simulator/)




#### B. Influance de la focale
[cineastuces_la focale d'un objectif](https://www.youtube.com/watch?v=BhX0RC6qYD0)



<center>
    <div class="grand">
        <figure>
            <img src="../images/cours_objectif/2-schema-champ-de-vision.png" />
            <img src="../images/cours_objectif/angle de champ.jpg" />
            <img src="../images/cours_objectif/aov.png" />
            <img src="../images/cours_objectif/longeur-focale-mm.jpg" />
            <img src="../images/cours_objectif/focale-et-angle-cadrage-par-focale-en-24x36-1.jpg" />
            <img src="../images/cours_objectif/15_objectifs.jpg" />
            <img src="../images/cours_objectif/Comment-choisir-un-objectif-photo-en-5-étapes2.png" />
            <img src="../images/cours_objectif/Focale_champs-1024x444.jpg" />
            <img src="../images/cours_objectif/Focales1.jpg" />
            <img src="../images/cours_objectif/focal-length-mm3.webp" />
            <img src="../images/cours_objectif/formation_prof_de_champ_bm_7.gif" />
            <img src="../images/cours_objectif/objectif02.jpg" />
            <img src="../images/cours_objectif/" />
            <figcaption> à faire</figcaption>
        </figure>
    </div>
</center>


#### C. Influance de taille de la surface photosensible

<center>
    <div class="grand">
        <figure>
            <img src="../images/cours_objectif/artfichier_805457_5621925_201604144023694.jpg" />
            <img src="../images/cours_objectif/capteurs.jpg" />
            <img src="../images/cours_objectif/Capteurs.jpg" />
            <img src="../images/cours_objectif/ind6546ex.jpg" />
            <img src="../images/cours_objectif/les-tailles-de-capteurs-et-les-focales-equivalentes-2fa74ed6__w380.jpg" />
            <img src="../images/cours_objectif/schema-angle-de-champ.png" />
            <figcaption> à faire</figcaption>
        </figure>
    </div>
</center>
