---
title : "Activité 3"
correctionvisible : true
pdf : ""
---


<div class="noprint" style="text-align:right;width:100%;background-color:white">
    <a href="../pdf/{{page.meta.pdf}}.pdf" 
    style="display:inline-block" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="#a3a3a3" stroke-width="1.5" stroke-linecap="butt" stroke-linejoin="arcs"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg>
    </a>
    <button id="bt_test">
        <img src="../../assets_extra/images/cravateseule.svg" width="30"/>
    </button>
</div>

<div id="masque"  style="display:none;text-align:center;font-size: 25px;font-weight: bold;">
    <a href="../pages_cache/enavance/">Page cachée</a>
</div>

<script>
    let bt1 = document.getElementById("bt_test");
    let d2 = document.getElementById("masque");


    function identification(){
        if (d2.style.display!= "block"){
            var reponse = prompt("Es-tu réellement un porteur de cravate ?","<saisir un identifiant>");
            if(reponse==45){
                d2.style.display = "block";
            }
        }
    };

    bt1.onclick = identification;
</script>






# Activité 3 : Prendre un photographie avec un sténopé


??? info "Compétences travaillées"
    Rechercher et extraire des informations

<br>

## 1. Les paramètres de la prise de vue
### 1. L'exposition
<a href="https://www.youtube.com/watch?v=V20odwEd5jE" >https://www.youtube.com/watch?v=V20odwEd5jE</a>

### 2. L'ouverture
<a href="https://www.youtube.com/watch?v=v_W8O1U4qRU" >https://www.youtube.com/watch?v=v_W8O1U4qRU</a>

### 3.La vitesse d'obturation
<a href="https://www.youtube.com/watch?v=pD45CIQx3Ks" >https://www.youtube.com/watch?v=pD45CIQx3Ks</a>

### 4.La sensibilité ISO
<a href="https://www.youtube.com/watch?v=EwHdCWmojDU&list=PLVGCy9QVeRNXyWXamf9h5fbFslJnaD2C9" >https://www.youtube.com/watch?v=EwHdCWmojDU&list=PLVGCy9QVeRNXyWXamf9h5fbFslJnaD2C9</a>


!!! faq "Questions"
    1. Qu’est-ce qu’une photo sous-exposée ?
    2. Comment reconnaît-on qu’une photo est sous-exposée ?
    3. Qu’est-ce que l’ouverture ?
    4. Qu’est-ce que le temps de pause ou la vitesse d’obturation ?
    5. Qu’est-ce que la sensibilité ISO ?
    6. On parle parfois de triangle d'exposition, expliquez cette appellation.



## 2. Avant de prendre une photo avec un sténopé

!!! faq "Questions"
    1. Recherche et décrire les étapes correspondant à la réalisation d'une photographie avec un sténopé.

</br>

!!! faq "Questions"
    1. *(Recherche internet)* Quelle est la sensibilité ISO d’un papier photo RC.
    2. *(Recherche internet)* Sur un sténopé de quoi dépend l’ouverture ? Comment la calculer ?
    3.  Lors de la prise de vue avec un sténopé, quel·s paramètre·s peut-on régler pour que la photo soit correctement exposée ?
    4.  Pourquoi est-il important de déterminer l’ouverture de son sténopé ?

</br>

## 3. Prendre une photo avec un sténopé

**Document :** Calcul de l'ouverture d'un sténopé


!!! faq "Questions"
    1. Calculer l'ouverture de votre sténopé.
    2. Réaliser une mesure avec un pose mètre de la scéne que vous souhaitez photographier.
    3. En déduire le temps de pose nécéssaire à votre prise de vue.
    4. Réaliser la photographie.

</br>


## 4. Les caractéristiques d'une photo au sténopé

Étudier l'ensemble des photographie réalisée par la classe.


!!! faq "Questions"
    1.  *(Recherche internet)* Qu'est-ce que la profondeur de champ ?
    2.  Que dire de la profondeur de champ des images réalisée avec un sténopé ? *(Chercher et observer des images prises au sténopé sur internet, si nécéssaire)*

<br>
