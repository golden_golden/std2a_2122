---
title : "Activité 1"
correctionvisible : true
pdf : ""
---


<div class="noprint" style="text-align:right;width:100%;background-color:white">
    <a href="../pdf/{{page.meta.pdf}}.pdf" 
    style="display:inline-block" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="#a3a3a3" stroke-width="1.5" stroke-linecap="butt" stroke-linejoin="arcs"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg>
    </a>
    <button id="bt_test">
        <img src="../../assets_extra/images/cravateseule.svg" width="30"/>
    </button>
</div>

<div id="masque"  style="display:none;text-align:center;font-size: 25px;font-weight: bold;">
    <a href="../pages_cache/enavance/">Page cachée</a>
</div>

<script>
    let bt1 = document.getElementById("bt_test");
    let d2 = document.getElementById("masque");


    function identification(){
        if (d2.style.display!= "block"){
            var reponse = prompt("Es-tu réellement un porteur de cravate ?","<saisir un identifiant>");
            if(reponse==45){
                d2.style.display = "block";
            }
        }
    };

    bt1.onclick = identification;
</script>






# Activé 1 : Histoire de la photographie couleur


### **Document 1**: Naissance de la photographie
<img src="../images/View_from_the_Window_at_Le_Gras_Joseph_Nicéphore_Niépce.png" alt="Première photographie de l'histoire" width="250" align="right" />

Au début du $19^{ème}$ siècle Joseph Nicéphore Niépce (1765-1833) se consacrait à l’amélioration de la technique de la lithographie, très à la mode à cette époque. Pour dessiner les épreuves qu’il voulait reproduire, Nièpce pensa à utiliser la lumière.<br> Dès 1812, il parvint à obtenir en lithographie des négatifs et des positifs, mais ces images n’étaient pas stables. En 1826, après de très nombreuses recherches et expérimentations, il réussit à obtenir ce qui est majoritairement reconnu aujourd’hui comme la première photographie, en raison de sa stabilité et parce qu’il s’agit de la première image connue prise d'après nature avec une chambre noire utilisée comme appareil photographique. Elle représente : un bâtiment, un arbre et une grange, il s’agit de la vue de sa propriété de Saint-Loup-de-Varennes (Saône-et-Loire) prise depuis une fenêtre. Sa réalisation à nécessitée plusieurs jours d’exposition.

<img src="../images/Daguerreotipe.jpg" alt="Première photographie de l’histoire" width="150" align="left"/>
Dans le milieu des années 1830, l'inventeur, artiste et peintre Louis Jacques Mandé Daguerre (1787-1851) s’intéresse à la photographie. En 1839, il présente au public son invention, le Daguerréotype, qui reçoit un accueil des plus enthousiastes.<br>Grâce au daguerréotype, on obtient des images après « seulement » une demi-heure de pose (lorsque le ciel est parfaitement dégagé). Cette lenteur est quelque peu problématique : les rues de Paris, même à une heure d’affluence apparaissent totalement vides. Mais qu’importe, la photographie était inventée.

<p align="right">
    <em>D'après<a href="https://fr.wikipedia.org/wiki/Histoire_de_la_photographie"> Wikipédia.</a></em>
</p>

<br>


### **Document 2**: La colorisation photographique
<img src="../images/Hand-coloured_daguerreotype.jpg" alt="Daguerreotype coloré à la main" width="130" align="right" />
Quand en 1839, Johann Baptist Isenring(1796-1860) entend parler de l’invention du daguerréotype, il commande un appareil photo à Paris. En août 1840, il expose, dans son studio, des vues de la ville, des reproductions de peinture et 38 portraits photographiques, une exposition pour laquelle il imprime un catalogue de quatre pages. L’exposition a ensuite été montrée à Zurich, Munich, Augsbourg, Vienne et Stuttgart. En 1841, il ouvre à Munich un studio d’héliographie.

Isenring produit alors le premier daguerréotype coloré en utilisant un mélange de gomme arabique et de pigments. La poudre colorée était fixée sur la surface délicate du daguerréotype par chauffage. Il obtint ainsi un des premiers exemples de photographie coloriée à la main. Il dépose le brevet, y compris pour les États-Unis, puis, grâce à la vente de ses droits pour une durée de huit mois, il fait fabriquer un studio photo mobile, qui consiste en une chambre noire montée sur roues, la première au monde. 
 
<p align="right">
    <em>D'après<a href="https://fr.wikipedia.org/wiki/Johann_Baptist_Isenring"> Wikipédia.</a></em>
</p><br>



### **Document 3**: Les débuts de la photographique couleur

<center><iframe width="710" height="400" src="https://www.youtube.com/embed/qEdm8kHuqWQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<br>
<a href="https://www.youtube.com/watch?v=qEdm8kHuqWQ">Les début de la photographie couleur - L'autochrome</a>
</center><br>


### **Document 4**: Synthèse additive et synthèse soustractive

<div align="center"><img src="../images/Synthese_trichrome.svg" alt="Les Synthèses trichrome" width="500"/></div>

<br>

### **Questions** :

!!! faq "Questions"
    **La synthèse additive**<br>
    1. Pour quelle raison parle-t-on de synthèse additive ?<br>
    2. Complétez la partie correspondante du document 4.
   
    **La synthèse soustractive**<br>
    3. Pour quelle raison parle-t-on de synthèse soustractive ? <br>
    4. Complétez la partie correspondante du document 4.
    

    **La première photographie en couleur**<br>
    5. Quelle synthèse ont utilisé Maxwell et Sutton pour faire la photographie du "rubant de tartan".<br>
    6. Comment ont-ils procédés lors de la prise de vue.<br>
    7. Comment fait ont pour obtenir l’image en couleur.

    **Louis Ducos Du Hauron et Charle Cros**<br>
    8. Sur quelle synthèse repose leur procédé ?<br>
    9. De quelles couleurs sont les filtres utilisés lors de la prise de vue ?<br>
    10. Comment fait-on pour obtenir l'image en couleur.

    **L'autochrome**<br>
    11. Qui invente l’autochrome ?<br>
    12. Quels sont les défauts de l’autochrome ?

    **Les film couleurs**<br>
    13. Quelle synthèse est utilisé dans le film Kodachrome ?


<br>

### **Ressources :**

??? note "Ressources"
    _Textes_ :

    - [wikipédia_photo couleur](https://fr.wikipedia.org/w/index.php?title=Photographie_couleur&oldid=184836132)
    - [wikipédia_colorisation photographique](https://fr.wikipedia.org/wiki/Colorisation_photographique)
    - [colorisation avec technique moderne](https://www.laboiteverte.fr/vieilles-photos-celebres-colorisees/)
    - [colorisation par IA](https://www.numerama.com/tech/478500-ia-ce-site-colorise-automatiquement-nimporte-quelle-photo-en-noir-et-blanc.html)
    - [La Photo Couleur : Histoire D'une Invention](https://www.gralon.net/articles/photo-et-video/photo-et-video/article-la-photo-couleur---histoire-d-une-invention-3198.htm)
    - [Histoire de la photographie – partie 4 :découverte des films couleurs en 1936](https://www.ifolor.ch/fr/inspirations/histoire-photographie-partie4)
  
    <br>

    _Vidéos_ :

    - [Les début de la photographie couleur| L'AUTOCHROME](https://www.youtube.com/watch?v=qEdm8kHuqWQ)
