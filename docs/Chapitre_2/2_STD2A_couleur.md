---
title : "Cours"
---


<div class="noprint" style="text-align:right;width:100%;background-color:white">
    <a href="../pdf/chapitre2.pdf" style="display:inline-block" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="#a3a3a3" stroke-width="1.5" stroke-linecap="butt" stroke-linejoin="arcs"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg>
    </a>
    <button id="bt_test">
        <img src="../../assets_extra/images/cravateseule.svg" width="30"/>
    </button>
</div>

<div id="masque"  style="display:none;text-align:center;font-size: 25px;font-weight: bold;">
    <a href="../pages_cache/enavance/">Page cachée</a>
</div>

<script>
    let bt1 = document.getElementById("bt_test");
    let d2 = document.getElementById("masque");


    function identification(){
        if (d2.style.display!= "block"){
            var reponse = prompt("Est-tu rèllement un porteur de cravate ?","<saisir un identifiant>");
            if(reponse==45){
                d2.style.display = "block";
            }
        }
    };

    bt1.onclick = identification;

</script>




# Chapitre 1 : Les couleurs

## 1. Les caractéristiques de la lumière
La lumière peut être modélisée comme une onde électromagnétique. Or quand on considère une onde, une caractéristique importante dont on peut parler est sa périodicité spatiale, que l’on appelle aussi sa longueur d’ondes, notée λ. Il s’agit de la plus petite distance séparant deux points dans le même état vibratoire, par exemples deux extremums.

<figure class="grand">
    <img  src="../images/imageCours/longueur_d_onde.png" title="longueur d'onde" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen/>
    <figcaption><a href="" target="_blank">Longueur d'onde</a></figcaption>
</figure>


L’œil humain est sensible qu’aux ondes électromagnétiques aillant une longueur d’ondes comprise entre environ 400 nm et environ 800 nm.


<figure class="grand">
    <img  src="../images/imageCours/domaines_electromagnetique.jpg" title="Pixels" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen/>
    <figcaption><a href="" target="_blank">Le domaine de la lumière visible</a></figcaption>
</figure>



**Remarque** : *cf. chapitre sur la lumière pour en savoir plus.*
<br>

## 2. Perception des couleurs, l’œil et le cerveau.
### 2.1 Descriptions du système visuel
#### A. Constitution de l'œil
<div class="division">
    <div class="division-demigauche">
        <figure class="tresgrand">
            <img src="../images/imageCours/Perception-de-la-couleur-loeil.jpg"         title="perception de la couleurs par l'œil" frameborder="0"/>
        </figure>
    </div>
    <div class="division-demidroite">
        <figure class="grand">
            <img src="../images/imageCours/fovea.jpg" title="perception de la couleurs par  k’.,qgμ.x e,,l’œil" frameborder="0"/>
            <figcaption></figcaption>
        </figure>
    </div>
</div>


Les ondes lumineuses entrent dans l’œil par la pupille, traversent le cristallin, l’humeur vitrée et frappent la rétine au niveau de la fovéa.
La rétine est tapissée de cellules photoréceptrices qui si elles sont excitées, produiront un signal nerveux, qui sera ensuite traité par le cerveau.

Les cellules photoréceptrices de la rétine sont de deux types :

<div class="division" > 
    <div class="division-demigauche">
        <figure class="grand">
            <img src="../images/imageCours/Rod_Cell.svg" title="de l'œil au cerveau" frameborder="0"/>
            <figcaption><a href="" target="_blank">Bâtonnets</a></figcaption>
        </figure>
        <p>Les bâtonnets, très sensibles à l’intensité lumineuse, ils jouent un rôle prépondérant dans la vision nocturne, mais bien qu’ils aient une influence dans la perception des couleurs leur rôle n’est ici que secondaire.<br><em>(Dans la suite de ce cours, nous négligerons leur effet pour simplifier la compréhension du mécanisme de la perception des couleurs.)</em></p>
    </div>
    <div class="division-demidroite">
        <figure class="grand">
            <img  src="../images/imageCours/620px-Cone_s.svg" title="de l’œil au cerveau" frameborder="0"/>
            <figcaption><a href="" target="_blank">Cônes</a></figcaption>
        </figure>
        <p>Les cônes, qui jouent un rôle prépondérant dans la perception de la couleur.<br>Les cônes se répartissent en trois types, suivant leur sensibilité.</p>
    </div>
</div>

<br>

#### B. Sensibilité des cônes et des bâtonnets

<figure class="grand">
    <img  src="../images/imageCours/sensibilité_oeil.svg" title="sensibilité de l'œil" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen/>
    <figcaption><a href="" target="_blank">Courbe en pointillés : <strong>bâtonnets</strong>; courbes en traits plains : <strong>cônes</strong></a></figcaption>
</figure>


Les bâtonnets *(courbe en pointillé noir)* ne différencient pas les couleurs. Ils sont plus sensibles et s’activent même aux faibles intensités lumineuses. Ils facilitent la vision nocturne, en noir et blanc.


Les cônes se répartissent en trois types, suivant leur sensibilité :

<div style="display:flex;flex-direction: row;justify-content:space-around;flex-wrap: wrap;"> 
    <div style="width:33%">
        <figure>
            <img class="img_300" src="../images/imageCours/sensibilité_cones_bleu.svg" frameborder="0"  allowfullscreen/>
            <figcaption>Cônes bleu</figcaption>
        </figure>
        <p style="width:100%;padding-left:10%;padding-right:10%">
        Les cônes S, sont sensibles aux faibles longueurs d’ondes. Si ce sont les seuls récepteurs excités l’observateur percevra la sensation bleue. On parlera parfois de cône bleu.
        </p>
    </div>
    <div style="width:33%">
        <figure>
            <img class="img_300" src="../images/imageCours/sensibilité_cones_vert.svg" frameborder="0"  allowfullscreen/>
            <figcaption>Cônes vert</figcaption>
        </figure>
        <p style="width:100%;padding-left:10%;padding-right:10%">
        Les cônes M, sont sensibles aux longueurs d’ondes moyenne. Si ce sont les seuls récepteurs excités l’observateur percevra la sensation verte. On parlera parfois de cône vert.
        </p>
    </div>
    <div style="width:33%">
        <figure>
            <img class="img_300" src="../images/imageCours/sensibilité_cones_rouge.svg"  frameborder="0"  allowfullscreen/>
            <figcaption>Cônes rouge</figcaption>
        </figure>
        <p style="width:100%;padding-left:10%;padding-right:10%">
        Les cônes L, sont sensibles aux grandes longueurs d’ondes. Si ce sont les seuls récepteurs excités l’observateur percevra la sensation rouge. On parlera parfois de cônes rouges.
        </p>
    </div>
</div>


<p><strong>Remarque :</strong> La biologie fait que nous n’avons pas tous le même pouce. Pourquoi en serait-il différemment pour nos yeux ? Ainsi il est important de comprendre que les sensibilités des cônes et des bâtonnets présentés ici sont sujets à variations d’un individu à l’autre. </p>

<figure class="grand noprint">
    <iframe src="https://ladigitale.dev/digiplay/inc/video.php?videoId=wCMGxXgypS4&vignette=https://i.ytimg.com/vi/wCMGxXgypS4/hqdefault.jpg&debut=15&fin=120&largeur=200&hauteur=113" allowfullscreen style="frameborder:0;  width:600px;height:340px"></iframe>
    <figcaption><a href="https://www.youtube.com/watch?v=wCMGxXgypS4" target="_blank">DirtyBiology - Comment créer une couleur ?</a></figcaption>
</figure>

## 3. Perception des couleurs

<figure class="grand noprint">
    <iframe src="https://www.youtube.com/embed/RK2MDkbhqL0" allowfullscreen style="frameborder:0;width:600px;height:340px">></iframe>
    <figcaption><a href="https://www.youtube.com/watch?v=RK2MDkbhqL0" target="_blank">Événement couleur: comprendre la perception et la relativité de la couleur [IDIKO-14_GENERAL]</a></figcaption>
</figure>

Les cônes excités par les ondes lumineuses arrivant sur la rétine, produisent une information qui est acheminé par le sytème nerveux au cerveau, qui va alors produire une sensation colorée.

<figure class="grand">
    <img src="../images/imageCours/lumiere_au_cerveau.jpg" title="de l'œil au cerveau" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen/>
    <figcaption><a href="" target="_blank">Traitement de l’information lumineuse</a></figcaption>
</figure>


En simplifiant beaucoup, le cerveau va recevoir un message du type :

- *moi cône M suit excité 10/10*
- *moi cône L suit excité 10/10*
- *moi cône S suit excité 0/10*
  
et va traduire ces informations par : *jaune super intense*.

**Remarque :** Nous avons vu précédemment que d’un individu à un autre la sensibilité des cônes peu changer, mais il faut aussi penser que l’interprétation faite par le cerveau aussi peut être différente. Si un peu de philosophie sur le thème vous tente, [c’est ici !](https://www.youtube.com/watch?v=JlSXc9hf1bE){:target="_blank"}



!!! danger "Important"
    La couleur perçue d’une lumière est l’impression visuelle qu’elle nous laisse. Ce n’est pas une propriété intrinsèque de la lumière ! 


### 3.1 Décompositions de la lumière blanche – couleurs spectrales
Lorsque l’on utilise un système dispersif, comme un prisme ou un réseau, sur de la lumière blanche, on sépare les ondes lumineuses suivant leur longueur d’onde.

<figure class="moyen">
    <img  src="../images/imageCours/Light_dispersion_conceptual_waves.gif" title="de l'œil au cerveau" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen/>
    <figcaption><a href="https://youtu.be/w5GtWYGCdMA" target="_blank">Décomposition de la lumière blanche</a></figcaption>
</figure>

On obtient alors le spectre de la lumière blanche.

<figure class="moyen" >
    <img src="../images/imageCours/spectre.png" title="de l'œil au cerveau" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen/>
    <figcaption>Spectre de la lumière blanche</figcaption>
</figure>

En physique, on définit la lumière blanche par :

!!! info "Définition : Lumière blanche"
    La lumière blanche est une lumière polychromatique, elle est composée d’une infinité de radiations (d’ondes électromagnétiques sinusoïdales) de longueurs d’ondes différentes.



Lorsque l’on regarde ce spectre, les radiations qui arrivent dans notre œil pour une abscisse donnée du spectre, sont constitués de longueur d’ondes identiques.
L’observation du spectre de la lumière blanche nous permet de voir l’ensemble des couleurs que l’on peut ressentir avec des radiations constituées d’une seule longueur d’onde.

<!--On parle de couleurs-->

On constate alors que certaines couleurs ne sont pas présentes dans ce spectre, comme le rose ou encore le brun !


### 3.2 Quelques limites de notre système visuel !

<figure class="grand noprint" >
    <iframe src="https://ladigitale.dev/digiplay/inc/video.php?videoId=Wdy4YBULvdo&vignette=https://i.ytimg.com/vi/Wdy4YBULvdo/hqdefault.jpg&debut=69&fin=328&largeur=200&hauteur=113" allowfullscreen style="frameborder:0;width:600px;height:340px"></iframe>
    <figcaption><a href="https://www.youtube.com/watch?v=Wdy4YBULvdo" target="_blank">DirtyBiology - Le rose n'existe pas</a></figcaption>
</figure>

#### A. Des couleurs qui n’existent pas
Aucune lumière composée de radiations de longueur d’ondes identiques, n’est capable d’exciter les cônes rouges et les cônes bleu sans exciter également le cône vert. Mais si la lumière qui rentre dans notre œil est composé de plusieurs radiations différentes, alors cette situation peut se produire. Notre cerveau interprété alors cette information comme une sensation rose.

<br>

#### B. Des couleurs qui ne sont pas ce qu’elles laissent ressentir

<div class="division"> 
    <div class="division-demigauche">
        <figure class="tresgrand">
            <img src="../images/imageCours/sensibilité_cones_vert.svg" frameborder="0"  allowfullscreen/>
            <figcaption>Lumière monochromatique</figcaption>
        </figure>
        <p>La radiation de longueur d'onde λ= nm, excite les cônes vert et rouge, mais pas pas les cônes bleu. Le signal envoyé au cerveau est du type :
            <ul>
                <li>moi cône M suit excité 10/10</li>
                <li>moi cône L suit excité 10/10</li>
                <li>moi cône S suit excité 0/10</li>
            </ul>
            Le cerveau interprète ce signal comme du jaune.
        </p>
    </div>
    <div class="division-demidroite">
        <figure class="tresgrand">
            <img src="../images/imageCours/sensibilité_cones_rouge.svg"  frameborder="0"  allowfullscreen/>
            <figcaption>Lumière Polychromatique</figcaption>
        </figure>
        <p>Les radiations de longueurs d’onde λ= nm et λ= nm, excitent les cônes vert et rouge, mais pas les cônes bleus. Le signal envoyé au cerveau est du type :
            <ul>
                <li>moi cône M suit excité 10/10</li>
                <li>moi cône L suit excité 10/10</li>
                <li>moi cône S suit excité 0/10</li>
            </ul>
            Le cerveau interprète ce signal comme du jaune également !
        </p>
    </div>
</div>


La sensibilité des cônes fait qu’il est possible que le message envoyé au cerveau soit identique, malgré le fait que les lumières utilisées ne le soient pas !
Le cerveau recevant les mêmes informations ne pourra pas faire la distinction entre ces deux lumières. Dans les deux cas, il donnera la même sensation, soit ici du jaune.


Cette limitation de notre système visuel, nous permet d’utiliser les synthèses additive et soustractive, pour tromper notre perception et produire toutes les sensations colorées que l’on souhaite.

<br>

#### C. Des couleurs qui changent suivant leur environnement
Cependant, la perception des couleurs est très complexe, car elle ne dépend pas uniquement des longueurs d’ondes qui arrivent dans l’œil depuis l’objet coloré.
Bien d’autres paramètres rentre en ligne de compte, comme le contexte d’observation.

<figure class="grand">
        <img  src="../images/imageCours/brun.svg" title="Pixels" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen/>
        <figcaption><a href="https://youtu.be/Wdy4YBULvdo?t=229" target="_blank">Impact de l'environement sur la perception d'une couleur</a></figcaption>
</figure>

Par exemples, une même couleur paraîtra orange dans un contexte sombre et brun dans un contexte clair. <br>
L’étude de la perception des couleurs est alors davantage une question artistique que de physique.


## 4. La production de couleur
### 4.1 La synthèse additive : RVB 
Elle consiste à combiner de la lumière Rouge, Verte et le Bleu (RVB ou RGB en anglais).

<div class="division">
    <div class="division-demigauche">
        <figure class="grand">
            <img  src="../images/imageCours/rsynthAdd.gif" title="Synthèse aditive" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen/>
            <figcaption><a href="https://www.physicsclassroom.com/Physics-Interactives/Light-and-Color/RGB-Color-Addition/RGB-Color-Addition-Interactive" target="_blank">Synthèse additive</a></figcaption>
        </figure>
    </div>
    <div class="division-demidroite">
        <figure class="grand">
            <img loading="lazy" src="../images/imageCours/synthese_aditive.svg" title="Synthèse aditive" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen/>
            <figcaption><a href="https://www.youtube.com/watch?v=pLSbRs-mpEI" target="_blank">Synthèse additive</a></figcaption>
        </figure>
    </div>
</div>


#### Applications
<img class="petit droite" src="../images/imageCours/pixels-ecran.jpg"/>Les écrans utilisent la synthèse additive pour restituer les couleurs. Ils sont constitués de pixels composés de trois luminophores, un rouge, un vert et un bleu. Chaque luminophore possède 256 niveaux d’éclairements différents, ce qui permet la restitution de plus de 16,7 millions de couleurs différentes.
Les capteurs des APN fonctionnent un peu comme l’œil humain, ils possèdent des capteurs pour le Rouge, le Vert, le Bleu et ne voit qu’en RVB !

Fabrication des couleurs par un écran : <a href="https://physique-chimie.discip.ac-caen.fr/spip.php?article915" target="_blank">*animation,*</a><a href="https://www.youtube.com/watch?v=3BJU2drrtCM" target="_blank">*vidéo*</a>


### 4.2 La synthèse soustractive : CMJ et CMJN
Elle consiste à combiner de la matière Cyan, Magenta et Jaune (CMJ ou CMY en anglais).

<figure class="grand">
        <img  src="../images/imageCours/synthese_soustractive.svg" title="Pixels" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen/>
        <figcaption><a href="http://physique.ostralo.net/syntheses_couleurs/" target="_blank">Synthèse soustractive</a></figcaption>
</figure>


Les pigments utilisés dans la synthèse soustractive des couleurs absorbent, en fonction de leurs propriétés physico-chimiques, une partie des rayonnements du faisceau de la lumière. Les pigments agissent en quelque sorte comme des filtres. On dit soustractive, car on soustrait à la lumière incidente une partie de ces rayonnements.

Prenons le cas du jaune. En mettant un pigment jaune sur un support blanc, le faisceau bleu de la lumière est absorbé par le pigment jaune grâce à sa composition chimique. Le pigment jaune laissant passer les faisceaux rouges et verts restant, notre œil voit du jaune. En effet, en synthèse additive, l’addition qu’un faisceau rouge et d’un faisceau vert donne du jaune.


<figure class="moyen">
        <img  src="../images/imageCours/le-jaune-en-synthèse-soustractive-des-couleurs.png"/>
        <figcaption>Interaction d’un pigment jaune avec de la lumière blanche</figcaption>
</figure>

**Remarque :** En imprimerie, on ajoute une quatrième couleur : le noir. Il permet d’avoir des noirs profonds. On parle alors de CMJN (ou CMYK – K pour Key, la valeur ou luminosité). C’est de la quadrichromie. 



### 4.3 Couleurs primaires, secondaires et complémentaires
L’expérience montre qu’on peut reconstituer toutes les couleurs que l’œil peut percevoir grâce à un mélange de trois couleurs primaires, dans diverses proportions.

!!! info "Définition : Couleurs primaires"
    Trois couleurs sont mutuellement primaires, s’il est impossible de reconstituer l’une d’entre elles à partir des deux autres.

!!! info "Définition : Couleurs secondaires"
    Le mélange de deux couleurs primaires donne une couleur secondaire.

!!! info "Définition : Couleurs complémentaires"
    Deux couleurs sont complémentaires si leur addition donne du blanc *(en synthèse additive)* ou du noir *(en synthèse soustractive)*. 


Rouge et Cyan sont complémentaires :
<img  class="img_200" style="display:block;margin:auto" src="../images/imageCours/rccomplementaire.png"/>


Jaune et Bleu sont complémentaires :
<img  class="img_200" style="display:block;margin:auto" src="../images/imageCours/jbcomplementaire.png"/>


Magenta et Vert sont complémentaires :
<img  class="img_200" style="display:block;margin:auto" src="../images/imageCours/mvcomplementaire.png"/>


## 5. La couleur des objets
### 5.1 Rappels

Lorsque la lumière éclaire un objet,

- une partie est ***transmise*** à travers le second milieu (si l’objet est transparent) ;
- une partie est ***absorbée*** (si le milieu absorbe à cette fréquence) ;
- une partie est ***diffusée***.

**Remarque :** On dit qu’une lumière diffusée est réfléchie lorsqu’elle est diffusée dans une seule direction.

### 5.2 Interactions lumière – matière

!!! info "À retenir"
    La couleur des objets dépend des radiations qu’ils diffusent.

<br>


#### A. Quelle est la couleur d’un objet éclairé par une lumière blanche ?

Une partie de la lumière blanche reçue est absorbée tandis que l’autre est diffusée et donne sa couleur à l’objet :
un objet vert absorbe toutes les couleurs de la lumière blanche, sauf le vert.

<figure class="grand">
    <img  src="../images/imageCours/couleur_objet.gif" title="de l’œil au cerveau" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen/>
    <figcaption><a href="https://web-labosims.org/animations/couleur_objet2/couleur_objet.html" target="_blank">Couleurs de quelques objets</a></figcaption>
</figure>


!!! info "Définition : Couleur propre"
    La couleur propre d’un objet est celle que l’on observe lorsqu’il est éclairé en lumière blanche.

**Exemples:**

- Un objet dont la couleur propre est le blanc prend la couleur de la lumière qui l'éclaire, car il diffuse toutes les lumières colorées.
- Un objet noir ne diffuse aucune lumière. Il les absorbe.
- Un objet dont la couleur propre est rouge diffuse les longueurs d’ondes excitant les cônes rouges et absorbe toutes les autres radiations du visible.

<br>

#### B. Quelle est la couleur d’un objet éclairé par une lumière colorée ?

!!! info "Définition : Couleur apparente"
    La couleur apparente d’un objet est celle que l’on observe lorsqu’il est éclairé avec une lumière différente de la lumière blanche.

Si l’objet éclairé avec une lumière ne contient pas de lumière rouge alors l’objet ne diffusera aucune lumière, il apparaîtra noir.

**Exemples:**

- Un objet blanc prend la couleur de la lumière qui l’éclaire, car il ne peut diffuser que les radiations qu’il a reçu *(sauf s’il est fluorescent)*.
- Un objet noir ne diffuse aucune lumière. Il restera noir quelle que soit la lumière qui l’éclaire.
- Un objet rouge diffuse uniquement les radiations rouges. Ainsi, si la lumière qui l’éclaire comporte des radiations rouges, il les diffusera et paraîtra rouge, dans tout les autres cas, il ne diffusera aucune lumière, il paraîtra noir.



??? faq "S'exercer"
    <a href="https://www.physicsclassroom.com/Physics-Interactives/Light-and-Color/Filtering-Away/Interactive" target="_blank">Animation 1,</a>
    <a href="https://www.physicsclassroom.com/Physics-Interactives/Light-and-Color/Stage-Lighting/Stage-Lighting-Interactive" target="_blank"> Animation 2</a>

    1. Tentez de prédire la couleur perçue des objets lorsqu’ils seront éclairés par une source colorée.
    2. Jouer l’animation pour vérifier vos résultats.


<br>

??? note "Pour aller plus loin"
    **Un peu de philosophie :**
    - [Monsieur Phi_J'ai mal](https://www.youtube.com/watch?v=JlSXc9hf1bE)


??? note "Ressources"
    -[Johannes itten- art de la couleur(pdf)](https://app.box.com/s/p6pd7pappf93vqi6qasz)
    -[jeufiltre](https://www.physicsclassroom.com/Physics-Interactives/Light-and-Color/Color-Filters/Color-Filters-Interactive)
    - [exercices](https://fr.calameo.com/read/006013151d1ad20003156?authid=LiJkSVNqlZf0)
    - [dlateyte](https://dlatreyte.github.io/premieres-pc/chap-5/2-couleur-objets/)
    - [scienceetonante](https://www.youtube.com/watch?v=FvbNrwjIrNU)
    - [https://www.guide-gestion-des-couleurs.com](https://www.guide-gestion-des-couleurs.com/oeil-perception-couleurs.html)
    - [astrosurf](http://www.astrosurf.com/luxorion/apn-ir-uv3.htm)
    - [lne.fr](https://www.lne.fr/fr/comprendre/systeme-international-unites/candela)
    - [leclairage.fr](https://leclairage.fr/th-couleurs/)
    - [tube-a-essai.fr](https://tube-a-essai.fr/1ere-spe-chapitre-19-couleurs/)
    - [123couleurs.fr](https://www.123couleurs.fr/explications/explications-vision/tv-visioncouleurs/)
    - [openclassrooms.com](https://openclassrooms.com/fr/courses/5060661-initiez-vous-aux-traitements-de-base-des-images-numeriques/5060668-explorez-l-oeil-la-perception-des-images-et-l-image-numerique)
    - [alliot.fr](https://www.alliot.fr/COLOR/color.html)
    - [impritex.eu](https://www.impritex.eu/qu-est-ce-que-la-couleur/)
    - [Événement couleur: comprendre la perception et la relativité de la couleur [IDIKO-14_GENERAL]](https://www.youtube.com/watch?v=RK2MDkbhqL0)
    - [palonssciences.ca](https://parlonssciences.ca/ressources-pedagogiques/les-stim-en-contexte/comparaison-de-loeil-et-dun-appareil-photo)
    - [istockphoto.com](https://www.istockphoto.com/fr/portfolio/Graphic_BKK1979?mediatype=illustration)
    - [lelivrescolaire.fr](https://www.lelivrescolaire.fr/page/6903049)

??? note "Trouver une couleur"
    - [color.adobe.com](https://color.adobe.com/fr/create/color-contrast-analyzer)
    - [paletton.com](https://paletton.com/#uid=1000u0kllllaFw0g0qFqFg0w0aF)
    - [pourpre.com](http://pourpre.com/fr/nuanciers/)
