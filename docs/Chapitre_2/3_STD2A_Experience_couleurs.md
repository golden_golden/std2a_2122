---
title : Expérience
---

# Un peu de pratique

Dans ce TP nous allons réaliser une image en utilisant le principe de la trichromie.

## 1. Le matériel

- Téléphone mobile en mode noir et blanc.
- Filtres de couleurs Rouge, Vert et Bleu.
- Logiciel GIMP.

## 2. La prise de vue

Choisir une scène dans laquelle, il y a de la couleur !
*La présence d’une zone blanche, peut faciliter les réglages ultérieurs.*<br>
Bien mémoriser dans quel ordre ont été utilisés les filtres colorés, *je vous conseille l’ordre RVB !*<br>
Ne pas bouger entre les prises de vue, *utilisez un trépied ou à défaut un support*.<br>
Utilisez les réglages manuels, *si votre appareil le permet, afin d’avoir exactement les mêmes réglages pendant les trois prises de vue.*<br>
Amusez-vous, soyez créatifs !


## 3. Les manipulations dans GIMP
### 3.1 Préparer les images
#### A. Découper, recadrer les images
Ouvrir la ou les images dans GIMP.<br>
Utiliser l’outil de recadrage.<br>
*Dans les paramètres de l’outil, fenêtre de gauche, il est possible de fixer les proportions !*<br>
Sélectionner la zone qui vous intéresse de l’image, et taper sur la touche entrée du clavier.

<figure>
    <img class="img_900" src="../images/imageexperiences/recadrage.gif" title="Travail du groupe 1" frameborder="0" allowfullscreen/>
    <figcaption>Recadrage dans GIMP</figcaption>
</figure>




#### B. Enregistrer les images en .png
Pour enregistrer les images en png, afin qu’elles aient de la transparence.<br>
Cliquer sur l’onglet "Fichier" puis sélectionner dans le menu déroulant "Exporter sous…".<br>
Saisir un nom adapté et l'extension de format .png, ce qui donne par exemple *"rouge.png"*.<br>
Appuyer sur "Exporter", une nouvelle fenêtre s’ouvre vérifier que la casse "Enregistrer la couleur d’arrière-plan" est bien décochée et cliquer sur le bouton "Exporter".<br>

**Remarque :** Si vous utilisez les images de [Prokudin Gorskii(1863-1944)](https://fr.wikipedia.org/wiki/Sergue%C3%AF_Prokoudine-Gorski) disponibles et libre de droit grâce à la [bibliothèque du Congrès aux États Unis.](https://www.loc.gov/pictures/search/?st=grid&co=prok), l'ordre des calques de haut en bas est **bleu, vert et rouge**. Si vous vous intéressez à la prise de vue, voici un photo d’un appareil similaire a celui utilisé par [Prokoudine](http://www.vintagephoto.tv/mb.shtml).

<br/>

L'image de Prokudin Gorskii qui sert d'exemple dans la suite est disponnible : [https://www.loc.gov/pictures/item/2018681340/](https://www.loc.gov/pictures/item/2018681340/).

<br>


### 3.2 Réalisations de l’assemblage
#### A. Importer les images dans GIMP
Ouvrir une des images normalement, "Fichier" puis "Ouvrir…"<br>
Modifier/vérifier que l’image est en mode RVB, sinon vous ne pourrez pas colorer vos calques ! Pour cela, il faut se placer sur l’image, faire un clic droit avec la souris, sélectionner "Images" puis "Mode" et finalement "RVB". <br>

<figure>
    <img class="img_900" src="../images/imageexperiences/ModeRVB.png" title="Passer en mode RVB" frameborder="0" allowfullscreen/>
    <figcaption>Mode RVB</figcaption>
</figure>




Pour importer les images suivantes, il est nécessaire de les ouvrir en tant que calques. "Fichier" puis "ouvrir en tant que calques".<br>

#### B. Régler le paramétrage des calques
Pour que les images se superposent, il est nécessaire de changer le mode des deux calques supérieur.<br>
Il faut choisir le mode "écran".<br>
Le calque de base reste en mode "normal".<br>

#### C. Caler les images
Appuyer sur l’œil du troisième calque, pour ne conserver la vue que des deux premiers calques. <br>
Cliquer sur le second calque.<br>
Sélectionner "l'outil déplacement".<br>
Zoomer sur un détail caractéristique pour faciliter un calage de précision (Ctrl+roulette de la souri).

#### D. Colorier les images
Sélectionner le calque.<br>
Se placer sur l’image.<br>
Clique droit de la souri.<br>
Sélectionner "Couleurs", puis "Colorier…".<br>

<figure>
    <img class="img_500" src="../images/imageexperiences/coloriser.png" title="Colorier une images" frameborder="0" allowfullscreen/>
    <figcaption>Colorier une image</figcaption>
</figure>


Bouger les curseurs, pour obtenir les réglages du tableau ci-dessous.<br>

<figure>
    <img class="img_500" src="../images/imageexperiences/coloriser2.png" title="Colorier une images" frameborder="0" allowfullscreen/>
    <figcaption>Les réglages</figcaption>
</figure>


Puis "Valider", et faire de même pour les deux autres calques.




!!! info "Réglages :"
    Quelques réglages de base qui devrait vous permettre d’obtenir une image en couleurs.
    
    <center>
    <table>
        <thead>
            <tr>
                <th>Couches</th>
                <th>Rouge</th>
                <th>Verte</th>
                <th>Bleu</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Teinte</td>
                <td>0</td>
                <td>0,33</td>
                <td>0,66</td>
            </tr>
            <tr>
                <td>Saturation</td>
                <td>0,75</td>
                <td>0,75</td>
                <td>0,75</td>
            </tr>
            <tr>
                <td>Luminosité</td>
                <td>-0.30</td>
                <td>-0.30</td>
                <td>-0.30</td>
            </tr>
        </tbody>
    </table>
    </center>

    Il sera ensuite nécessaire de peaufiner autour de ces réglages pour obtenir un résultat satisfaisant !






#### D. Ajuster le rendu
Le rendu final, ne sera certainement pas entièrement satisfaisant, généralement une teinte domine.<br>
Pour un réglage plus fin, sélectionner le calque. <br>
Puis faire un clic droit sur l’image, sélectionner "Couleurs" puis "Luminosité-Contraste…".<br>

<figure>
    <img class="img_600" src="../images/imageexperiences/luminosite_contraste.png" title="Travail du groupe 1" frameborder="0" allowfullscreen/>
    <figcaption>Réglages final</figcaption>
</figure>

Jouer avec les curseurs, jusqu’à satisfaction.<br>
Faire de même avec les autres calques.<br>
Bon courage !




**Remarque :** Il existe des méthodes bien moins empiriques pour obtenir un très bon résultat, mais elles demandent plus de matériel et/ou sont bien plus techniques.



### 3.3 Exporter l’image finale
Une fois que l’image finale vous satisfait, vous pouvez :

- enregistrer votre travail, ce qui vous permettra d’y retravailler plus tard si vous le désirez.
- exporter votre image, si vous choisissez le format .jpg les couches vont être fusionner et l’image obtenue ressemblera au rendu final.

## 4. Quelques résultats
<figure>
    <img class="img_300" src="../images/imageexperiences/photos_trichromie_de_Craies.png" title="Travail du groupe 1" frameborder="0" allowfullscreen/>
    <figcaption>Craies</figcaption>
</figure>

## 5. Les travaux de quelques artistes
Travail de Hery Gaud
les cathédrales
<figure>
    <img class="img_500" src="../images/imageexperiences/reims-10.jpg" title="Travail de Hery Gaud" frameborder="0" allowfullscreen/>
    <figcaption>Cathédrale de Reins</figcaption>
</figure>

Travail de Clément Darrasse
[son travail](https://blog.grainedephotographe.com/expo-photo-trichromie-clement-darrasse/)

## 6. Utilisation des principes de la trichromie, dans la pratique astrophotographique
[Bastien Foucher](https://www.bastienfoucher.com/tutoriels/palette-hubble-12/)



## 7. Ressources
??? note "Resources :"

    - [Banque d'image d'époque - Library of congress ](https://www.loc.gov/collections/prokudin-gorskii)
    - [Pour en savoir plus sur Prokudin-Gorskii](https://pirandello.org/arts_sciences/technique_photographe.pdf)
    - [Le forum sur lequel j'ai trouver la méthode - galerie photo](http://www.galerie-photo.info/forumgp/read.php?3,80523,80567)
    - [Une autre sources que j'ai utilisé](http://archive.brhfl.com/ph.brhfl.com/2012/09/07/gimp-trichromes/)
    - [Henry gaud - La Trichromie pose la bonne question ](https://galerie-photo.com/trichromie.html)
    - [Henry gaud - Henri Gaud : plat du jour,travail en trichromie](https://galerie-photo.com/henri-gaud-plat-du-jour.html)
    - [Appareil spéciaux pour faire de la trichromie](http://www.vintagephoto.tv/scouts2.shtml)
    - [Acheter de filtre pas trop cher pour commencer - achat à plusieurs](https://www.thomann.de/fr/lee_farbfolie_nr106_primary_red.htm?sid=bbda451ac26149d7c210a3757fcaa5fa)
    - [Acheter de filtre pas trop cher pour commencer - achat seul](https://www.thomann.de/fr/rosco_077_par64_set.htm)
