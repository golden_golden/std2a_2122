---
title : "Cours"
correctionvisible : true
pdf : "colorimetrie"
---


<div class="noprint" style="text-align:right;width:100%;background-color:white">
    <a href="../pdf/{{page.meta.pdf}}.pdf" 
    style="display:inline-block" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="#a3a3a3" stroke-width="1.5" stroke-linecap="butt" stroke-linejoin="arcs"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg>
    </a>
    <button id="bt_test">
        <img src="../../assets_extra/images/cravateseule.svg" width="30"/>
    </button>
</div>

<div id="masque"  style="display:none;text-align:center;font-size: 25px;font-weight: bold;">
    <a href="../pages_cache/enavance/">Page cachée</a>
</div>

<script>
    let bt1 = document.getElementById("bt_test");
    let d2 = document.getElementById("masque");


    function identification(){
        if (d2.style.display!= "block"){
            var reponse = prompt("Es-tu réellement un porteur de cravate ?","<saisir un identifiant>");
            if(reponse==45){
                d2.style.display = "block";
            }
        }
    };

    bt1.onclick = identification;
</script>




# Chapitre 2 : Un peu de colorimétrie



## 1 Coder les couleurs
### 1.1 La trivariance visuelle
Le principe de la trivariance énonce que, pour la perception visuelle, toute couleur peut être reproduite par un mélange de trois couleurs, dites primaires, en proportion unique.

#### A. Un peu de vocabulaire

!!! info "Métamère"
    Deux couleurs métamères, ou homochromes, sont deux lumières visibles dont le spectre physique est différent, mais que la vision humaine ne différencie pas. 



<figure class="grand">
    <img  src="../images/imageCours/metameres.png" title="metameres" frameborder="0"/>
    <figcaption>Bien que ces six sources aient des spectres différents, la couleur perçue par la plupart des gens sera le même violet.</figcaption>
</figure>

**Remarque :** Il faut cependant bien comprendre que les métamères s’égalisent « perceptuellement » mais pas physiquement.
Ainsi deux peintures peuvent être des métamères sous une source lumineuse mais pas sous une autre, ce qui peut poser problème lors de la restauration de peinture par exemple.




### 1.2 Le modèle RVB *(ou RGB)* pour coder une couleur
Pour coder une couleur, il suffit donc de trois nombres mesurant les quantités de rouge, de vert et de bleu.

#### A. Résolution tonale : La quantification
La résolution tonale, c’est-à-dire le nombre de valeur possible que l’on peut restituer vas avoir une influence sur les nuances de couleurs que le dispositif va être en mesure de créer.

<div class="division" >
    <div class="division-demigauche">
    <figure >
        <img src="../images/imageCours/codeRVB3bits.png" title="RVB 3 bits" frameborder="0"/>
        <figcaption>RVB codé sur 3 bits soit 8 variations pour chaque canal</figcaption>
    </figure>
    </div>
    <div class="division-demidroite">
    <figure >
        <img src="../images/imageCours/codeRVB4bits.png" title="RVB 4 bits" frameborder="0"/>
        <figcaption>RVB codé sur 4 bits soit 16 variations pour chaque canal</figcaption>
    </figure>
    </div>
</div>

Avec un codage sur $8 bits$, on peut coder $256×256×256>16 Millions$ de couleurs.

Plus le nombre de bits utilisé pour coder les couleurs est important, plus il sera possible de restituer les nuances.
Cependant, ces informations supplémentaires devront être stockées !

<!-- concevoir une image, même photo (si possible avec un ciel) mais échantillonnage ≠, légende poids de chaque photo et échantillonnage -->


<br>

**Remarque :** Pour atteindre les capacités humaines à discerner les nuances visuelles, il faudrait coder les couleurs sur au moins $12 bits$ par composantes.

Cependant même si l’on utilise une résolution suffisante l’exactitude de la couleur restituée par un dispositif utilisant un code RVB, n’est pas du tout garantie ! 

<br>

<figure class="petit droite">
    <img  src="../images/imageCours/televiseur.jpg" title="variation couleurs mur de téléviseur" frameborder="0"/>
    <figcaption>Aucune couleur n’est identique malgré le fait que ces téléviseurs diffusent le même flux vidéo !</figcaption>
</figure>

#### B. Couleurs primaires


Le problème intervient lorsque l’on compare les couleurs. La couleur produite par deux appareils différents pour le même code RVB, est en général *(sans calibration)* différente !

<!-- concevoir une image, deux carrés verts codé par 255,0,0 mais ≠ -->
Les couleurs primaires utilisées par un appareil lui sont propre !



## 2. Colorimétrie
### 2.1 Pourquoi la colorimétrie
La couleur est une sensation important !
Les enjeux économiques liés à la couleur et à sa bonne restitution sont nombreux ; un produit peut être un échec commercial uniquement à cause de sa couleur, un article acheté sur le web peut être une source de déception lors de sa réception, car la couleur affichée sur le moniteur et celle de l’objet ne sont pas identiques,…

<!-- argumentation sur l’importance de la couleur dans notre société -->



### 2.2 Qu'est-ce que la colorimétrie

!!! info   "**Colorimétrie** :"
    La colorimétrie est la discipline psychophysique qui se donne pour objectif la mesure de la couleur. Elle relie des mesures physiques effectuées sur la lumière aux perceptions colorées.



Posséder une codification standardisée des couleurs va permettre de :

- Savoir s’il est possible de capter ou reproduire exactement une sensation colorée sur un appareil ou support donné.
- De faire en sorte que la couleur captée, soit celle visualisée et restituée.
- …



## 3. Des standards pour "cartographier" les couleurs
### 3.1 Standardisations

Il est possible de définir la couleur comme étant la perception visuelle humaine de la répartition spectrale de la lumière.
Cependant, le rapport entre l’analyse spectrale du rayonnement lumineux et la perception des couleurs est complexe, et dépend de nombreux paramètres, comme la nature de la source lumineuse, l’observateur…

Afin d’établir un système le plus univoque possible, il est nécessaire d’établir des standards, pour les caractéristiques de la source lumineuse, pour les caractéristiques de l’observateur…


### 3.2 Sources de lumières standardisée
La vision s’adapte aux conditions d'éclairage et perçoit les rapports entre les couleurs de manière presque identique avec des lumières dont le spectre est très différent.
Pour que l’on puisse caractériser une couleur, il est nécessaire de connaître les caractéristiques de la source de lumière utilisée pour son observation. Il a donc été nécessaire d’élaborer des standard pour les sources de lumière, que l’on nomme alors illuminant.

!!! info "**illuminant**"
    En colorimétrie, un illuminant est la lumière qui éclaire un objet. Ce sont des sources théoriques, basé sur des moyennes et définit par des normes.


Les illuminants A, B et C ont été introduits en 1931 avec pour but de représenter respectivement la lumière moyenne d’une lampe à incandescence, la lumière directe du soleil et la lumière du jour. Les illuminants D quant à eux représentent des phases de la lumière du jour (par exemple D65 pour un ciel nuageux). L’illuminant E est l’illuminant d’énergie égale, tandis que l’illuminant F représente diverses lampes fluorescentes de compositions variées.

<!---[wikipédia_illuminant](https://fr.wikipedia.org/wiki/Illuminant)--->

Une des caractéristiques principale d’un illuminant est ce que l’on appelle sa température de couleur du point blanc.
Mais pour comprendre cette notion, il est nécessaire de s’intéresser au rayonnement des corps noir.
 

#### A. Rayonnement des corps noir 
<div class="division">
<div class="division-tiergauche">
    <figure>
        <img  src="../images/imageCours/rayonnement_corps_noir.png" title="rayonnement du corps noir" frameborder="0"/>
    <figcaption><a href="https://www.youtube.com/watch?v=TBXze1ZV6NE" target="_blank">Émission énergétique du corps noir aux différentes températures thermodynamiques</a></figcaption>
    </figure>
</div>
<div class="division-deuxtierdroite">
    Un corps noir est un corps physique théorique, qui lorsqu’il est frappé par est une énergie quelconque va l’absorbé intégralement et va chauffer. Il va alors émettre un rayonnement.

    Pour une certaine plages de température d’équilibre du corps noir, le rayonnement émis vas correspondre avec le domaine du visible.

    <br>
    <br>

    <strong>Remarques :</strong> Les objets chauffés se comportent quasiment comme des corps noirs.

    <!---[Planck (physique fondamentale) - Passe-science #29](https://www.youtube.com/watch?v=TBXze1ZV6NE)--->
</div>
</div>

### 3.3 L’œil standard
La CIE (Commission internationales de l'Éclairage) à réaliser de nombreuses expériences, sur la comparaison de couleurs et la perception de celles-ci comme étant métamères sous un illuminant donné, par un grand nombre d'individu.

<!--- image protocole expérimental --->

Ces expériences ont permis de définir un "observateur standard" qui aurait une "vision normale des couleurs".




### 3.4 Comment décrire une couleur

Toute sensation lumineuse peut être entièrement caractérisée par trois variables indépendantes :


!!! info "**La luminance** :"
    La luminance correspond à l’intensité de la lumière perçue et mesurable. On dit de la source qu’elle est "intense" ou "faible", que l’objet est "clair" ou "foncé".

!!! info "**La longueur d’onde dominante (ou la teinte)** :"
    La teinte (ou longueur d’onde dominante) correspond à la couleur de la lumière perçue. C’est une grandeur repérable *("c’est rouge")* mais pas mesurable. On l’assimile à la longueur d’onde de la lumière qui produit la même sensation.

!!! info "**La saturation (ou pureté d’excitation)** :"
    La saturation est reliée au pourcentage de la lumière blanche qui s’ajoute à la teinte donnée. Une couleur pure, sans lumière blanche, est dite saturée, et plus on rajoute de lumière blanche, plus la teinte est lavé (jusqu’à l’extrême, devenir blanche.)

Partant du principe que la couleur est ce qui différencie deux lumières d’égale intensité, on peut considérer que deux grandeurs définissent la couleur :

- la longueur d’onde dominante ;
- la pureté colorimétrique, qui est la proportion de l'intensité due à la longueur d'onde dominante.


!!! info "**Chromaticité** :"
    On suppose que la coloration est une propriété indépendante de la luminosité, et on l’appelle chromaticité. Il reste deux valeurs, qui peuvent alors se représenter sur un plan.



## 4. Le diagramme de chromacité
### 4.1 Qu’est-ce que le diagramme de chromacité



En colorimétrie, un diagramme de chromaticité est une représentation cartésienne où les points représentent la chromaticité des stimulus de couleur.

<!---[wiki-CIExy](https://fr.wikipedia.org/w/index.php?title=CIE_xyY&oldid=164524427)--->

La CIE a établi en 1931 le diagramme de chromacité xyY, modélisant toutes les couleurs visibles par l’œil humain. L’intérieur du diagramme dans le plan (x, y) représente toutes les chromaticités perceptibles.

Deux couleurs de même chromaticité ne se ressemblent pas nécessairement ; un rose et un brun peuvent occuper le même point, s’ils ne diffèrent que par la luminosité. 



<figure>
    <img src="../images/imageCours/e.png" title="Diagramme chromacité xyY" frameborder="0"/>
    <figcaption><a href=https://fr.wikipedia.org/wiki/Diagramme_de_chromaticit%C3%A9>image wiki</a></figcaption>
</figure>




### 4.2 Descriptions du diagramme de chromacité

<figure class="grand">
    <img src="../images/imageCours/Diagramme.png" title="Diagramme chromacité xyY" frameborder="0"/>
    <figcaption>Diagramme chromacité xyY</figcaption>
</figure>



Le diagramme est constitué de deux courbes et d’un point de référence.

- Le ***lieu du spectre*** — *ou spectrum locus* — est la courbe en forme de fer à cheval. Elle représente l’ensemble des ondes électromagnétique monochromatique appartenant au domaine du visible, c’est pourquoi le lieu du spectre est gradué selon la longueur d’onde de 380 nanomètres (nm) à 700 nm.<br>
Les couleurs présentes sur le lieu du spectre sont les plus saturées possibles, ce sont des couleurs pures à 100 %.


- ***"la ligne (ou droite) des pourpres"*** est la droite oblique en bas. Elle contient des couleurs qui ne peuvent être obtenues qu’en mélangeant des rouges et des bleu-violet.


- ***Le point blanc*** : il s’agit du point blanc de l’illuminant de référence. Il correspond aux couleurs les moins saturées possibles (ou les plus lavées). Il a souvent les coordonnées x=1/3 ; y=1/3.



### 4.3 Lire le diagramme de chromacité

Chaque couleur est représentée par un couple de coordonnées trichromiques $(x;y)$ qui va permettre de déterminer sa teinte et sa saturation.

Une demi-droite ayant le point blanc pour origine, représente toutes les couleurs possédant une même teinte, plus ou moins lavées de blanc. Elles ont toutes la même longueur d’onde dominant.

La position d’un point entre le blanc et la teinte pure située sur le bord de la figure définit la proportion de teinte et de blanc, c’est-à-dire la saturation. En divisant la distance au point au point blanc par celle du segment entier on obtient la pureté d’excitation.


Pour restituer la sensation lumineuse, il est nécessaire d’ajout à la chrominance, la luminance, noté Y, absente du spectre, d’où le nom "xyY" donné au diagramme.

<br>

#### A. Détermination de la longueur d'onde dominante et de la pureté d’excitation
<div class="division">
<div class="division-demigauche">
    <figure>
        <img  src="../images/imageCours/dominate_purete.png" title="Diagramme chromacité xyY" frameborder="0"/>
        <figcaption>Détermination de la longueur d’onde dominante</figcaption>
    </figure>
</div>
<div class="division-demidroite">
    <strong>Exemple :</strong><br> 
    Soit un point M de coordonées trichromiques <em class="math">x<sub>M</sub> = 0.14 ; y<sub>M</sub> = 0.54</em>
    <ul>
        <li>Sa longueur d'onde dominate est l'intersection entre la droite EM et le lieu du spectre.</li>
        <li>Sa pureté d'excitation (saturation) est le rapport des longueurs EM et EM' mesurées à la règle.<br>
        <em class="math">
        Pureté d'excitation =
        <math xmlns="http://www.w3.org/1998/Math/MathML">
            <mfrac>
                <mi>EM</mi>
                <mi>EM'</mi>
            </mfrac>
        </math>
        = 0.56
        </em> ou encore <em class="math">56%</em>.</li>
    </ul>

    En résumé : le point M correspond donc à un vert à <em class="math">56%</em> de saturation.
</div>
</div>



#### B. Cas des pourpres

<div class="division">
<div class="division-demigauche">
    Ces couleurs ne sont pas dans le spectre de la lumière visible. Il est par conséquent impossible de déterminer leur longueur d’onde dominante. Si le point M est situé sous le point E, par convention, on détermine la longueur d’onde de sa couleur complémentaire, qu’on affecte d’un signe "moins".<br>

    <br>

    <strong>Exemple :</strong><br>
    Un point M de coordonées trichromatiques <em class="math">x<sub>M</sub> = 0.30 ; y<sub>M</sub> = 0.2</em>
    
    Sa longueur d'onde dominante vaut <em class="math">-557nm</em> et sa pureté d'excitation     
    <em class="math">
        <math xmlns="http://www.w3.org/1998/Math/MathML">
            <mfrac>
                <mi>EM</mi>
                <mi>EM'</mi>
            </mfrac>
        </math>
        = 0.41 = 41%.
        </em>
    <br>

    <br>

    En résumé : Le point M est un pourpre, saturé à <em class="math">41%</em>, dont la couleur complémentaire a pour longueur d’onde <em class="math">-557nm</em>.
</div>
<div class="division-demidroite">
    <figure>
        <img  src="../images/imageCours/cas_pourpre.png" title="cas des pourpres" frameborder="0"/>
        <figcaption>Cas des pourpres</figcaption>
    </figure>
</div>
</div>

??? faq "Exercice d'application"
      <figure class="tresgrand">
         <img  src="../images/imageCours/CIExy1931_fixed.svg" title="Diagramme chromacité xyY" frameborder="0"/>
         <figcaption>à faire</figcaption>
      </figure>

      En utilisant le diagramme de chomaticité ci-dessus, déterminer les longueurs d'ondes dominantes et les puretés d’excitation des points suivants :

     - $A(x_A = 0.45 ; y_A = 0.45)$
     - $B(x_B = 0.10 ; y_B = 0.75)$
     - $C(x_C = 0.63 ; y_C = 0.28)$




#### C. Mélange de couleur
Le mélange de la couleur $C_1$ de longueur d’onde dominante $λ_1$ et d’une couleur $C_2$ de longueur d’onde dominante $λ_2$ donne une couleur d’onde $C_3$ de longueur d’onde dominante $λ_3$.

<figure class="grand">
    <img  src="../images/imageCours/mélange de couleur.png" title="mélange de couleur" frameborder="0"/>
    <figcaption>Longueur d'onde dominante de la couleur obtenue suite a un mélange de couleur</figcaption>
</figure>

<!---[doc:Insia_p113](https://people.rennes.inria.fr/Eric.Marchand/ESIR/ESIR2/BINP/couleur.pdf)--->

-----


La pureté d’excitation correspond approximativement à la notion de coloration, indépendamment de la teinte. Les couleurs délavées ou grisâtres ont une faible pureté d’excitation. Mais on perçoit toujours les couleurs sombres comme moins colorées que les couleurs lumineuses, alors que leur pureté d'excitation peut être proche de 18.

Pour un système d’affichage par synthèse additive, tel qu’un écran d’ordinateur, le triangle formé par les trois points correspondant aux trois couleurs primaires représente le gamut du système. Seules des couleurs représentées par un point situé à l'intérieur du triangle peuvent être reproduites, à condition que leur luminosité ne dépasse pas un maximum qui dépend de leur distance aux primaires.




## 4. Les espaces de couleurs – le gamut
### 4.1 Qu’est-ce qu’un gamut
Un espace couleur est un ensemble couleurs. On appelle cela également un gamut. Il représente l’ensemble des couleurs qu’un dispositif (appareil photo), peut prendre en photo.

Le modèle de gestion des couleurs des appareils techniques (écran, appareil photo, scanner…) est basé sur le système RVB de l’œil. Cependant, chaque appareil va gérer les couleurs "à sa manière". Le "gamut" représente la gamme de couleurs que l’appareil est capable d’afficher, de reproduire ou d’imprimer.




Il est inclus dans le diagramme de chromaticité et occupe moins de surface que lui (sauf pur le gamut ProPhoto de Kodak).
Comparer des gamuts permet de choisir des appareils permettant de reproduire fidèlement des couleurs.

<figure class="moyen">
    <img  src="../images/imageCours/gamut.png"  frameborder="0"/>
    <figcaption>Espaces couleurs - gamut d'un écran "Dell "</figcaption>
</figure>


----
Pour un système d’affichage par synthèse additive, tel qu’un écran d’ordinateur, le triangle formé par les trois points correspondant aux trois couleurs primaires représente le gamut du système.

Seules des couleurs représentées par un point situé à l’intérieur du triangle peuvent être reproduites, à condition que leur luminosité ne dépasse pas un maximum qui dépend de leur distance aux primaires. 

----




**modèle RVB (ou RGB)** le retour !
- notion de variation des sources primaires
- d'encodage d’une couleur
- 


### 4.2 Quel espace de couleurs


<figure class="moyen">
    <img  src="../images/imageCours/gamut.png" title="Diagramme chromacité xyY" frameborder="0"/>
    <figcaption>Comparaison de quelques gamuts fréguent</figcaption>
</figure>


- En rouge : Espace sRGB de HP et Microsoft pour l’affichage par un moniteur(1996).
- En violet : Espace ADOBE RVB (1998) crée pour les graphistes dont le travail se destine à l’impression.
- En Jaune : Espace CMYK représentant les couleurs imprimables.



### 4.3 Synthèse vidéo


<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/Ps1Rs17cT-o?start=681" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</center>




<!----
### 4.3 Qu’est-ce qu’un profil icc
----
---------

Nous avons vu que la perception des couleurs est subjective. Notre système visuel analyse à sa façon des fréquences d’ondes lumineuses. Si une personne perçoit deux couleurs comme étant identiques, ça n’implique pas que cela sera le cas pour une autre personne.
Dès lors, pour être en mesure de caractériser le plus précisément possible une couleur, il est nécessaire de définir un référentiel standardisé et de fait indépendant des variations lié au système visuel de chacun.
C’est pourquoi la colorimétrie (discipline d’étude des couleurs) a définit un œil humain standard et de nombreux systèmes de représentations des couleurs, basés sur les définitions d’illuminants standards.


<figure>
    <img class="img_600" src="../images/imageCours/base_colorimétrie.png" frameborder="0"/>
    <figcaption>Les éléments de la colorimétrie</figcaption>
</figure>



L’objectif de la colorimétrie est de relier la mesure physique de la lumière et la perception des couleurs. Plus précisément, on veut constituer un ensemble de nombres qui représente sans ambiguïté chaque couleur, de telle sorte qu’il soit inutile de voir deux lumières définies par ces nombres pour connaître leur relation visuelle, et pour dire, par exemple « A est plus claire » ou, « plus pâle » ou « plus verte » que B.
[wikipédia-colorimétrie](https://fr.wikipedia.org/wiki/Diagramme_de_chromaticit%C3%A9

-----
-----
--->

??? note "Ressources"
    - [Chromaticité Gamut_vidéoyoutube](https://www.youtube.com/watch?v=Ps1Rs17cT-o)
    - [Jiphorn_Un peu de colorimétrie](https://www.youtube.com/watch?v=dOs5lcYriE4)
    - [http://www.brucelindbloom.com/index.html?ColorCalculator.html](http://www.brucelindbloom.com/index.html?ColorCalculator.html)
    - [wiki_illuminant](https://fr.wikipedia.org/wiki/Illuminant)
    - [Profil-couleur.com](http://www.profil-couleur.com/)
    - [Profil-couleur.com](http://sti.mermoz.free.fr/mo/vision1/Profil-couleur/www.profil-couleur.com/lc/000-lumiere-couleur.html)
    - [cours.univ-paris1.fr](https://e-cours.univ-paris1.fr/modules/uved/envcal/html/compositions-colorees/representations-couleur/modeles-ref-cie/modele-xyz.html)
    - [rennes.inria.fr](https://people.rennes.inria.fr/Eric.Marchand/ESIR/ESIR2/BINP/couleur.pdf)
    - [CIE](https://cie.co.at/)
    - [wiki-CIE](https://fr.wikipedia.org/wiki/Commission_internationale_de_l%27%C3%A9clairage)
    - [afe-eclairage.fr](http://www.afe-eclairage.fr/)
    - [les espaces de couleurs](https://www.youtube.com/watch?v=tYUHrWskyZI)
    - [Color Vision 3: Color Map](https://www.youtube.com/watch?v=KDiTxWcD3ZE)
    - [www.colorsystem.com](http://www.colorsystem.com/?page_id=901&lang=fr)
    - [modele-cie-1931.blogspot.com/](https://modele-cie-1931.blogspot.com/)
    - [www.optique-ingenieur.org](http://www.optique-ingenieur.org/fr/cours/OPI_fr_M07_C02/co/Grain_OPI_fr_M07_C02_.html)

