---
title : Exercices
correctionvisible : True

---

# Exercices :


### **Exercice 1 :**

<figure>
    <img class="img_600" src="../images/exercices/diagramme_chrom_ex1.png" title="Diagramme Chromatique" frameborder="0"/>
    <figcaption>Diagramme chrommatique xyY</figcaption>
</figure>


Pour cet exercice, on utilise un illuminant de référence C, de coordonnées trichromatique $x_c=0.31$ et $y_c=0.32$.


!!! faq "Question"
    1. Placer le point blanc de l’illuminant, noté C, sur le diagramme CIE.

Sous cet illuminant, les coordonnées trichromatiques de trois encres d’imprimerie noté L, M et N sont données dans le tableau suivant:
<center>
<table>
    <thead>
        <tr>
            <th>Encre</th>
            <th>x</th>
            <th>y</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>L</td>
            <td>0,437</td>
            <td>0,485</td>
        </tr>
        <tr>
            <td>M</td>
            <td>0,408</td>
            <td>0,222</td>
        </tr>
        <tr>
            <td>N</td>
            <td>0,177</td>
            <td>0,226</td>
        </tr>
    </tbody>
</table>
</center>


!!! faq "Questions"
    1. Reporter ces points sur le diagramme CIE
    2. Identifier les couleurs des encres données
    3. À l’aide du diagramme, trouver la longueur d’onde dominante de L et N et la longueur d’onde dominante Complémentaire de M.
    4. Déterminer leur pureté d'excitation


L’addition de ces encres deux à deux permet d’obtenir des couleurs E, F et G de coordonnées :

<center>
<table>
    <thead>
        <tr>
            <th>Encre</th>
            <th>x</th>
            <th>y</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>E=L+M</td>
            <td>0,568</td>
            <td>0,354</td>
        </tr>
        <tr>
            <td>F=L+N</td>
            <td>0,259</td>
            <td>0,473</td>
        </tr>
        <tr>
            <td>G=M+N</td>
            <td>0,200</td>
            <td>0,160</td>
        </tr>
    </tbody>
</table>
</center>

!!! faq "Questions"
    1. Reporter ces couleurs sur le diagramme CIE
    2. Identifier ces couleurs
    3. Le résultat était-il prévisible ? Expliquer quel type de synthèse couleurs est utilisé.
    4. Déterminer la longueur d’onde dominante et la pureté de chacune de ces trois couleurs
    5. Relier les six points du diagramme : donner le nom de la forme dessinée pour l'imprimante.
    6. Donner sa signification.


<br/>


??? faq correction "Correction"
    1. Po



### **Exercice 2 :** *exercice 18p116 du manuel numérique*

<center>
<div class="grand">
    <figure>
        <img src="../images/exercices/exo18.png" />
        <figcaption>Manuel Numérique Hilbrt p116</figcaption>
    </figure>
</div>
</center>



??? faq correction "Correction"
    1. Les couleurs présentes sur la limite courbe du diagramme, en forme de fer à cheval, appelé *lieu du spectre*, représente l'ensemble des couleurs pures, c'est-à-dire à une onde électromagnétique monochromatique.
    2. Le point de coordonnées **x=1/3=0,33,y=1/3=0,33** représente *le blanc équi-énergétique* utilisé comme référence dans le système CIE XYZ.
    3. Le gamut d'un système d'affichage par synthèse additive, tel qu'un écran d'ordinateur, représente *l'ensemble des couleurs qui peuvent être reproduit*.

<br/>

### **Exercice 3 :** *exercice 19p116 du manuel numérique*

<center>
<div class="grand">
    <figure>
        <img src="../images/exercices/exo19.png" />
        <img src="../images/exercices/exo19_2.png" />
        <figcaption>Manuel Numérique Hilbrt p116</figcaption>
    </figure>
</div>
</center>



??? faq correction "Correction"
    1. Les coordonnées  du point blanc *équi-énergétique* sont **x=1/3=0,33,y=1/3=0,33**.
    2. 
        <center><img src="../images/correction/exo19c.svg" /></center>
        <br/>

       a. On place le point à l'aide de ces coordonnées x,y sur le diagramme.
       On place le point de l'illuminant, soit ici le blanc équiénergétique.
       Pour déterminer la longueur d'onde dominanet et le facteur de pureté, il faut effectuer un tracer sur le diagramme *(cf diagramme !)*.

       La longueur d'onde dominante du point de coordonnes $x=0,25, y=0,65$ est de $λ=541nm$.

       b. On cherche son facteur de pureté :

       On mesure sur le diagramme, à l'aide de sa règle les distances : DC et DC'.


       $\frac{DC}{DC'}=\frac{3,6cm}{4,6cm}=0,79$


       Le facteur de pureté de cette couleur est de $79$%.
   
    3. 
    On trace sur le diagramme de chromaticité :

      <center><img src="../images/correction/exo19c2.svg" /></center>

    La longueur d'onde dominante du point de coordonnes $x=0,25, y=0,65$ est de $λ=610nm$.

    <br/>

    On cherche son facteur de pureté :

    On mesure sur le diagramme, à l'aide de sa règle les distances : DE et DE'.

    $\frac{DE}{DE'}=\frac{2,21cm}{2,43cm}=0,91$

    Le facteur de pureté de cette couleur est de $91$%.
   


<br/>

### **Exercice 4 :** *exercice 20p116 du manuel numérique*

<center>
<div class="grand">
    <figure>
        <img src="../images/exercices/exo20.png" />
        <img src="../images/exercices/exo20_2.png" />
        <img src="../images/exercices/exo20_3.png" />
        <img src="../images/exercices/exo20_4.png" />
        <figcaption>Manuel Numérique Hilbrt p116</figcaption>
    </figure>
</div>
</center>


??? faq correction "Correction"
    1. Le second système d'affichage est le plus performant car la surface couverte par son gamut est la plus importante, il est donc capable de reproduire davantage de "vrai" couleur.


    2. Pour répondre à la question, il est nécéssaire de placer sur les diagrammes la couleur qui nous intéresse.*(c'est deux diagrammes sont identique, seul le triangle, les gamuts changent !)*
    On ne connaît pas les coordonées x et y, on doit ici utiliser la longueur d'onde dominante et son coefficient de purreté.
    On trace donc le point de l'illuminant (point équinergétique x=0.33,y=0.33,ici noté D),puis la droite qui passe par ce point et par le point λ=580nm sur la courbe du spectre.
    On sait que le facteur de pureté fait est de 0,5. 
    Or la définition du facteur de pureté est:
    
    $\frac{DA}{DA'}=0,5$
    $DA = 0,5 × DA'$

    On mesure la distance DA' sur le document, on calcule DA à l'aide de la relation ci-dessus, et on place le point A sur le diagramme.

    Conclusion: 
    Les deux systèmes d'affichage sont capable de reproduire une couleur dont la longueur d'onde dominante est $580nm$ et le facteur de pureté $0,5$ , car cette couleur appartient bien aux deux gamuts.
    <center><img src="../images/correction/exo20c.svg" /></center>

    3.  
        a. Les autres couleurs reproductibles sont obtenue par synthèse additive des lumière émise par les trois luminophore, ainsi elles ne peuvent pas être plus pure que les couleurs des luminophore et donc les couleurs des trois luminophore sont les extrémités du gamut.

        b. Les caractéristiques des luminophores du premier système sont :

        - rouge avec une longueur d'ondes majoritaire $λ_{rouge}=610nm$ et un facteur de pureté : $95$%.

        $\frac{DR}{DR'}=\frac{2,29 cm}{2,40 cm}=0,95$

        - vert avec une longueur d'ondes majoritaire $λ_{vert}=554nm$ et un facteur de pureté : $94$%.

        $\frac{DV}{DV'}=\frac{10,1 cm}{10,8 cm}=0,94$

        - bleu avec une longueur d'ondes majoritaire $λ_{bleu}=460nm$ et un facteur de pureté : $88$%.

        $\frac{DB}{DB'}=\frac{1,62cm}{1,85cm}=0,88$

        <center><img src="../images/correction/exo20c1.svg" /></center>

        Les caractéristiques des luminophores du second système sont :

        - rouge avec une longueur d'ondes majoritaire $λ_{rouge}=620nm$ et un facteur de pureté : $100$% *(car sur le lieu du spectre*).
        - vert avec une longueur d'ondes majoritaire $λ_{vert}=542nm$ et un facteur de pureté : $92$%.

        $\frac{DV}{DV'}=\frac{3,4 cm}{3,7 cm}=0,92$

        - bleu avec une longueur d'ondes majoritaire $λ_{bleu}=465nm$ et un facteur de pureté : $100$% *(car sur le lieu du spectre*).


        <center><img src="../images/correction/exo20c2.svg" /></center>

<br/>


### **Exercice 5 :** *exercice 21p117 du manuel numérique*
<center>
<div class="grand">
    <figure>
        <img src="../images/exercices/exo21.png" />
        <img src="../images/exercices/exo21_2.png" />
        <figcaption>Manuel Numérique Hilbrt p117</figcaption>
    </figure>
</div>
</center>




??? faq correction "Correction"
    1. 
    <center><img src="../images/correction/exo21c.svg" /></center>

    2. La couleur est rouge et ses coordonnées sont : $x=0,60,y=0,31$.


<br/>


### **Exercice 6 :** *exercice 22p117 du manuel numérique*

<center>
<div class="grand">
    <figure>
        <img src="../images/exercices/exo22_1.png" />
        <img src="../images/exercices/exo22_2.png" />
        <figcaption>Manuel Numérique Hilbrt p116</figcaption>
    </figure>
</div>
</center>



??? faq correction "Correction"
    1.  
    2. On constate que le gamut tracé à la question 1, ne couvre pas la totalité des couleurs visibles par un œil humain moyen (zone gris foncé encore apparente), il ne sera donc pas possible de reproduire toutes les couleurs visibles avec ce gamut.
    <center><img src="../images/correction/exo22c.svg" /></center>          
    
    3. Les extrémités du gamut sont situé sur la courbe *le lieu du spectre* est sont donc des lumières de couleur pure.
    4. Les couleurs des luminophores du gamut optimal sont:
          - rouge avec une longueur d'ondes majoritaire $λ_{rouge}=700nm$ et un facteur de pureté : $100$% *(car sur le lieu du spectre*).
          - vert avec une longueur d'ondes majoritaire $λ_{vert}=518nm$ et un facteur de pureté : $100$% *(car sur le lieu du spectre*).
          - bleu avec une longueur d'ondes majoritaire $λ_{bleu}=460nm$ et un facteur de pureté : $100$% *(car sur le lieu du spectre*).




<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  
