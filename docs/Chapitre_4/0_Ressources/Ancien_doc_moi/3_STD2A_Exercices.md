---
title : Exercices
correctionvisible : false
pdf : "exercices"
---


<div class="noprint" style="text-align:right;width:100%;background-color:white">
    <a href="../pdf/{{page.meta.pdf}}.pdf" 
    style="display:inline-block" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="#a3a3a3" stroke-width="1.5" stroke-linecap="butt" stroke-linejoin="arcs"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg>
    </a>
</div>



# Exercices


<figure>
  <img src="../images/atelier_Marcos_Rodrigo.png" width="1000" />
  <figcaption>Atelier de Marco Rodrigo</figcaption>
</figure>




!!! faq "Consigne :"
    Observer la photo de l’atelier de Marco Rodrigo ci-dessus et identifiez un maximum de matériaux différents. Classez-les par famille.


!!! note "Matériaux organiques"


!!! note "Matériaux métalliques"


!!! note "Matériaux minéraux"


!!! note "Matériaux composites"





<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  

