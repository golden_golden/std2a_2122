---
title : "Activité 2"
correctionvisible : true
pdf : ""
---


<div class="noprint" style="text-align:right;width:100%;background-color:white">
    <a href="../pdf/{{page.meta.pdf}}.pdf" 
    style="display:inline-block" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="#a3a3a3" stroke-width="1.5" stroke-linecap="butt" stroke-linejoin="arcs"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg>
    </a>
    <button id="bt_test">
        <img src="../../assets_extra/images/cravateseule.svg" width="30"/>
    </button>
</div>

<div id="masque"  style="display:none;text-align:center;font-size: 25px;font-weight: bold;">
    <a href="../pages_cache/enavance/">Page cachée</a>
</div>

<script>
    let bt1 = document.getElementById("bt_test");
    let d2 = document.getElementById("masque");


    function identification(){
        if (d2.style.display!= "block"){
            var reponse = prompt("Es-tu réellement un porteur de cravate ?","<saisir un identifiant>");
            if(reponse==45){
                d2.style.display = "block";
            }
        }
    };

    bt1.onclick = identification;
</script>






# Activité 2 : Propriétés physiques de quelques matériaux


## Partie 1 : Compléter le tableau à l’aide de la liste suivante

**Liste des caractéristiques :**<br> *Conductivité électriques, masse volumique, aptitude face à l’usinage, le recyclage, aptitude au formage, résistance aux chocs, l’oxydabilités, conductivité thermique…*

<center>
<table >
    <tr>
        <td>........................................................</td>
        <td>Le matériau est-il solide ou fragile ?</td>
    </tr>
    <tr>
        <td></td>
        <td>Le matériau est-il facilement usinable ?</td>
    </tr>
    <tr>
        <td></td>
        <td>Le matériau peut-il être valorisé en fin de vie ?</td>
    </tr>
    <tr>
        <td></td>
        <td>Le matériau conduit-il le courant électrique ?</td>
    </tr>
    <tr>
        <td></td>
        <td>Le matériau est-il lourd ou léger ?</td>
    </tr>
    <tr>
        <td></td>
        <td>Le matériau rouille ou résiste à la corrosion ?</td>
    </tr>
    <tr>
        <td></td>
        <td>Le matériau conduit-il la chaleur ?</td>
    </tr>
    <tr>
        <td></td>
        <td>Le matériau peut-il être déformé facilement ?</td>
    </tr>
</table>
</center>




## Partie 2 : Proposer un protocole d’expérimentation permettant de vérifier les propriétés suivantes

<center>
<table >
    <head>
        <tr>
            <td><center><strong> Définition</strong></center></td>
            <td><center><strong> Protocole</strong></center></td>
         </tr>
    </head>
    <tr>
        <td><strong> Masse volumique</strong><br> Des pièces de même volume n’ont pas forcément la même masse.</td>
        <td></td>
    </tr>
    <tr>
        <td><strong> Elasticité</strong><br> Les matériaux reprennent plus ou moins leur forme initiale après avoir été soumis à une force .</td>
        <td></td>
    </tr>
    <tr>
        <td><strong> Résistance aux chocs</strong><br> Les matériaux absorbent les impacts de façon différente.</td>
        <td></td>
    </tr>
    <tr>
        <td><strong> Conductivité électrique</strong><br> Certains matériaux laissent passer un courant électrique tandis que d'autres sont isolants.</td>
        <td></td>
    </tr>
    <tr>
        <td><strong> Conductivité thermique</strong><br>Les matériaux sont plus ou moins conducteurs de chaleur.</td>
        <td></td>
    </tr>
    <tr>
        <td><strong> Absorption acoustique</strong><br>Les matériaux sont plus ou moins des isolants sonores.</td>
        <td></td>
    </tr>
    <tr>
        <td><strong> Propriétés magnétiques</strong><br>Matériaux pouvant être aimantés</td>
        <td></td>
    </tr>
    <tr>
        <td><strong> Aptitude au formage</strong><br>Propriétés de matériaux à conserver durablement une déformation.</td>
        <td></td>
    </tr>
    <tr>
        <td><strong> Température de fusion</strong><br>Température lors du changement d'état solide-liquide</td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td>.............................................................................................</td>
    </tr>
</table>
</center>
