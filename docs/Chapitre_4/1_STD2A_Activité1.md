---
title : "Activité 1"
correctionvisible : true
pdf : ""
---


<div class="noprint" style="text-align:right;width:100%;background-color:white">
    <a href="../pdf/{{page.meta.pdf}}.pdf" 
    style="display:inline-block" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="#a3a3a3" stroke-width="1.5" stroke-linecap="butt" stroke-linejoin="arcs"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg>
    </a>
    <button id="bt_test">
        <img src="../../assets_extra/images/cravateseule.svg" width="30"/>
    </button>
</div>

<div id="masque"  style="display:none;text-align:center;font-size: 25px;font-weight: bold;">
    <a href="../pages_cache/enavance/">Page cachée</a>
</div>

<script>
    let bt1 = document.getElementById("bt_test");
    let d2 = document.getElementById("masque");


    function identification(){
        if (d2.style.display!= "block"){
            var reponse = prompt("Es-tu réellement un porteur de cravate ?","<saisir un identifiant>");
            if(reponse==45){
                d2.style.display = "block";
            }
        }
    };

    bt1.onclick = identification;
</script>






# Activité 1 : De la matière au matériau

??? info "Compétences travaillées"



### **Document 1** : Extrait du dossier de presse de l’artiste Daniel Pujola








**Une œuvre enracinée dans un terroir**

<img src="../images/activite1/Sardane_2007.Jpg" alt="Sardane 2007" width="200" align="right" />

Situé au cœur de la Dordogne, l’atelier de Daniel Pujola concentre en son sein la richesse d’un patrimoine séculaire. Ses tableaux sont le reflet de cette terre dont il est issu et ils en deviennent eux-mêmes la matière puisque les matériaux utilisés par le peintre lui sont directement empruntés. Ainsi, chaque œuvre est une véritable mise en scène où la peinture flirte avec des objets aussi insolites qu’un morceau de bois, des éclats de porcelaine, des ressorts, des bouchons de
pêche tannés par la mer ; elle représente par là-même une véritable métaphore du travail de l’Homme, de son action sur les produits qu’il a façonnés ou plus simplement une lutte contre la détérioration de la nature.

Des objets délaissés, écartés vont ainsi être sublimés à travers l’art pour atteindre un deuxième niveau de signification qui sera leur existence même. « Je sais ce que je dois ramasser » dit D.Pujola. L’œuvre d’art devient une continuité de ce travail extraordinaire qu’effectue la nature. L’artiste ramasse, pose, peint afin d’aboutir à l’harmonie la plus totale.

<img src="../images/activite1/Ecriture_intime_2005.Jpg" width="200" align="left" />

L’œil non averti pensera qu’ils sont disséminés çà et là. Pourtant le choix des matériaux ainsi que leur place dans une œuvre s’avèrent être le résultat d’un choix peut-être, d’une obligation à coup sûr, démarche exigée par l’œuvre elle-même. Ainsi, la présence du sable nous transporte non seulement vers l’Afrique du Nord ou l’Espagne, comme une évocation des origines hispaniques de l’artiste, mais nous ramène aussi plus simplement à ses racines locales, dans les carrières du Périgord.

Quant à la nature dans laquelle vit le peintre, on peut sentir sa présence constante à travers l’utilisation d’éléments qui en sont directement extraits comme le bois ou le lichen qui est remodelé puis repeint. Ajoutons également le fer qui revêt une double connotation puisqu’il nous rappelle la proximité de l’Océan mais aussi l’importance de la religion que l’on retrouve à travers des objets recueillis sur les lieux de culte.

<br>

### **Document 2** : Extrait d’une interview
[…] Quelles sont vos techniques ?

\- L’acrylique sur toile ou papier où se mêle la matière, pigments naturels, terre, écorces, pages de vieux livres, bandes de plâtre, métal.… complétés par des éclaboussures, des graffitis qui traduisent l’émotion brute du moment, l’instantanéité, le caractère intuitif…

<br>

### **Document 3** : Vidéo “Comment ça marche”



<center><iframe width="710" height="400" src="https://www.youtube-nocookie.com/embed/VevQyq3kzGQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[Vidéo du CEA](https://www.youtube.com/watch?v=VevQyq3kzGQ){:target="_blank"}
</center>

<br>


### **Questions** :

!!! faq "Documents 1 et 2"

    1. Lister les différents matériaux utilisés par le peintre.
 

!!! faq "Document 3"

    1. Quelle différence faire entre “matière” et “matériau” ?
    2. Citer et définir les propriétés physiques susceptibles de caractériser un matériau.
    3. Identifier les grandes classes de matériaux et s’en servir pour catégoriser ceux utilisés par D. Pujola.
    4. Selon vous, quels critères sont à prendre en compte lors du choix d’un matériau ?

