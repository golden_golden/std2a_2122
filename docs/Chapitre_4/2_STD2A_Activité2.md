---
title : "Activité 2"
correctionvisible : true
pdf : ""
---


<div class="noprint" style="text-align:right;width:100%;background-color:white">
    <a href="../pdf/{{page.meta.pdf}}.pdf" 
    style="display:inline-block" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="#a3a3a3" stroke-width="1.5" stroke-linecap="butt" stroke-linejoin="arcs"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg>
    </a>
    <button id="bt_test">
        <img src="../../assets_extra/images/cravateseule.svg" width="30"/>
    </button>
</div>

<div id="masque"  style="display:none;text-align:center;font-size: 25px;font-weight: bold;">
    <a href="../pages_cache/enavance/">Page cachée</a>
</div>

<script>
    let bt1 = document.getElementById("bt_test");
    let d2 = document.getElementById("masque");


    function identification(){
        if (d2.style.display!= "block"){
            var reponse = prompt("Es-tu réellement un porteur de cravate ?","<saisir un identifiant>");
            if(reponse==45){
                d2.style.display = "block";
            }
        }
    };

    bt1.onclick = identification;
</script>






# Activité 2 : Les matériaux composites

Activité 1 du manuel numérique

<center>
<img src="../images/activite2/doc1.png" alt="doc1" />
</center>

<center>
<img src="../images/activite2/doc1b.png" alt="doc1b" />
</center>

<center>
<img src="../images/activite2/doc2.png" alt="doc2" />
</center>

<center>
<img src="../images/activite2/doc3.png" alt="doc3" />
</center>

<center>
<img src="../images/activite2/doc4.png" alt="doc4" />
</center>

<center>
<img src="../images/activite2/questions.png" alt="question" />
</center>






 


