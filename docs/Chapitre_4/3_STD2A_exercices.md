---
title : Exercices
correctionvisible : false
pdf : "Sujetdevoirmaison"
---


<div class="noprint" style="text-align:right;width:100%;background-color:white">
    <a href="../pdf/{{page.meta.pdf}}.pdf" 
    style="display:inline-block" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="#a3a3a3" stroke-width="1.5" stroke-linecap="butt" stroke-linejoin="arcs"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg>
    </a>
</div>



# Exercices

Exercices du manuel numérique

## Partie 1 : Familles de matériaux

**Exercice 1**<br>
<center>
<img src="../images/exercices/exo1.png" alt="exo1" />
</center>

**Exercice 2**<br>
<center>
<img src="../images/exercices/exo2.png" alt="exo2" />
</center>

**Exercice 4**<br>
<center>
<img src="../images/exercices/exo4.png" alt="exo4" />
</center>

<center>
<img src="../images/exercices/exo4b.png" alt="exo4" />
</center>

**Exercice 5**<br>
<center>
<img src="../images/exercices/exo5.png" alt="exo5" />
</center>

**Exercice 6**<br>
<center>
<img src="../images/exercices/exo6.png" alt="exo6" />
</center>

**Exercice 7**<br>
<center>
<img src="../images/exercices/exo7.png" alt="exo7" />
</center>

**Exercice 8**<br>
<center>
<img src="../images/exercices/exo8.png" alt="exo8" />
</center>

**Exercice 9**<br>
<center>
<img src="../images/exercices/exo9b.png" alt="exo9" />
</center>

<center>
<img src="../images/exercices/exo9b2.png" alt="exo9" />
</center>


## Partie 2 : Propriétés des matériaux

**Exercice 10**<br>
<center>
<img src="../images/exercices/exo10.png" alt="exo10" />
</center>

**Exercice 11**<br>
<center>
<img src="../images/exercices/exo11b.png" alt="exo11" />
</center>

<center>
<img src="../images/exercices/exo11b2.png" alt="exo11" />
</center>

**Exercice 12**<br>
<center>
<img src="../images/exercices/exo12.png" alt="exo12" />
</center>

<center>
<img src="../images/exercices/exo12b.png" alt="exo12" />
</center>

**Exercice 13**<br>
<center>
<img src="../images/exercices/exo13.png" alt="exo13" />
</center>

**Exercice 14**<br>
<center>
<img src="../images/exercices/exo14.png" alt="exo14" />
</center>

<center>
<img src="../images/exercices/exo14b.png" alt="exo14" />
</center>



<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  

