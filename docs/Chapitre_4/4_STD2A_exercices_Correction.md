---
title : Exercice Correction
---

# Correction des exercices :

## Familles de matériaux
### **Exercice 1 :**

1. Les trois grandes familles de matériaux sont :
   
      - *matériau organique.*
      - *matériau métaux.*
      - *matériau minéraux.*
  

2. En incorporant à un métal un ou plusieurs autres métaux, où des éléments non métalliques, on obtient un alliage.
3. Le bronze est un alliage, mélange de cuivre et d’étain.
4. Les matériaux organiques sont principalement constitués de carbone et d’hydrogène.
5. Le verre appartient à la famille des minéraux.


### **Exercice 2 :**

- matériau organique : *bois, nylon, charbon, pvc, silicone.*
- matériau métaux : *cuivre, bronze, aluminium, acier.*
- matériau minéraux : *verre, céramique.*


### **Exercice 3 :**
1. En incorporant à un métal un ou plusieurs autres métaux, où des éléments non métalliques, on obtient un alliage.
2. L’acier inoxydable, est un alliage, car il est constitué de fer, de carbone *(- de 1,2 %)* et de chrome *(au moins 10,5 % de chrome)*.
3. L’acier inoxydable appartient à la famille des métaux.


### **Exercice 4 :**
1. Les matériaux utilisés pour "The looking Glass", sont :

      - le verre
      - l'acier inoxydable
      - du silicone
  

2. Le verre appartient à la famille des minéraux, l’acier inoxydable appartient à la famille des métaux et le silicone à la famille des matériaux organiques.


## Matériaux innovants
### **Exercice 5 :**
1. Un matériau composite, est un matériau constitué d’au moins deux matériaux non miscibles mais ayant une forte capacité d’adhésion. Le nouveau matériau à des propriétés mécaniques que les matériaux le constituant n’ont pas.
2. Le renfort, est le squelette du matériau sont rôles est d’assurer sa solidité.
3. La matrice, est l’enveloppe autour du renfort. Elle assure la cohésion et la forme du matériau tout en protégeant le renfort, en répartissant le plus uniformément possible les contraintes mécaniques et en l’isolant des attaques du milieu extérieur.
4. Les nanomatériaux sont des matériaux dont l’une au moins des dimensions est comprise entre 1 et 100 nanomètres (10^{-9} mètre). Le passage de la matière à des dimensions nanométriques fait apparaître des propriétés inattendues et souvent totalement différentes de celles des mêmes matériaux à l’échelle micro ou macroscopique.


### **Exercice 6 :**
1.  La fibre de verre appartient à la famille des céramiques et le plastique appartient à la famille des composés organiques.
2.  Dans cette chaise :

    - Le renfort est la fibre de verre.
    - La matrice est le plastique.


### **Exercice 7 :**
1. Les matériaux utilisés pour la structure de cette école, sont :
   
      - la fibre de verre.
      - résine polymère.
      - acier.
      - bois.

2. L'acier appartient aux métaux, la fibre de verre aux céramiques, le bois et la résine polymère aux organiques.


### **Exercice 8 :**
1. Les matériaux utilisés pour la structure de *l'intechange Pavillon* sont : 
    -   l’acier inoxydable
    -   le béton
    -   la fibre de verre
    -   le bois.

2. Familles des matériaux utilisé :
      - matériau organique : *bois*
      - matériau métaux : *acier inoxydable.*
      - matériau céramique : *béton, fibre de verre.*

3. L’acier inoxydable, est un alliage, car il est constitué de fer, de carbone *(- de 1,2 %)* et de chrome *(au moins 10,5 % de chrome)*.
4. Dans ce béton armé, la matrice est le béton.
5. Dans ce béton armé le renfort est la fibre de verre.


### **Exercice 9 :**
1. Les nanomatériaux sont des matériaux dont l’une au moins des dimensions est comprise entre 1 et 100 nanomètres (10^{-9} mètre). Le passage de la matière à des dimensions nanométriques fait apparaître des propriétés inattendues et souvent totalement différentes de celles des mêmes matériaux à l’échelle micro ou macroscopique.
2. Dans l'Oxylink, les nanomatériaux sont des oxydes métalliques.
3. Dans la peinture Bühler, les nanomatériaux permettent *" une meilleure résistance à l’arrachage, une plus grande résistance à l’humidité tout en permettant à la peinture de sécher plus rapidement et de repousser d’avantage les salissures."*


## Propriétés des matériaux
### **Exercice 10 :**
1. Les trois principaux critères rentrant dans le choix d’un matériau sont :
   
      -  Les propriétés physico-chimiques
      -  Les propriétés liées à process de fabrications
      -  Le coût
      -  L'impact sur l’environnement

2. Un matériau biodégradable est un matériau qui peut se dégrader, dans un temps court au regard du temps humain.(en eau, dioxyde de carbone, méthane et biomasse sous l’action de micro-organismes (bactéries, champignons) réduisant ainsi l’impact de ces derniers sur l’environnement.)
3. Les matériaux biosourcés sont issus de la matière organique renouvelable (biomasse), d’origine végétale ou animale : bois, chanvre, paille, ouate de cellulose, textiles recyclés, etc. Leur impact sur l’environnement est limité lorsque l’on considère leur empreinte carbone ou leur cycle de vie.


### **Exercice 11 :**
1. Les familles des matériaux envisagés sont :
      - matériau organique : *polycarbonate*
      - matériau métaux : *acier*
      - matériau minéraux : *verre.*


2. .
    <figure class="grand">
        <img src="../images/exercices/exo11v2.png" title="schéma de l’emballage imaginé" frameborder="0"/>
        <figcaption>Schéma de l’emballage imaginé</figcaption>
    </figure>


1. On cherche le volume de l’emballage, noté V:

   Soit V_1 le volume extérieur de l’emballage et V_2 le volume de l'évidement intérieur de l’emballage, on a alors :
   V=V_1-V_2


   Or V_1= π × R_1² × h_1 et V_2= π × R_2² × h_2


   Or a alors, par substitution :
   V=π × R_1² × h_1-π × R_2² × h_2
  

   **A.N:**

   V=π × (3,0 cm)² × 10cm -π ×(2,0cm)² × 8,0 cm
   V=182 cm^3
   
   Le volume de l’emballage est de V=182.10^{-6} m^3.

    

2. On cherche la masse de l’emballage, noté m :
   
   ρ=\frac{m}{V}
   
   donc 
    m =ρ×V

**A.N:**

Pour l’acier :

m_{acier} = 7 500 kg.m^{-3} × 182.10^{-6} m³

m_{acier} = 1,37 kg

  

Pour le Verre :

m_{verre} = 2 500 kg.m^{-3} × 182.10^{-6} m³

m_{verre} = 0,455 kg

  

Pour le polycarbonate :

m_{poly} = 1 200 kg.m^{-3} × 182.10^{-6} m³

m_{poly} = 0,218 kg



5. Les caractéristiques physico-chimiques des matériaux proposés et le coût, nous oriente vers l’utilisation du verre.
Au niveau environnemental, le verre est également un bon candidat bien qu’il soit deux fois plus lourd et donc que l’énergie nécessaire pour les transports soit supérieure à celle utilisée pour du polycarbonate, le verre est un matériau qui est à 100 % recyclable.



### **Exercice 12 :**
Les avantages :
-  La masse volumique beaucoup plus faibles.
-  Résistance aux rayures et aux agents chimiques.

Les inconvénients sont :
- Le coût
- La faible résistance aux chocs.


### **Exercice 13 :**
On cherche le volume de l’œuvre, noté V:

   Soit V_1 le volume du pavé et V_2 le volume de la boule, on a alors :
   V=V_1+V_2


   Or V_1= l × L × h et V_2= \frac{4}{3}×π × R³


   Or a alors, par substitution :
   V= l × L × h + \frac{4}{3}×π × R³
  

   **A.N:**

   V=25 cm × 10cm × 7cm + \frac{4}{3}×π × 7³ cm
   V= 3 187cm^3
   
   Le volume de l'œuvre est de V=3,2.10^{-3} m^3.

    

1. On cherche la masse de l’œuvre, noté m :
   
   ρ=\frac{m}{V}
   
   donc 
    m =ρ×V

**A.N:**

Pour le cuivre :

m_{cuivre} = 8,96 g.cm^{-3} × 3 187 cm³

m_{cuivre} = 28 556g

m_{cuivre} = 28,556 kg

  

Pour le Verre :

m_{verre} = 2,5 g.cm^{-3} × 3 187 cm³

m_{verre} = 7 968 g

m_{verre} = 7,968 kg



### **Exercice 14 :**
1. La robe conçue par CuteCircuit peut être considérée comme un textile intelligent, car il change de couleur en fonction des émotions du porteur.
2. Les matériaux rentant dans la composition de cette robe sont :
   
      - le polyamide
      - le graphène.
  
3. Le polyamide et le graphène appartiennent à la famille de matériaux organiques.
4. Le graphène est un très bon conducteur électrique.
5. On utilise du graphène, car il est très léger et permet d’élaborer un tissu (pas une armure !).

