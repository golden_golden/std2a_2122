﻿---
title : "Cours"

---


# Chapitre 3 : Les matériaux





??? info "Dans le programme"
    **Thème 1** : Connaître et transformer les matériaux

    | **Notions et contenus**                                                                                         | **Capacités exigibles**                                                                                 |
    | --------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------- |
    | Familles de matériaux.                                                                                          | - Distinguer les grandes classes de matériaux utilisés dans lesdomaines du design et des métiers d’art. |
    | Propriétés physiques des matériaux : masse volumique, élasticité, conductivité thermique,absorption acoustique. | - Comparer expérimentalement des *caractéristiques physiques de différents matériaux*.                  |
 

??? info "Compétences travaillées"
    




<br>

## 1. Familles de matériaux

!!! note "matière"
	Le mot matière s’emploie pour parler d’une substance (matière précieuse, matières grasses).Une matière première est une substance d'origine naturelle qui doit être transformée afin d'être utilisée dans la fabrication d'un objet technique.

!!! note "matériaux"
	Un matériau est une substance qui provient de la transformation d'une matière première et qui est prête à entrer dans la fabrication d'un objet technique.

Les matériaux sont généralement répartis en 4 classes (ou familles) différentes :

- Les matériaux organiques
- Les matériaux métalliques
- Les matériaux minéraux
- Les matériaux innovants


### 1.1 Les matériaux organiques

Ce sont des matériaux d’origine naturelle (animale ou végétale) ou synthétique. Tous ces matériaux ont un point commun : ils sont essentiellement constitués d’atomes de carbone.

<br>

#### 1.1.1 Matériau organiques naturels

##### a. Le bois

Ses caractéristiques varient en fonction de l’espèce d’arbre dont il est issu. Il peut être :

- très tendre : *peuplier, bouleau…*
- très dur : *chêne, acacia…*

Il sont facile à usiner, en revanche, il est difficile à façonner par déformation.

<br>

##### b. les dérivés du bois

**Les agglomérés** : IIs sont constitués de copeaux de bois collés à chaud sous presse.

**Les lamellés** : Ils sont constitués de couches de bois en lamelles collés à chaud sous presse.

**Les cartons et papiers** : Ils sont constitués de fibres de bois et de cellulose. Ils sont essentiellement utilisés dans l’emballage.

<br>

##### c. Les fibres naturelles

Elles sont utilisées pour tisser des toiles et des tissus.
La laine est fournie par les moutons, lamas, chèvres… La soie est extraite des cocons du ver à soie. Le coton provient des gousses des graines de coton et le lin des tiges d’une plante du même nom.

<br>

##### d. Le cuir

C’est de la peau animale tannée (généralement de la peau de grands mammifères tels le À bœuf et le porc), c’est-à-dire une substance morte, imputrescible, souple et insoluble dans l’eau. On en fait des accessoires de maroquinerie, vêtements, chaussures.…

<br>


#### 1.1.2 Matériaux organiques synthétiques : les matières plastiques

Ces matières ont été créées en transformant, par des réactions chimiques, des produits naturels tels que le charbon, le pétrole ou le bois. Certaines peuvent remplacer l’acier eu égard à leurs caractéristiques.

<br>

##### a. Les thermoplastiques

Parfois appelés “thermoformables”, ils ont des propriétés très diverses. Leur point commun est qu’ils se moulent facilement et peuvent être refondus et réutilisés. IIs se déforment sous l’effet de la chaleur.

<br>

##### b. Les thermodurcissables

Ils sont moulés une fois pour toutes. IIs sont durs et rigides. IIs ne peuvent plus être déformés.

<br>


##### c. Les caoutchoucs et les élastomères

Le caoutchouc *(littéralement « arbre qui pleure »)* est obtenu à partir d latex qui est la sève blanche sécrétée par un arbre :l’hévéa. C’est donc un plastique à base "naturelle”. C’est un élastomère : il a la propriété de reprendre sa forme après avoir été déformé. On en fabrique des élastiques, les chambres à air de véhicules… Aujourd’hui on fabrique des élastomères synthétiques aux propriétés très proches de celle du caoutchouc, pour un coup moindre.

<br>

### 1.2 Les matériaux métalliques

Les matériaux métalliques peuvent être :

- Des métaux purs, c’est-à-dire composés d’un seul éléments chimique, comme le fer.
- Des alliages. En incorporant à un métal un ou plusieurs autres métaux, où des éléments non métalliques, on forme des alliages, comme : le bronze, le laiton, les aciers…

Les métaux sont des éléments naturels, on les trouve dans le sol, le plus souvent sous forme de minerai (roche riche en métal), parfois sous forme pure (pépite).

<br>

#### 1.2.1 Les alliages : fer, carbone

##### a. L’acier
Il est obtenu en incorporant moins de 2 % de carbone dans du fer. Il peut être allié à de nombreux autres métaux comme le chrome et/ou le nickel, qui le transforment en acier inoxydable.

*Exemple : la visserie, les boite de conserve…*

<br>

##### b. La fonte
Elle est obtenue en incorporant plus de 2 % de carbone dans le fer et est par conséquent plus lourde que l’acier. En fusion, elle se moule facilement.

*Exemple : le mobilier urbain comme les fontaines ou les bancs, les radiateurs…*

<br>


#### 1.2.2 L’aluminium et ses alliages

##### a. L’aluminium

L’aluminium est un métal naturel de couleur grise blanchâtre. Il présente l’avantage de résister à la corrosion et d’être très léger, il se moule facilement et se travaille sans difficulté.


*Exemple : la feuille de “papier alu”, les canettes de soda…*

<br>

##### b. Le Zamak *(alliage aluminium-zinc)* :
Très utilisé dans la fabrication de pièces de décoration où pour le modélisme.

<br>

##### c. Le Duralumin** *(alliage aluminium, cuivre et magnésium)* :
Il est très rigide (d’où son nom). Il est utilisé notamment pour fabriquer des pièces d’horlogerie.

<br>

#### 1.2.3 Le cuivre et ses alliages

##### a. Le cuivre

Métal de couleur rouge, qui conduit particulièrement bien la chaleur. Très utilisé, notamment pour les circuits électriques ou la cuisine *(casseroles)*.

<br>

##### b. Le laiton *(alliage cuivre-zinc)*
Il se moule très facilement et donne des pièces très précises, comme les robinets ou les instruments de musiques.

<br>

##### c. Le bronze *(alliage cuivre-étain)*
Initialement de couleur jaune, il se patine avec le temps et est utilisé pour la sculpture où la fabrication des cloches.<br>

Pour les plus curieux, consulter le lien suivant traitant du cuivre et de ses alliages : [*ici*](https://www.futura-sciences.com/planete/dossiers/geologie-cuivre-premier-metal-travaille-homme-779/)

<br> 

#### 1.2.4 Le zinc, alliages et protections
##### a. Le zinc
Sa principale utilisation réside dans la protection du fer et des aciers contre la corrosion. Il est utilisé notamment pour les revêtements des gouttières métalliques ou les paraboles.

<br>

##### b. Les alliages avec le zinc
Le zinc rentre dans la composition de quelques alliages que nous avons vue précédemment, avec l’aluminium pour former le zamak, et avec le cuivre pour former le laiton.

<br>

##### c. Protection contre la corrosion
Le dépôt d’une mince couche de zinc en surface de l’acier ou du fer le protège de la corrosion : c’est le zingage, ou la galvanisation.

<br>

#### 1.2.5 L’étain et ses alliages
##### a. L’étain
Il est utilisé principalement pour le revêtement de tôles fines d’acier, pour l’étamage (traitement de surface qui consiste à appliquer une couche d’étain sur une pièce métallique). C’est un des constituants des boîtes de conserve, fabriquées en «fer blanc » (acier + couche d’étain).

<br>

##### b. Les alliages avec l’étain
L’étain rentre dans la composition de quelques alliages, dont le principal est celui formé avec le cuivre, le bronze.

<br>

### 1.3 Les matériaux minéraux
Ils ne contiennent pas d’atomes de carbone. Ce sont des roches, des céramiques où des verres.

<br>

#### 1.3.1 Les céramiques
À l’origine fabriquées à partir d’argile (poteries), ce sont aussi des matériaux obtenus à partir de terre où de sable cuit. Elles comprennent la faïence (carrelage, vaisselle, le grès (vaisselle) et la porcelaine (isolant, vaisselle).

Les céramiques sont dures, résistantes à la chaleur mais fragiles. Aujourd’hui les céramiques techniques ont des qualités exceptionnelles et entrent dans la composition d’un grand nombre d’objets (tables de cuisson, réacteurs d’avions, revêtements spéciaux…).

<br>

#### 1.3.2 Les verres
Les verres sont des matériaux très anciens élaborés à partir de sable. Obtenus par fusion de la silice, on peut leur ajouter des additifs afin de modifier leurs caractéristiques (exemple : l’ajout de plomb permet d’obtenir le cristal) et modifier aussi leurs couleurs (par ajout d’oxydes métalliques).

Le verre est un matériau très dur mais très fragile. Il est utilisé dans la fabrication de bouteilles et de vitres, mais également de “laine de verre” (par fusion, on obtient alors un matériau isolant de consistance laineuse).

<br>

### 1.4 Les matériaux innovants

#### 1.4.1 Les matériaux composites
Par définition, ce sont des matériaux constitués d’au moins deux matériaux non miscibles mais ayant une forte capacité d’adhésion.<br>
Le nouveau matériau à des propriétés mécaniques que les matériaux le constituant n’ont pas, généralement on cherche à améliorer la légèreté, ou la rigidité.<br>
Ils sont le plus souvent couteux à fabriquer, car de haute technologie (exemple : le kevlar, les fibres de carbone). Il existe cependant des composites naturels comme l’os.

<br>
Dans un matériau composite, on distingue :

- Le renfort, squelette du matériau dont le rôle est d’assurer sa solidité ; Ils peuvent être de plusieurs types :
- Des particules, comme des cailloux durs dans le béton, ou des microbilles de verre, de la céramique…
- Des fibres, comme des fibres de carbone, de verre ou de kevlar, alliant légèreté et résistance.
- La matrice, enveloppe autour du renfort, qui assure la cohésion et la forme du matériau tout en protégeant le renfort, en répartissant le plus uniformément possible les contraintes mécaniques et en l'isolant des attaques du milieu extérieur.


*Exemples : Le béton armé est un matériau composite issu de la combinaison du béton et de l’acier.*



#### 1.4.2 D’autres matériaux d’innovants
##### A. Les nanomatériaux
Les nanomatériaux sont des matériaux dont l’une au moins des dimensions est comprise entre 1 et 100 nanomètres ($10^{-9}$ mètre).

*Exemples :
Les nanoparticules, comme les nanoparticules de dioxyde de zinc, de latex, d’alumine, sont des nanomatériaux dont les trois dimensions sont à l’échelle nanométrique.
Les nanotubes de carbone, dont deux dimensions sont à l’échelle nanométrique.
Les nano-feuilles, comme le graphène, dont une seule dimension est à l’échelle nanométrique.*

Le passage de la matière à des dimensions nanométriques fait apparaître des propriétés inattendues et souvent totalement différentes de celles des mêmes matériaux à l’échelle micro ou macroscopique, notamment en termes de résistance mécanique, de réactivité chimique, de conductivité électrique et de fluorescence. Par exemple, l’or est totalement inactif à l’échelle micrométrique alors qu’il devient un excellent catalyseur de réactions chimiques lorsqu’il prend des dimensions nanométriques.

##### B. Les matériaux à changements de phases

##### C. Beaucoup d’autres matériaux innovants
Les cristaux liquides, les agro-matériaux ou les textiles intelligents sont le plus en plus utilisés…

<br>

## 2. Propriétés physiques des matériaux
Il existe une multitude de propriétés physiques caractéristiques (ou non) des matériaux. L’objectif du chapitre n’est pas d’en dresser une liste interminable, mais de savoir quelles propriétés peuvent orienter le choix vers un matériau plutôt qu’un autre.

<br>


### 2.1 Propriétés chimiques
potentiel hydrogène (pH)
hygroscopie
Réactivité
résistance à la corrosion
…

### 2.2 Propriétés électriques
Conductivité 
Résistivité
…

### 2.3 Propriétés magnétiques
Aimantation

### 2.4 Propriétés mécaniques
Dureté
Ductibilité
Élasticité et Limite d'élasticité
Fragilité
Fatigue 

Limite d'endurance
Résisatance à l'abrasion
Résistance à l'enfoncement
Résistance au cisaillement 
Résistance à la torsion
Résistance au flambage
Résistance à la déchirure
Résistance pyroscopique
…
Rugosité
Viscosité


### 2.5 Propriétés optiques
Couleur
Luminosité
indice de réfraction
Transimittance 
Photosensibilité
…

### 2.6 Propriétés physiques
Flottabilité
Gélivité
Masse volumique
…

### 2.7 Propriétés thermiques
Coefficient ed dilatation
Capacité thermique, capacité thermique massique et capacité thermique volumique
Conductivité thermique
Température de changement d'états
…

### 2.8 Propriétés liée à la fabrication
Coulabilité
Température et pression d'extrusions
Dureté
Taux d'usinabilité






### 2.1 Masse volumique
La masse volumique ρ d’un matériau est une grandeur égale au quotient de sa masse m par son volume V :

<center>$ρ=\frac{m}{V}$</center>


Si la masse du matériau est donnée en $kg$ et celle du volume en $m³$, la densité sera exprimée en $kg.m^{-3}$

<br>

Le volume peut être obtenu par deux méthodes :

- Par le calcul, et la mesure de longueur.

	*Exemple :*

	*Le volume d’un pavé droit de longueur $L$, de largeur $l$ et de hauteur $h$ est donné par :*<br>

	<center>$V=L×l×h$</center>


	*Le volume d’un cylindre de hauteur $h$, et dont la base circulaire a pour rayon $r$ est donné par :*<br>

	<center>$V= πr²×h$</center>


	*Le volume d’une sphère de rayon $r$ est donné par :*<br>

	<center>$V= \frac{4}{3} πr³$</center>


<br>

- Par la méthode du déplacement d’eau.
Cette méthode est adaptée pour les objets immergeables et dont la forme est complexe. Lorsque on immerge un corps dans de l’eau le volume de liquide déplacé est égal au volume occupé par le corps.<br>

	<figure class="moyen">
		<img src="../images/cours/deplacementdeau.png" title="Méthode du déplacement d’eau" frameborder="0"/>
		<figcaption>Détermination d'un volume par la méthode du déplacement d’eau</figcaption>
	</figure>

<br>

### 2.2 Élasticités
En physique, l’élasticité est la propriété d’un matériau solide à retrouver sa forme d’origine après avoir été déformé. La déformation élastique intervient pour les faibles sollicitations.
La contrainte délimitant le domaine élastique des autres domaines est appelée limite d’élasticité.
<br>

### 2.3 Duretés
La dureté d’un matériau est définie comme la résistance mécanique qu’un matériau oppose à la pénétration.

Il existe plusieurs définition et échelle pour la dureté. Parmi ces définitions la plus connue est :

L’échelle de Mohs *(dureté des minéraux)*


<figure class="grand">
	<img src="../images/cours/Moth.png" title="Echelle de Mohs" frameborder="0"/>
	<figcaption>Référence pour l'échelle de Mohs</figcaption>
</figure>


<br>

### 2.4 Compressibilités

<br>

### 2.5 Conductivité/Résistivité électriques

<br>

### 2.6 Conductivités thermiques
La conductivité thermique λ d'un matériau, exprimée en W.K^{-1}.m^{-1}, caractérise sa capacité à conduire de la chaleur : plus sa valeur sera faible, plus le matériau sera isolant.

<br>


### 2.7 Propriétés magnétiques


<br>

### 2.8 Aptitudes au formage

<br>

### 2.9 Températures de fusion

<br>

## 4. Choix raisonné de matériaux
### 4.1 Cahiers des charges
Le choix d’un matériau se fait suivant une multitude de critères, parmi lesquels on trouve :

- Les propriétés physiques et chimiques.
- Les possibilités, contraintes de fabrication.
- Le coût
- Les possibilités d’approvisionnement
  
- Les impacts sur l’environnement
- Critères éthiques et politiques


Le matériau parfait n’existe généralement pas, il est le plus souvent le résultat de compromis.

<br>

### 4.2 Impacts sur l’environnement
Les enjeux environnementaux sont désormais primordiaux dans l’élaboration des cahiers des charges.
L’impact d’un matériau sur l’environnement est souvent difficile à établir, car il est nécessaire de prendre en compte l’entièreté de son cycle de vie :

- Impact de la production des matières premières
- Consommation énergétique pour la production
- Énergie et pollution liée aux transports
- Recyclabilité

<br>

#### 4.2.1 Les matériaux biosourcés
Les matériaux biosourcés sont issus de la matière organique renouvelable (biomasse), d’origine végétale ou animale : bois, chanvre, paille, ouate de cellulose, textiles recyclés, etc. Ils peuvent être utilisés comme matière première dans des produits de construction et de décoration. Leur impact sur l’environnement est limité lorsque l’on considère leur empreinte carbone (absorbent le plus souvent du $CO_2$, nécessitent peu d’intermédiaires dans leur fabrication) ou leur cycle de vie (ce sont la plupart des matériaux recyclables.)

<br>

#### 4.2.2 Les matériaux biodégradables
Les matériaux biodégradables peuvent se dégrader, dans un temps court au regard du temps humain, en eau, dioxyde de carbone, méthane et biomasse sous l’action de micro-organismes (bactéries, champignons) réduisant ainsi l’impact de ces derniers sur l’environnement.

<br>

### 4.3 Critères éthiques et politiques
Les enjeux éthiques et politiques peuvent également avoir une importance non négligeable. Si l’obtention d’un matériau peu se faire en faisant en sorte que les travailleurs qui sont intervenus pour l’obtenir, n'aient pas été exploités et soient rémunérés décemment, c’est mieux !
Les filières courtes peuvent également être privilégiées.

<br>


Ressources :
https://fr.wikipedia.org/wiki/Liste_de_propri%C3%A9t%C3%A9s_d%27un_mat%C3%A9riau
https://www.alloprof.qc.ca/fr/eleves/bv/sciences/les-proprietes-des-materiaux-s1447