# Les métaux 

## 1. Les réaction d'oxydoréduction
<center>
<iframe src="../pdf/Terminale  STI2D -Chapitre 10_Les matériaux.pdf" height="800" width="1200"></iframe>
</center>

## 2. Cours en vidéo

??? remarque "Rappel de seconde sur les réactions chimiques"
    **Cours sur comment équilibrer une équation chimique** 
    <center>
    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/VZVBS4OwwlE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </center>
    **Exemples**
    <center>
    <iframe width="560" height="315" src="https://www.youtube.com/embed/sU5530meyaM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </center>
    <center>
    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/-k7J22UZP-E" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </center>
    <center>
    <iframe width="560" height="315" src="https://www.youtube.com/embed/c1FEk-HY1j0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </center>






### a. Cours et méthodes en détail, idéal pour apprendre !
*Regarder les trois vidéo dans l'ordre.*

<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/bN_3k9YfRxI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>

<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/xHQj1QsuW9k" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>

<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/NHQ7hDRWktM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Remarques: je vous conseille de faire les demi équations en premier et de les écrires les réactifs en premier dans un second temps !*
</center>


### b. Cours en version, résumé idéal pour réviser !
<center>
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/hjjazEdcSVI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>


## 3. Exercices et exemples de résolution en vidéo

***Que devez vous savoir faire ? Et que trouve t'on dans les exercices ?***

Vous devez savoir :

- Définir un oxydant et un réducteur *(c'est du cours)* 
- Identifier dans un couple qui est l'oxydant et qui est le réducteur.
  
- Établir une demi-équation électronique, correspondant à un couple ox/red.
- Établir une équation bilan, lorsque l'on vous donne deux couple ox/red et que l'on vous indique les réactifs mis en présences (l'oxydant d'un couple et le réducteur du second couple, sinon il ne se passe rien !)
  
<br><br>

**Exercice 1 :** Niveau habituel
<center>
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/M5eJ6WXdEfA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>


**Exercice 2 :** Niveau habituel
<center>
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/W-pE-VZ2m8c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>


**Exercice 3 :** Niveau habituel
<center>
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/54MBUkMyayw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>

**Exercice 4 :** Niveau habituel
<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/std0KwLjeqk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>

**Exercice 5 :** Niveau, c'est pas le plus simple !
<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/2PNS1-DMVXU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>


**Exercice 6 :** Niveau le max du max que l'on peut vous demander !
<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/std0KwLjeqk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>





??? remarque "Reconnaitre oxydant et réducteur dans une équation bilan"
    Exercice peu classique, je ne vous intérrogerai pas sur un exercice de ce type, mais sa résolution peu vous aider.
    <center>
    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/ThXNSfqz2Qg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </center>


