---
title : "Exercices"
correctionvisible : True
pdf : 
---


<div class="noprint" style="text-align:right;width:100%;background-color:white">
    <a href="../pdf/{{page.meta.pdf}}.pdf" 
    style="display:inline-block" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="#a3a3a3" stroke-width="1.5" stroke-linecap="butt" stroke-linejoin="arcs"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg>
    </a>
</div>




### Exercice 1 : 
<center>
<div class="grand">
    <figure>
        <img src="../images/images_exercices1/exo1.png" />
        <figcaption>Manuel Numérique Hilbrt p59</figcaption>
    </figure>
</div>
</center>

??? faq correction "Correction"
    1. Un ***oxydant*** est une espèce chimique capable de ***capter*** un ou plusieurs électrons.<br>
    Un ***réducteur*** est une espèce chimique capable de ***donner*** un ou plusieurs électrons.<br><br>

    2. L'oxydant et le réducteur intervenant dans une réaction (demi-équation éléctronique) sont dit conjugués et forment un couple oxydant/réducteur ou couple redox, noté \(Ox/Red\).<br><br>
    3. Lorsqu'une espèce chimique perd un ou des électrons, elle à subit une oxydation. *(le produit de la réaction est un oxydant)*.<br>
    <center>$Réducteur \xrightarrow{Oxydation} Oxydant + n e^-$</center><br><br>
    Lorsqu'une espèce chimique gagne un ou des électrons, elle à subit une réduction. *(le produit de la réaction est un réducteur)*.<br>
    <center>$Oxydant + n e^-   \xrightarrow{réduction} Réducteur$</center><br><br>
    4. On appelle réaction d'oxydoréduction, une réaction dont les réactifs sont un oxydant et un réducteur de deux couples : *oxydant/réducteur* différents. Durant cette réaction, un transfert d'électrons s'effectue entre les réactifs.
    <center>$Ox_1 + Red_2 \xrightarrow{échange d'électrons} Red_1 + Ox_2$</center><br><br>
    5. une réaction d'oxydoréduction peut être vue comme la combinaison de deux demi-équation électronique.
    <center>$Ox_1 + n e^-   \xrightarrow{réduction} Red_1$</center><br>
    <center>$Red_2  \xrightarrow{oxydation} Ox_2 + n e^-$</center><br>
    <center>$Ox_1 + Red_2  \xrightarrow{échange de n électrons} Red_1 + Ox_2$</center><br>
   



### Exercice 2 : 
<center>
<div class="grand">
    <figure>
        <img src="../images/images_exercices1/exo2.png" />
        <figcaption>Manuel Numérique Hilbrt p59</figcaption>
    </figure>
</div>
</center>

??? faq correction "Correction"
    Pour rappel :
    <center>$Oxydant + n e^- = Réducteur$</center><br><br>
    1. <center>$Pb^{4+} + 2e^- = Pb^{2+}$</center><br>
    Ainsi, on en déduit que \(Pb^{4+}\) est l'oxydant du couple.<br>
    et que \(Pb^{2+}\) est le réducteur du couple.<br>
    On notera le couple redox : \(Pb^{4+} / Pb^{2+}\)<br><br>
    2. <center>$Au^{3+} + 3e^- = Au$</center><br>
    Ainsi, on en déduit que \(Au^{3+}\) est l'oxydant du couple.<br>
    et que \(Au\) est le réducteur du couple.<br>
    On notera le couple redox : \(Au^{3+} / Au \)<br><br>
    3. <center>$Bi^{5+} + 2e^- = Bi^{3+}$</center><br>
    Ainsi, on en déduit que \(Bi^{5+}\) est l'oxydant du couple.<br>
    et que \(Bi^{3+}\) est le réducteur du couple.<br>
    On notera le couple redox : \(Bi^{5+} / Bi^{3+}\)<br><br>




### Exercice 3 : 
<center>
<div class="grand">
    <figure>
        <img src="../images/images_exercices1/exo3.png" />
        <figcaption>Manuel Numérique Hilbrt p59</figcaption>
    </figure>
</div>
</center>

??? faq correction "Correction"
    Pour retrouver les couples redox, on va d'abord regrouper les espèces qui appartiennent à un couple.
    Pour cela, on forme des paires à l'aide des éléments chimiques qui constitue les espèces (S'il y a plusieurs élements on ***termine*** par l'oxygène et l'hydrogène.)<br>
    Ainsi, on peut regrouper :<br>
    \(Cu\) et \(Cu^{2+}\)<br>
    \(Pt^{2+}\) et \(Pt\)<br>
    \(Ni^{2+}\) et \(Ni\)<br>
    \(I_2\) et \(I^{-}\)<br>
    \(H_2O_2\) et \(O_2\)<br>


    Dans un second temps, il faut déterminer qui est l'oxydant et qui est le réducteur. Établir les demi équation électronique peu être un bon moyen.<br><br>
    <center>$Cu^{2+} + 2e^- = Cu$</center>
    On en déduit que \(Cu^{2+}\) est l'oxydant du couple et que \(Cu\) est le réducteur du couple. Le couple redox est donc : \(Cu^{2+}/Cu\)<br><br>
    <center>$Pt^{2+} + 2e^- = Pt$</center>
    On en déduit que \(Pt^{2+}\) est l'oxydant du couple et que \(Pt\) est le réducteur du couple. Le couple redox est donc : \(Pt^{2+}/Pt\)<br><br>
    <center>$Ni^{2+} + 2e^- = Ni$</center>
    On en déduit que \(Ni^{2+}\) est l'oxydant du couple et que \(Ni\) est le réducteur du couple. Le couple redox est donc : \(Ni^{2+}/Ni\)<br><br>
    <center>$I_2 + 2e^- = 2I^-$</center>
    On en déduit que \(I_2\) est l'oxydant du couple et que \(I^-\) est le réducteur du couple. Le couple redox est donc : \(I_2/I^-\)<br><br>
    <center>$O_2 + 2H⁺+ 2e^- = H_2O_2$</center>
    On en déduit que \(O_2\) est l'oxydant du couple et que \(H_2O_2\) est le réducteur du couple. Le couple redox est donc : \(O_2/H_2O_2\)<br><br>




### Exercice 4 : 
<center>
<div class="grand">
    <figure>
        <img src="../images/images_exercices1/exo4.png" />
        <figcaption>Manuel Numérique Hilbrt p59</figcaption>
    </figure>
</div>
</center>

??? faq correction "Correction"
    Pour rappel :
    <center>$Oxydant + n e^- \xrightarrow{réduction} Réducteur$</center><br><br>

    <center>$Cu^{2+} + 2 e^- \xrightarrow{réduction} Cu$</center>
    <center>$ClO_3^{-}+H_2O + 2 e^- \xrightarrow{réduction} ClO_2^- + 2HO^-$</center><br><br>


    Pour rappel :
    <center>$Réducteur \xrightarrow{oxydation} Oxydant + ne^-$</center><br><br>

    <center>$PbO \xrightarrow{oxydation} PbO_2 + H_2O + 2e^-$</center>
    <center>$2I^- \xrightarrow{oxydation} I_2 + 2e^-$</center>




### Exercice 5 : 
<center>
<div class="grand">
    <figure>
        <img src="../images/images_exercices1/exo5.png" />
        <img src="../images/images_exercices1/exo5_2.png" />
        <figcaption>Manuel Numérique Hilbrt p59</figcaption>
    </figure>
</div>
</center>

??? faq correction "Correction"
    <center>$Sn^{4+} + 4 e^- \rightleftharpoons Sn$</center>
    <center>$S_4O_6^{2-}+ 2 e^- \rightleftharpoons 2S_2O_3^{2-}$</center>
    <center>$PO_4^{3-}+11H⁺+7e^- \rightleftharpoons PH_3^{4+}+4H_2O$</center>
    <center>$2HClO +2H⁺ + 2e^- \rightleftharpoons Cl_2 + 2H_2O$</center><br><br>
    <center>$MnO_4^- + 4H⁺+3e^- \rightleftharpoons MnO_2 + 2H_2O$</center>



### Exercice 6 : 
<center>
<div class="grand">
    <figure>
        <img src="../images/images_exercices1/exo6.png" />
        <figcaption>Manuel Numérique Hilbrt p59</figcaption>
    </figure>
</div>
</center>

??? faq correction "Correction"
    <center>$Co^{3+} + e^- = Co^{2+}$</center>
    <center>$NO_2 + 4H⁺ + 4e^- = N_2 + 2H_2O$</center>
    <center>$BrO_4^{-} +2H⁺ + 2e^- = BrO_3^{-} + H_2O$</center><br>
    Hors programme en l'état :
    <center>$Hg_2Cl_2 + 2e^- =2Hg + 2Cl^-$</center>



### Exercice 7 : 
<center>
<div class="grand">
    <figure>
        <img src="../images/images_exercices1/exo7.png" />
        <figcaption>Manuel Numérique Hilbrt p59</figcaption>
    </figure>
</div>
</center>

??? faq correction "Correction"
    Les ions\(NO_3^-\) sont les ions oxydant du couple \(NO_3^-/NO\), la demi-équation électronique correspondante est : 
    <center>$NO_3^- + 4H⁺+ 3e^- =NO + 2H_2O$</center><br>
    Le zinc \(Zn\) est le réducteur du couple \(Zn^{2+}/Zn\), la demi-équation électronique correspondante est : 
    <center>$Zn^{2+} + 2e^- = Zn$</center><br>
    <center>
    <div class="grand">
        <figure>
            <img src="../images/images_exercices1/c_exo7_1.png" />
        </figure>
    </div>
    </center>
    <br><br>

    Les ions\(Fe^{3+}\) sont les ions oxydant du couple \(Fe^{3+}/Fe^{2+}\), la demi-équation électronique correspondante est : 
    <center>$Fe^{3+} + e^- = Fe^{2+}$</center><br>
    Le Fer \(Fe\) est le réducteur du couple\(Fe^{2+}/Fe\), la demi-équation électronique correspondante est : 
    <center>$Fe^{2+} + 2e^- = Fe$</center><br>
    <center>
    <div class="grand">
        <figure>
            <img src="../images/images_exercices1/c_exo7_2.png" />
        </figure>
    </div>
    </center>



### Exercice 8 : 
<center>
<div class="grand">
    <figure>
        <img src="../images/images_exercices1/exo8.png" />
        <img src="../images/images_exercices1/exo8_2.png" />
        <figcaption>Manuel Numérique Hilbrt p59</figcaption>
    </figure>
</div>
</center>
Attention, il y a une erreur dans la question 5, le couple redox est \(NO_3^-/NO\) !

??? faq correction "Correction"
    1. La couleur bleu est du à l'apparition d'ions cuivre dans la solution.<br><br>
    2. Le cuive solide \(Cu\) au début, s'est transformé en ion cuivre \(Cu^{2+}\).<br>
    Il a cédé des électrons, c'est un réducteur. Au cours de la réaction, il subit une oxydation *(on a formé l'oxydant)*.<br><br>
    3. <center>$Cu \xrightarrow{}  Cu^{2+} + 2e^-$</center><br>
    4. Les ions \(NO_3^-\) sont oxydant du couple \(NO_3^-/NO\) (oui, il y a une erreur dans le sujet !). Au cours de la réaction, les ions \(NO_3^-\) vont subir une réduction *(on forme le réducteur)*.<br><br>
    5. La demi-équation électronique est :<br>
    <center>$NO_3^- + 4H^+ + 3e^- \xrightarrow{}  NO + 2H_2O$</center><br><br>
    6. 
    <center>
    <div class="grand">
        <figure>
            <img src="../images/images_exercices1/c_exo8_1.png" />
        </figure>
    </div>
    </center>



   



### Exercice 9 : 
<center>
<div class="grand">
    <figure>
        <img src="../images/images_exercices1/exo9.png" />
        <img src="../images/images_exercices1/exo9_2.png" />
        <img src="../images/images_exercices1/exo9_3.png" />
        <img src="../images/images_exercices1/exo9_4.png" />
        <figcaption>Manuel Numérique Hilbrt p59</figcaption>
    </figure>
</div>
</center>
Attention, il y a une erreur dans la question 2.a, le couple redox est \(O_2/O^{2-}\) !

??? faq correction "Correction"
    <center>
    <div class="grand">
        <figure>
            <img src="../images/images_exercices1/c_exo9_1.png" />
            <img src="../images/images_exercices1/c_exo9_2.png" />
        </figure>
    </div>
    </center>





<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  

