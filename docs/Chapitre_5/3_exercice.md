---
title : "Exercices correction "
correctionvisible : true
pdf : 
---


<div class="noprint" style="text-align:right;width:100%;background-color:white">
    <a href="../pdf/{{page.meta.pdf}}.pdf" 
    style="display:inline-block" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="#a3a3a3" stroke-width="1.5" stroke-linecap="butt" stroke-linejoin="arcs"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg>
    </a>
</div>




### Exercice 1 : 
1. Un ***oxydant*** est une espèce chimique capable de ***capter*** un ou plusieurs électrons.<br><br>
2. Un ***réducteur*** est une espèce chimique capable de ***donner*** un ou plusieurs électrons.<br><br>
3. Lorsqu'une espèce chimique perd un ou des électrons, elle à subit une oxydation. *(le produit de la réaction est un oxydant)*.<br><br>
    <center>$Réducteur \xrightarrow{Oxydation} Oxydant + n e^-$</center><br><br>
4. On appelle réaction d'oxydoréduction, une réaction dont les réactifs sont un oxydant et un réducteur de deux couples : *oxydant/réducteur* différents. Durant cette réaction, un transfert d'électrons s'effectue entre les réactifs.
<center>$Ox_1 + Red_2 \xrightarrow{échange d'électrons} Red_1 + Ox_2$</center><br><br>
Une réaction d'oxydoréduction peut être vue comme la combinaison de deux demi-équation électronique.
<center>$Ox_1 + n e^-   \xrightarrow{réduction} Red_1$</center><br>
<center>$Red_2  \xrightarrow{oxydation} Ox_2 + n e^-$</center><br>
<center>$Ox_1 + Red_2  \xrightarrow{échange de n électrons} Red_1 + Ox_2$</center><br>
   



### Exercice 2 : 
<center>
<div class="grand">
    <figure>
        <img src="../Evaluation/correction/2_1.png" />
    </figure>
</div>
</center>



### Exercice 3 : 
<center>
<div class="grand">
    <figure>
        <img src="../Evaluation/correction/3.png" />
        <img src="../Evaluation/correction/3_2.png" />
    </figure>
</div>
</center>


### Exercice 4 : 
<center>
<div class="grand">
    <figure>
        <img src="../Evaluation/correction/4.png" />
    </figure>
</div>
</center>



### Exercice 5 : 
<center>
<div class="grand">
    <figure>
        <img src="../Evaluation/correction/5_1.png" />
    </figure>
</div>
</center>

### Exercice 6 : 
<center>
<div class="grand">
    <figure>
        <img src="../Evaluation/correction/6_1.png" />
        <img src="../Evaluation/correction/6_2.png" />
        <img src="../Evaluation/correction/6_3.png" />
    </figure>
</div>
</center>



### Exercice 7 : 
<center>
<div class="grand">
    <figure>
        <img src="../Evaluation/correction/7.png" />
        <img src="../Evaluation/correction/7_1.png" />
    </figure>
</div>
</center>





<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  

