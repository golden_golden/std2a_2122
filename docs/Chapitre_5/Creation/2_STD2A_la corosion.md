# Les métaux

## 1. Les réaction d'oxydoréduction
<center>
<iframe src="../pdf/Terminale  STI2D -Chapitre 10_Les matériaux.pdf" height="800" width="1200"></iframe>
</center>

## 2. La corrosion des métaux
<center>
<iframe src="../pdf/Première  STI2D -Chapitre 10_Les matériaux.pdf" height="800" width="1200"></iframe>
</center>



??? remarque "Conception"
    aqua fortis<br>
    van dyck ou cyanotype<br>
    Oxyde métalique (coloration des céramiques)<br>
