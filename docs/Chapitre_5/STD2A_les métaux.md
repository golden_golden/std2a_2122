# Les métaux

## 2. La corrosion des métaux

<iframe src="../pdf/Première  STI2D -Chapitre 10_Les matériaux.pdf" height="200" width="800"></iframe>



<!---
??? remarque "Conception"
    phénomène de corrosions des métaux<br>
    protection des métaux de la corrosion <br>
    aqua fortis<br>
    van dyck ou cyanotype<br>
    Oxyde métalique (coloration des céramiques)<br>
--->
