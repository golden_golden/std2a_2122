---
title : Exercices
correctionvisible : true
pdf : "exercices"
---


<div class="noprint" style="text-align:right;width:100%;background-color:white">
    <a href="../pdf/{{page.meta.pdf}}.pdf" 
    style="display:inline-block" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="#a3a3a3" stroke-width="1.5" stroke-linecap="butt" stroke-linejoin="arcs"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg>
    </a>
</div>



# Exercices sur la lumière


## Longueurs d'onde et énergie
### Exercice 1 : 
<center>
<div class="grand">
    <figure>
        <img src="../images_exercices/exo1.png" />
        <figcaption>Manuel Numérique Hilbrt p93</figcaption>
    </figure>
</div>
</center>

!!! faq correction "Correction"
    1. La relation entre la fréquence d'une onde $ν$ et sa longueur d'onde $λ$ est :<br>
    <center>$λ= \frac{c}{ν}$</center>
    Autre notation :<br>
    <center>$ν= \frac{c}{λ}$</center>

    1. La relation précédente nous montre que la fréquence est proportionnelle à l'inverse de la longueur d'onde ainsi si plus la longueur d'ondes est élevée, plus la fréquence est petite.
   
    1. On cherche la fréquence d'une ondes ayant une longueur d'onde de $λ=850nm$:
    <center>$ν= \frac{c}{λ}$</center>
    **Application numérique :**<br>
    $ν= \frac{3,00.10⁸ m.s^{-1}}{850.10^{-9}m}=3,53.10^{14} s^{-1}= 3,53.10^{14} Hz$<br>
    La fréquence d'une ondes ayant une longueur d'onde de $λ=850nm$ est de $ν= 3,53.10^{14} Hz$.<br>

    1. On cherche la longueur d'onde d'une onde ayant une fréquence de $ν=2,2.10^{12} MHz$:
    <center>$λ= \frac{c}{ν}$</center>
    **Application numérique :**<br>
    $λ= \frac{3,00.10⁸ m.s^{-1}}{2,2.10^{12} MHz}= \frac{3,00.10⁸ m.s^{-1}}{2,2.10^{12}.10^{6} Hz} = \frac{3,00.10⁸ m.s^{-1}}{2,2.10^{18} s^{-1}}=1,4.10^{-10} m$<br>
    La longueur d'onde d'une onde ayant une fréquence de $ν=2,2.10^{12} MHz$ est de $λ=1,4.10^{-10} m$.

    1. On cherche l'énergie d'une onde ayant une fréquence de $ν=5,4.10^{10} GHz$:
    <center>$E= h×ν$</center>
    **Application numérique :**<br>
    $E= 6,63.10^{-34} J.s  × 5,4.10^{10} GHz= 6,63.10^{-34} J.s × 5,4.10^{10}.10^{9} Hz = 6,63.10^{-34} J.s × 5,4.10^{19}s^{-1} =3,58.10^{-14}J$<br>
    L'énergie d'une onde ayant une fréquence de $ν=5,4.10^{10} GHz$ est de $E=3,58.10^{-14}J$.

    1. La particule qui transporte cette énergie est le photon.

  



### Exercice 2 : 
<center>
<div class="grand">
    <figure>
        <img src="../images_exercices/exo2.png" />
        <figcaption>Manuel Numérique Hilbrt p93</figcaption>
    </figure>
</div>
</center>

!!! faq correction "Correction"
    1. 
    <center>
        <div class="grand">
            <figure>
                <img src="../images_exercices/exo2c.png" />
                <figcaption>Cours Manuel Numérique Hilbrt p83</figcaption>
            </figure>
        </div>
    </center>

    2. Les limites du domaine visibles dans le vide sont pour des longueurs d'ondes comprises entre $400nm et 800nm$.<br>
    On cherche le domaine de fréquence correspondant :<br>
    <center>$ν= \frac{c}{λ}$</center>
    **Application numérique :**<br>
    $ν_1= \frac{3,00.10⁸ m.s^{-1}}{400.10^{-9}m}= 7,50.10^{14} Hz$<br>
    $ν_2= \frac{3,00.10⁸ m.s^{-1}}{800.10^{-9}m}= 3,75.10^{14} Hz$<br>
    Les limites du domaine visibles sont $ν_2= 3,75.10^{14} Hz$ et $ν_1= 7,50.10^{14} Hz$.
    <center>
        <div class="grand">
            <figure>
                <img src="../images_exercices/exo2c2.png" />
                <figcaption>Cours Manuel Numérique Hilbrt p83</figcaption>
            </figure>
        </div>
    </center>

    3. Pour être en mesure d'exploiter le document mis à disposition dans l'exercice, il est nécéssaire de déterminer la longueur d'onde.
    On cherche la longueur d'onde d'une onde ayant une fréquence de $ν=3.10^{18} Hz$:
    <center>$λ= \frac{c}{ν}$</center>
    **Application numérique :**<br>
    $λ= \frac{3,00.10⁸ m.s^{-1}}{3.10^{18} Hz} = \frac{3,00.10⁸ m.s^{-1}}{3.10^{18} s^{-1}}=1.10^{-10} m$<br>
    La longueur d'onde d'une onde ayant une fréquence de $ν=3.10^{18} Hz$ est de $λ=10^{-10} m$.
    Après exploitation de la figure, on constate que cette onde appartient au domaine des Rayons X.

   
### Exercice 3 : 
<center>
<div class="grand">
    <figure>
        <img src="../images_exercices/exo3.png" />
        <figcaption>Manuel Numérique Hilbrt p93</figcaption>
    </figure>
</div>
</center>

!!! faq correction "Correction"
    1. Les rayons infrarouges n'appartiennent pas au domaine du visibles. Notre œil n'est pas sensible aux rayonnements de ce domaines.
    <center>
        <div class="grand">
            <figure>
                <img src="../images_exercices/exo2c.png" />
                <figcaption>Cours Manuel Numérique Hilbrt p83</figcaption>
            </figure>
        </div>
    </center>


  


### Exercice 4 : 
<center>
<div class="grand">
    <figure>
        <img src="../images_exercices/exo4.png" />
        <figcaption>Manuel Numérique Hilbrt p93</figcaption>
    </figure>
</div>
</center>

!!! faq correction "Correction"
    1. 
    <center>
        <div class="grand">
            <figure>
                <img src="../images_exercices/exo2c2.png" />
                <figcaption>Cours Manuel Numérique Hilbrt p83</figcaption>
            </figure>
        </div>
    </center>
  

### Exercice 5 : 
<center>
<div class="grand">
    <figure>
        <img src="../images_exercices/exo5texte.png" />
        <img src="../images_exercices/exo5questions.png" />
        <figcaption>Manuel Numérique Hilbrt p94</figcaption>
    </figure>
</div>
</center>

!!! faq correction "Correction"
    1. On cherche la longueur d'onde du rayonnement laser utilisé, noté $λ$:<br>
    
    D'après le cours <br>
    $ξ_{photon}= \frac{h×c}{λ}$<br>
    Donc :
    <center>$λ= \frac{h×c}{ξ_{photon}}$</center>
    **Application numérique :**<br>
    $λ= \frac{6,63.10^{-34} J.s × 3,00.10⁸ m.s^{-1}}{1,87.10^{-20} J} =1,06.10^{-5} m$<br>
    La longueur d'onde du rayonnement laser utilisé est de $λ=1,06.10^{-5} m$.<br>


    On cherche la fréquence de l'onde du laser, noté $υ$:<br>
    <center>$ν= \frac{c}{λ}$</center>
    **Application numérique :**<br>
    $ν= \frac{3,00.10⁸ m.s^{-1}}{1,06.10^{-5}m}= 2,83.10^{13} Hz$<br>
    La fréquence de l'onde du laser est de $ν=2,83.10^{13} Hz$.<br>

    **Remarque :** On aurait put calculer la fréquence directement.<br>
    $ξ_{photon}= h×ν$<br>
    Donc :<br>
    <center>$ν= \frac{ξ_{photon}}{h}$</center>
    **Application numérique :**<br>
    $ν= \frac{1,87.10^{-20}J}{6,63.10^{-34} J.s} =2,82.10^{13} Hz$<br>
    *L'écart vient des arrondits succécif dans la première méthodes.*<br>
    La fréquence de l'onde du laser est de $ν=2,83.10^{13} Hz$.<br>

    2. Ce type de rayonnement appartient au domaine des infrarouges (IR) d'après la figure ci-dessous.
    <center>
        <div class="grand">
            <figure>
                <img src="../images_exercices/exo2c.png" />
                <figcaption>Cours Manuel Numérique Hilbrt p83</figcaption>
            </figure>
        </div>
    </center>



### Exercice 6 : 
<center>
<div class="grand">
    <figure>
        <img src="../images_exercices/exo6.png" />
        <figcaption>Manuel Numérique Hilbrt p94</figcaption>
    </figure>
</div>
</center>

!!! faq correction "Correction"
    1. On cherche la fréquence de l'onde, noté $ν$:<br>
    D'après le cours<br>
    $ξ_{photon}= h×ν$<br>
    Donc :<br>
    <center>$υ= \frac{ξ_{photo}}{h}$</center>
    **Application numérique :**<br>
    $ν= \frac{2.36.10^{-20}J}{6,63.10^{-34} J.s} =3,56.10^{13} Hz$<br>
    La fréquence de l'onde du laser est de $ν=3,56.10^{13} Hz$.<br>


    On cherche la longueur d'onde du rayonnement laser utilisé, noté $λ$:<br>
    <center>$λ= \frac{c}{ν}$</center>
    **Application numérique :**<br>
    $λ= \frac{3,00.10⁸ m.s^{-1}}{3,56.10^{13} Hz}=8,43.10^{-6} m$<br>

    
    **Remarque :** On aurait put calculer la longueur d'onde directement.<br>

    $ξ_{photon}= \frac{h×c}{λ}$<br>
    Donc :
    <center>$λ= \frac{h×c}{E_{photon}}$</center>
    **Application numérique :**<br>
    $λ= \frac{6,63.10^{-34} J.s × 3,00.10⁸ m.s^{-1}}{2,36.10^{-20} J} =8,43.10^{-6} m$<br>
    La longueur d'onde du rayonnement laser utilisé est de $λ=8,43.10^{-6} m$.<br>


### Exercice 7 : 
<center>
<div class="grand">
    <figure>
        <img src="../images_exercices/exo7texte.png" />
        <img src="../images_exercices/exo7questions.png" />
        <figcaption>Manuel Numérique Hilbrt p94</figcaption>
    </figure>
</div>
</center>

!!! faq correction "Correction"
    1. On sait que :
     <center>
        <div class="grand">
            <figure>
                <img src="../images_exercices/exo2c.png" />
                <figcaption>Cours Manuel Numérique Hilbrt p83</figcaption>
            </figure>
        </div>
    </center>
    Or l'énergie du photon est proportionnelle à la fréquence, en effet :<br>
    $ξ_{photon}= h×ν$<br>
    Ainsi plus la fréquence est grande plus l'énergie du photon est grande.<br>
    Par ordre d'énergies croissance, on a donc :<br>
    <center>$E_{Ondes radio}<E_{Micro-ondes}<E_{IR}<E_{Visible}<E_{UV}<E_{Rayons X}<E_{Rayons γ}$</center>
    1. Les rayons γ et les rayons X présentent un danger pour l'être humain, car ce sont des rayonnements très énergétiques. Il intéragissent avec les cellules de l'organisme.

  

### Exercice 8 : 
<center>
<div class="grand">
    <figure>
        <img src="../images_exercices/exo8texte.png" />
        <img src="../images_exercices/exo8photo.png" />
        <img src="../images_exercices/exo8questions.png" />
        <figcaption>Manuel Numérique Hilbrt p94</figcaption>
    </figure>
</div>
</center>

!!! faq correction "Correction"
    1. On cherche la longueur d'onde du rayonnement laser utilisé, noté $λ$:<br>
    
    D'après le cours <br>
    $ξ_{photon}= \frac{h×c}{λ}$<br>
    Donc :
    <center>$λ= \frac{h×c}{ξ_{photon}}$</center>
    **Application numérique :**<br>
    $λ= \frac{6,63.10^{-34} J.s × 3,00.10⁸ m.s^{-1}}{5,59.10^{-19} J} =3,56.10^{-7} m=356.10^{-9} m$<br>
    La longueur d'onde du rayonnement laser utilisé est de $λ=356 nm$.<br>


    On cherche la fréquence de l'onde du laser, noté $υ$:<br>
    <center>$ν= \frac{c}{λ}$</center>
    **Application numérique :**<br>
    $ν= \frac{3,00.10⁸ m.s^{-1}}{356.10^{-9} m}= 8,43.10^{14} Hz$<br>
    La fréquence de l'onde du laser est de $ν=8,43.10^{14} Hz$.<br>

    **Remarque :** On aurait put calculer la fréquence directement.<br>
    $ξ_{photon}= h×ν$<br>
    Donc :<br>
    <center>$ν= \frac{ξ_{photon}}{h}$</center>
    **Application numérique :**<br>
    $ν= \frac{5,59.10^{-19}J}{6,63.10^{-34} J.s} =8,43.10^{14} Hz$<br>
    *L'écart vient des arrondits succécif dans la première méthodes.*<br>
    La fréquence de l'onde du laser est de $ν=8,43.10^{14} Hz$.<br>

    2. D'après la figure ci-dessous, ce type de rayonnement appartient au domaine des infrarouges (IR) mais il se trouve très proche du domaine du visible. 
    <center>
        <div class="grand">
            <figure>
                <img src="../images_exercices/exo2c.png" />
                <figcaption>Cours Manuel Numérique Hilbrt p83</figcaption>
            </figure>
        </div>
    </center>
  

<br>

## Interaction lumière-matière et source de lumière
### Exercice 9 : 
<center>
<div class="grand">
    <figure>
        <img src="../images_exercices/exo9.png" />
        <figcaption>Manuel Numérique Hilbrt p95</figcaption>
    </figure>
</div>
</center>

!!! faq correction "Correction"
    (*Réalisée en classe*)<br>
    1. La transition représentée correspond à l'émmision d'un photo.
    2. $ξ_p$ représente l'énergie de l'état excité et $ξ_n$ représent l'énergie dans l'état fondamental.
    3. L'énegie du photon correspond à l'écart d'énergie entre $ξ_p$ et $ξ_n$.<br>
    $ξ_{photon}=ξ_{final}-ξ_{initial}$<br>
    Soit ici : <br>
    <center>$ξ_{photon}=ξ_{n}-ξ_{p}$</center>
    1. La relation entre $h, ν, ξ_{photon}$ est :<br>
    $ξ_{photon}= h×ν$  (1)<br>
    Or $ξ_{photon}=ξ_{n}-ξ_{p}$ (2)<br>
    Par substitution de (2) dans (1), on en déduit que :
    $ξ_{n}-ξ_{p}= h×ν$
    <center>$ν=\frac{ξ_{n}-ξ_{p}}{h}$</center>
  


### Exercice 10 : 
<center>
<div class="grand">
    <figure>
        <img src="../images_exercices/exo10.png" />
        <figcaption>Manuel Numérique Hilbrt p95</figcaption>
    </figure>
</div>
</center>

!!! faq correction "Correction"
    1.
    <center>
        <div class="grand">
            <figure>
                <img src="../images_exercices/exo10diagramme2.png" />
            </figure>
        </div>
    </center>
    2. On cherche l'énergie du photon, noté $ξ_{photon}$:<br>
    $ξ_{photon}=ξ_{final}-ξ_{initial}$<br>
    Soit ici :<br>
    <center>$ξ_{photon}=ξ_{∞}-ξ_{1}$ </center>
    **Application numérique :**<br>
    $ξ_{photon}=0eV-(-13,6eV)=13,6 eV$<br>
    L'énergie du photon émis est de $ξ_{photon}=13,6 eV$<br>
    3. On cherche la longueur d'onde correspondant, noté $λ$.<br>
    D'après le cours <br>
    $ξ_{photon}= \frac{h×c}{λ}$<br>
    Donc :
    <center>$λ= \frac{h×c}{ξ_{photon}}$</center>
    **Application numérique :**<br>
    $λ= \frac{6,63.10^{-34} J.s × 3,00.10⁸ m.s^{-1}}{13,6 eV}$<br>
    Il y a un problème au niveau de l'unité de l'énergie du photo, celle-ci doit être exprimée en J.<br> 
    Or $1eV=1,60.10^{-19}J$, donc :<br>
    $λ= \frac{6,63.10^{-34} J.s × 3,00.10⁸ m.s^{-1}}{13,6×1,60.10^{-19}J}=9,1.10^{-8} m$<br>
    La longueur d'onde du photon émis est de $λ=91nm$.<br>


### Exercice 11 : 
<center>
<div class="grand">
    <figure>
        <img src="../images_exercices/exo11.png" />
        <img src="../images_exercices/exo11questions.png" />
        <figcaption>Manuel Numérique Hilbrt p95</figcaption>
    </figure>
</div>
</center>

!!! faq correction "Correction"
    1. Le spectre présenté est constitué d'une raie lumineuse sur un fond noir, il s'agit donc d'un spectre de raie d'émission.<br>
    2. 
    <center>
        <div class="grand">
            <figure>
                <img src="../images_exercices/diagrammesemission.png" />
            </figure>
        </div>
    </center>
    De plus, la longueur d'onde du photo émis est d'environ, $λ=520nm$, d'après le spectre fournit sur le sujet.<br>
    On cherche l'énergie du photo, noté $ξ_{photon}$<br>
    <center>$ξ_{photon}= \frac{h×c}{λ}$</center>
    **Application numérique :**<br>
    $ξ_{photon}= \frac{6,63.10^{-34} J.s × 3,00.10⁸ m.s^{-1}}{520.10^{-9} m} =3,83.10^{-19} J$<br>
    Conversions :<br>
    $ξ_{photon}=\frac{3,83.10^{-19}J}{1,60.10^{-19}J.eV^{-1}}= 2,39eV$<br>
    Dans le diagramme énergétique, on sait également que :
    $ξ_{photon}=ξ_{final}-ξ_{initial}= 2,39eV$
  

### Exercice 12 : 
<center>
<div class="grand">
    <figure>
        <img src="../images_exercices/exo12.png" />
        <figcaption>Manuel Numérique Hilbrt p95</figcaption>
    </figure>
</div>
</center>

!!! faq correction "Correction"
    1. On cherche la longueur d'onde du photo émis, noté $λ$ :<br>
    D'après le cours <br>
    $ξ_{photon}= \frac{h×c}{λ}$<br>
    Donc :
    <center>$λ= \frac{h×c}{ξ_{photon}}$</center>
    **Application numérique :**<br>
    $λ= \frac{6,63.10^{-34} J.s × 3,00.10⁸ m.s^{-1}}{2,12 eV}$<br>
    Il y a un problème au niveau de l'unité de l'énergie du photo, celle-ci doit être exprimée en J.<br> 
    Or $1eV=1,60.10^{-19}J$, donc :<br>
    $λ= \frac{6,63.10^{-34} J.s × 3,00.10⁸ m.s^{-1}}{2,12×1,60.10^{-19}J}=589.10^{-9} m$<br>
    La longueur d'onde du photon émis est de $λ=589nm$.<br>
    Lorsque l'on reporte cela sur le spectre de la lumière blanche, on constate que cela correspond à une radiations plutôt jaune.


### Exercice 13 : 
<center>
<div class="grand">
    <figure>
        <img src="../images_exercices/exo13.png" />
        <figcaption>Manuel Numérique Hilbrt p95</figcaption>
    </figure>
</div>
</center>

!!! faq correction "Correction"
    1. On cherche l'énergie du photo, noté $ξ_{photo}$<br>
    <center>$ξ_{photon}= \frac{h×c}{λ}$</center>
    **Applications numériques :**<br>
    Pour le photon 1 de longueur d'onde $λ_1=445nm$ :<br>
    $ξ_{photon1}= \frac{6,63.10^{-34} J.s × 3,00.10⁸ m.s^{-1}}{445.10^{-9} m} =4,47.10^{-19} J$<br>
    Soit :<br>
    $ξ_{photon1}=\frac{4,47.10^{-19}J}{1,60.10^{-19}J.eV^{-1}}= 2,79eV$<br>
    L'énergie du photon 1 est de $ξ_{photon1}= 2,79eV$.<br>

    Pour le photon 2 de longueur d'onde $λ_2=500nm$ :<br>
    $ξ_{photon2}= \frac{6,63.10^{-34} J.s × 3,00.10⁸ m.s^{-1}}{500.10^{-9} m} =3,98.10^{-19} J$<br>
    Soit :<br>
    $ξ_{photon2}=\frac{3,98.10^{-19}J}{1,60.10^{-19}J.eV^{-1}}= 2,49eV$<br>
    L'énergie du photon 2 est de $ξ_{photon2}= 2,49eV$.<br>

    Pour le photon 3 de longueur d'onde $λ_3=590nm$ :<br>
    $ξ_{photon3}= \frac{6,63.10^{-34} J.s × 3,00.10⁸ m.s^{-1}}{590.10^{-9} m} =3,37.10^{-19} J$<br>
    Soit :<br>
    $ξ_{photon3}=\frac{3,37.10^{-19}J}{1,60.10^{-19}J.eV^{-1}}= 2,11eV$<br>
    L'énergie du photon 3 est de $ξ_{photon3}= 2,11eV$<br>

    Pour le photon 4 de longueur d'onde $λ_4=670nm$ :<br>
    $ξ_{photon4}= \frac{6,63.10^{-34} J.s × 3,00.10⁸ m.s^{-1}}{670.10^{-9} m} =2,97.10^{-19} J$<br>
    Soit :<br>
    $ξ_{photon4}=\frac{2,97.10^{-19}J}{1,60.10^{-19}J.eV^{-1}}= 1,56eV$<br>
    L'énergie du photon 4 est de $ξ_{photon4}= 1,56eV$<br>
  

### Exercice 14 : 
<center>
<div class="grand">
    <figure>
        <img src="../images_exercices/exo14texte.png" />
        <img src="../images_exercices/exo14.png" />
        <figcaption>Manuel Numérique Hilbrt p95</figcaption>
    </figure>
</div>
</center>

!!! faq correction "Correction"
    1.
    <center>
        <div class="grand">
            <figure>
                <img src="../images_exercices/diagrammesemission.png" />
            </figure>
        </div>
    </center>

    1. On cherche la longueur d'onde du photo émis, noté $λ$ :<br>
    D'après le cours <br>
    $ξ_{photon}= \frac{h×c}{λ}$<br>
    Donc :<br>
    $λ= \frac{h×c}{ξ_{photon}}$<br>
    Or :<br>
    $ξ_{photon}= E_{1}-E_{0}$<br>
    Par substitution, on obtient alors :
    <center>$λ= \frac{h×c}{E_{1}-E_{0}}$</center>
    **Application numérique :**<br>
    $λ= \frac{6,63.10^{-34} J.s × 3,00.10⁸ m.s^{-1}}{(-3,03 eV)-(-5,14 eV)}$<br>
    Il y a un problème au niveau de l'unité de l'énergie du photo, celle-ci doit être exprimée en J.<br> 
    Or $1eV=1,60.10^{-19}J$, donc :<br>
    $λ= \frac{6,63.10^{-34} J.s × 3,00.10⁸ m.s^{-1}}{((-3,03 eV)-(-5,14 eV))×1,60.10^{-19}J}=589.10^{-9} m$<br>
    La longueur d'onde du photon émis est de $λ=589nm$.<br>
    1. Cette longueur d'onde est comprise entre 400nm et 800nm. Ce photon appartient donc au dommaine du visible.
    2. D'après le spectre proposé, la couleur de ce photon est jaune.
    3. Le spectre de la lumière émise par le gaz :
    <center>
        <div class="grand">
            <figure>
                <img src="../images_exercices/diagrammesodium.png" />
            </figure>
        </div>
    </center>


### Exercice 15 : 
<center>
<div class="grand">
    <figure>
        <img src="../images_exercices/exo15.png" />
        <img src="../images_exercices/exo15texte.png" />
        <img src="../images_exercices/exo15questions.png" />
        <figcaption>Manuel Numérique Hilbrt p96</figcaption>
    </figure>
</div>
</center>

!!! faq correction "Correction"
    1. 
    2. Le photon émis posséde une couleur appartenant au domaine du Jaune-vert. L'étude du spectre de la lumière blanche, nous apprend que la longueur d'onde du photons est donc comprise entre $λ_1=475nm$ et $λ_2=600nm$.
    3. On cherche le domaine d'énergie correspondant.<br>
    D'après le cours :<br>
    <center>$ξ_{photon}= \frac{h×c}{λ}$</center>
    **Application numérique :**<br>
    $ξ_{photon1}= \frac{6,63.10^{-34} J.s × 3,00.10⁸ m.s^{-1}}{475.10^{-9} m} =4,23.10^{-19} J$<br>
    Conversions :<br>
    $ξ_{photon1}=\frac{4,23.10^{-19}J}{1,60.10^{-19}J.eV^{-1}}= 2,64eV$<br>
    $ξ_{photon2}= \frac{6,63.10^{-34} J.s × 3,00.10⁸ m.s^{-1}}{600.10^{-9} m} =3,32.10^{-19} J$<br>
    Conversions :<br>
    $ξ_{photon2}=\frac{4,23.10^{-19}J}{1,60.10^{-19}J.eV^{-1}}= 2,08eV$<br>
    Le photon possède une énergie comprise entre $2,64eV$ et $2,08eV$.<br>
    4.?<br>
  
  

### Exercice 16 : 
<center>
<div class="grand">
    <figure>
        <img src="../images_exercices/exo16.png" />
        <img src="../images_exercices/exo16photo.png" />
        <img src="../images_exercices/exo16question.png" />
        <figcaption>Manuel Numérique Hilbrt p96</figcaption>
    </figure>
</div>
</center>

!!! faq correction "Correction"
    1. "La luminescence désigne l'émission d'un rayonnement électromagnétique d'origine non thermique, par opposition à l'incandescence."<br>
    2. Dans le cas présenté, la lumière n'est pas émise par une source thermique, il s'agit donc bien de luminescence d'après la définition cité à la question précédente.<br>





<script type="text/javascript">
    let test = "{{page.meta.correctionvisible}}"
    var elmts = document.getElementsByClassName('correction');
    if (test == "False"){
        for(var i=0;i<elmts.length;i++){
            elmts[i].style.display='none';
        }
    };
</script>  

