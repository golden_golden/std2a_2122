# Chapitre 1: Les ondes électromagnétiques et la Lumière 


## La dualité ondes corpuscules 
- [dualité onde-corpuscule](https://scienceetonnante.com/2021/01/15/la-dualite-onde-corpuscule/)
- [La plus belle expérience de physique](https://scienceetonnante.com/2018/04/13/lexperience-des-fentes-dyoung-en-mecanique-quantique/)
  
*Pour aller plus loin :*
- [les inégalités de Bell](https://scienceetonnante.com/2020/10/23/bell-aspect/)



## Les ondes électromagnétiques

## Modèle ondulatoire
### Vitesse de propagation
### Périodicité spaciale : la longueur d'onde
### Périodicité temporelle : la période
### Fréquence


[Planck (physique fondamentale) - Passe-science #29](https://www.youtube.com/watch?v=TBXze1ZV6NE)

## Modèle corpusculaire

 

### Les différents dommaines des ondes électromagnétiques
#### Les ondes-radio
#### Les ultra-violets
calaméo
#### Les Infra-rouges
calaméo+histore kodac 
#### Les rayons X
calaméo




### Production de rayonnement 
## Le rayonnement des corps noirs
[Planck (physique fondamentale) - Passe-science #29](https://www.youtube.com/watch?v=TBXze1ZV6NE)



## Intéraction de la lumière avec la matière 
- [Atomes et rayonnemnent_jean Dalibard](https://www.youtube.com/watch?v=YhMa2gPKdaw)

### Avec les métaux
### Les milieux transparant
### Les substances colorée
#### La fluoréscence 
activité micro fluorescence X, calaméo
#### La Phosphoresance




----
remarque parler de rayonnement comme transfert d'énergie thermique 
---
