### Exercice 2 : 
<center>
<div class="grand">
    <figure>
        <img src="../images_exercices/exo2.png" />
        <figcaption>Manuel Numérique Hilbrt p93</figcaption>
    </figure>
</div>
</center>

!!! faq correction "Correction"
    1. 
    <center>
        <div class="grand">
            <figure>
                <img src="../images_exercices/exo2c.png" />
                <figcaption>Cours Manuel Numérique Hilbrt p83</figcaption>
            </figure>
        </div>
    </center>

    2. Les limites du domaine visibles dans le vide sont pour des longueurs d'ondes comprises entre $400nm et 800nm$.<br>
    On cherche le domaine de fréquence correspondant :<br>
    <center>$ν= \frac{c}{λ}$</center>
    **Application numérique :**<br>
    $ν_1= \frac{3,00.10⁸ m.s^{-1}}{400.10^{-9}m}= 7,50.10^{14} Hz$<br>
    $ν_2= \frac{3,00.10⁸ m.s^{-1}}{800.10^{-9}m}= 3,75.10^{14} Hz$<br>
    Les limites du domaine visibles sont $ν_2= 3,75.10^{14} Hz$ et $ν_1= 7,50.10^{14} Hz$.
    <center>
        <div class="grand">
            <figure>
                <img src="../images_exercices/exo2c2.png" />
                <figcaption>Cours Manuel Numérique Hilbrt p83</figcaption>
            </figure>
        </div>
    </center>

    3. Pour être en mesure d'exploiter le document mis à disposition dans l'exercice, il est nécéssaire de déterminer la longueur d'onde.
    On cherche la longueur d'onde d'une onde ayant une fréquence de $ν=3.10^{18} Hz$:
    <center>$λ= \frac{c}{ν}$</center>
    **Application numérique :**<br>
    $λ= \frac{3,00.10⁸ m.s^{-1}}{3.10^{18} Hz} = \frac{3,00.10⁸ m.s^{-1}}{3.10^{18} s^{-1}}=1.10^{-10} m$<br>
    La longueur d'onde d'une onde ayant une fréquence de $ν=3.10^{18} Hz$ est de $λ=10^{-10} m$.
    Après exploitation de la figure, on constate que cette onde appartient au domaine des Rayons X.


### Exercice 10 : 
<center>
<div class="grand">
    <figure>
        <img src="../images_exercices/exo10.png" />
        <figcaption>Manuel Numérique Hilbrt p95</figcaption>
    </figure>
</div>
</center>

!!! faq correction "Correction"
    1.
    <center>
        <div class="grand">
            <figure>
                <img src="../images_exercices/exo10diagramme2.png" />
            </figure>
        </div>
    </center>
    2. On cherche l'énergie du photon, noté $ξ_{photon}$:<br>
    $ξ_{photon}=ξ_{final}-ξ_{initial}$<br>
    Soit ici :<br>
    <center>$ξ_{photon}=ξ_{∞}-ξ_{1}$ </center>
    **Application numérique :**<br>
    $ξ_{photon}=0eV-(-13,6eV)=13,6 eV$<br>
    L'énergie du photon émis est de $ξ_{photon}=13,6 eV$<br>
    3. On cherche la longueur d'onde correspondant, noté $λ$.<br>
    D'après le cours <br>
    $ξ_{photon}= \frac{h×c}{λ}$<br>
    Donc :
    <center>$λ= \frac{h×c}{ξ_{photon}}$</center>
    **Application numérique :**<br>
    $λ= \frac{6,63.10^{-34} J.s × 3,00.10⁸ m.s^{-1}}{13,6 eV}$<br>
    Il y a un problème au niveau de l'unité de l'énergie du photo, celle-ci doit être exprimée en J.<br> 
    Or $1eV=1,60.10^{-19}J$, donc :<br>
    $λ= \frac{6,63.10^{-34} J.s × 3,00.10⁸ m.s^{-1}}{13,6×1,60.10^{-19}J}=9,1.10^{-8} m$<br>
    La longueur d'onde du photon émis est de $λ=91nm$.<br>


### Exercice 11 : 
<center>
<div class="grand">
    <figure>
        <img src="../images_exercices/exo11.png" />
        <img src="../images_exercices/exo11questions.png" />
        <figcaption>Manuel Numérique Hilbrt p95</figcaption>
    </figure>
</div>
</center>

!!! faq correction "Correction"
    1. Le spectre présenté est constitué d'une raie lumineuse sur un fond noir, il s'agit donc d'un spectre de raie d'émission.<br>
    2. 
    <center>
        <div class="grand">
            <figure>
                <img src="../images_exercices/diagrammesemission.png" />
            </figure>
        </div>
    </center>
    De plus, la longueur d'onde du photo émis est d'environ, $λ=520nm$, d'après le spectre fournit sur le sujet.<br>
    On cherche l'énergie du photo, noté $ξ_{photon}$<br>
    <center>$ξ_{photon}= \frac{h×c}{λ}$</center>
    **Application numérique :**<br>
    $ξ_{photon}= \frac{6,63.10^{-34} J.s × 3,00.10⁸ m.s^{-1}}{520.10^{-9} m} =3,83.10^{-19} J$<br>
    Conversions :<br>
    $ξ_{photon}=\frac{3,83.10^{-19}J}{1,60.10^{-19}J.eV^{-1}}= 2,39eV$<br>
    Dans le diagramme énergétique, on sait également que :
    $ξ_{photon}=ξ_{final}-ξ_{initial}= 2,39eV$
  

