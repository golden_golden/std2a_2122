---
title : "Personnalisation"
---

# Un peu de personnalisation

## 1. Utilité de cette page

Le but de cette page est de vous permettre de visualiser les couleurs disponibles pour le site afin de choisir celle qui sera utilisée au cours de cette année.

*Ps : Le but réel est pour moi de tester les QCM et une ou deux choses au niveau de la programmation de page web.*



## 2. Voter pour une couleur

Clique sur le bouton pour changer la couleur de la page et vous faire un avis :

<div class="mdx-switch" align="center">
    <button data-md-color-primary="red"><code>red</code></button>
    <button data-md-color-primary="pink"><code>pink</code></button>
    <button data-md-color-primary="purple"><code>purple</code></button>
    <button data-md-color-primary="deep-purple"><code>deep purple</code></button>
    <button data-md-color-primary="indigo"><code>indigo</code></button>
    <button data-md-color-primary="blue"><code>blue</code></button>
    <button data-md-color-primary="light-blue"><code>light blue</code></button>
    <button data-md-color-primary="cyan"><code>cyan</code></button> 
    <br>
    <button data-md-color-primary="teal"><code>teal</code></button>
    <button data-md-color-primary="green"><code>green</code></button>
    <button data-md-color-primary="light-green"><code>light green</code></button>
    <button data-md-color-primary="lime"><code>lime</code></button>
    <button data-md-color-primary="yellow"><code>yellow</code></button>
    <button data-md-color-primary="amber"><code>amber</code></button>
    <button data-md-color-primary="orange"><code>orange</code></button>
    <br>
    <button data-md-color-primary="deep-orange"><code>deep orange</code></button>
    <button data-md-color-primary="brown"><code>brown</code></button>
    <button data-md-color-primary="grey"><code>grey</code></button>
    <button data-md-color-primary="blue-grey"><code>blue grey</code></button>
    <button data-md-color-primary="black"><code>black</code></button>
    <button data-md-color-primary="white"><code>white</code></button>
</div>

 
<script>
  var buttons = document.querySelectorAll("button[data-md-color-primary]")
  buttons.forEach(function(button) {
    button.addEventListener("click", function() {
      var attr = this.getAttribute("data-md-color-primary")
      document.body.setAttribute("data-md-color-primary", attr)
      var name = document.querySelector("#__code_2 code span:nth-child(7)")
      name.textContent = attr.replace("-", " ")
    })
  })
</script>

<br>
Pour faire valoir votre choix, il vous suffit de voter :

<center>[**C’est par ici !**](https://bossuet.mon-ent-occitanie.fr/classes/classe-1std2a/personnalisation-du-site--70854.htm?URL_BLOG_FILTRE=%235344){:target="_blank"}</center>

## 2. Pour plus de personnalisation
### 2.1 Le nom du site
Si vous le souhaitez, il vous est possible de faire une proposition :
[**ici**](https://mensuel.framapad.org/p/nomdusite_std2a-9pnp?lang=fr){:target="_blank"}

D’ici une semaine ou deux, s’il y a eu des propositions, on fera un petit sondage pour le choix définitif !

### 2.2 Le logo
S’il y a des motivés, vous pouvez déposer vos propositions de logo : 
[**ici**](https://drive.google.com/drive/folders/13nxWxcuH9GXCl7fyaoIPxDfHPVu6MEsp?usp=sharing){:target="_blank"}


***Attention, il y a des contraintes :***

- *Une image en SVG*
- *Utiliser un format carré, sinon il y aura des déformations !*
- *Le logo est très petit dans le header, ce qui est une grosse contrainte lors de la réalisation !*
- *Le logo doit être en lien avec le programme de cette année et la discipline.*
- *Les couleurs, s’il y en a doivent être en accord avec la couleur choisie par la classe à la suite de vote ci-dessus.*

