﻿# Chapitre 1 : Généralités sur les matériaux


## 1. Familles de matériaux
Les matériaux sont généralement répartis en 4 classes (ou familles) différentes.


### 1.1 Les matériaux organiques
Ce sont des matériaux d'origine naturelle (animale ou végétale) ou synthétique. Tous ces matériaux ont un point commun : ils sont constitués d'atomes de carbone.

<br>

#### 1.1.1 Matériau organiques naturels
##### a. Le bois
Ses caractéristiques varient en fonction de l’espèce d'arbre dont il est issu. Il peut être :

- très tendre : *peuplier, bouleau…*
- très dur : *chêne, acacia…*

Il sont facile à usiner, en revanche, il est difficile à façonner par déformation.

<br>

##### b. les dérivés du bois

**Les agglomérés** : IIs sont constitués de copeaux de bois collés à chaud sous presse.

**Les lamellés** : Ils sont constitués de couches de bois en lamelles collés à chaud sous presse.

**Les cartons et papiers** : lls sont constitués de fibres de bois et de cellulose. lls sont essentiellement utilisés dans l’emballage.

<br>

##### c. Les fibres naturelles

Elles sont utilisées pour tisser des toiles et des tissus.
La laine est fournie par les moutons, lamas, chèvres … La soie est extraite des cocons du ver à soie. Le coton provient des gousses des graines de coton et le lin des tiges d’une plante du même nom.

<br>

##### d. Le cuir

C'est de la peau animale tannée (généralement de la peau de grands mammifères tels le À bœuf et le porc), c'est-à-dire une substance morte, imputrescible, souple et insoluble dans l’eau. On en fait des accessoires de maroquinerie, vêtements, chaussures.….

<br>


#### 1.1.2 Matériaux organiques synthétiques : les matières plastiques

Ces matières ont été créées en transformant, par des réactions chimiques, des produits naturels tels que le charbon, le pétrole ou le bois. Certaines peuvent remplacer l’acier eu égard à leurs caractéristiques.

<br>

##### a. Les thermoplastiques

Parfois appelés "thermoformables", ils ont des propriétés très diverses. Leur point commun est qu'ils se moulent facilement et peuvent être refondus et réutilisés. IIs se déforment sous l'effet de la chaleur.

<br>

##### b. Les thermodurcissalbes

Ils sont moulés une fois pour toute. IIs sont durs et rigides. IIs ne peuvent plus être déformés.

<br>


##### c. Les caoutchoucs et les élastomères

Le caoutchouc *(littéralement "arbre qui pleure")* est obtenu à partir d latex qui est la sève blanche sécrétée par un arbre :l'hévéa. C'est donc un plastique à base "naturelle”. C'est un élastomère : il a la propriété de reprendre sa forme après avoir été déformé. On en fabrique des élastiques, les chambres à air de véhicules…. Aujourd'hui on fabrique des élastomères synthétiques aux propriété très proches de celle du caoutchouc, pour un coup moindre.

<br>

### 1.2 Les matériaux métalliques
#### 1.2.1 Les métaux naturels

Les métaux sont des éléments naturels, On les trouve dans le sol, le plus souvent sous forme de minerai (roche riche en métal), parfois sous forme pure (pépite). En incorporant à un métal un ou plusieurs autres métaux, où des éléments non métalliques, on forme des alliages.

<br>

#### 1.2.2 Les alliages de fer et de carbone

##### a. L'acier
Il est obtenu en incorporant moins de 2% de carbone dans du fer. Il peut être alié à de
nombreux autres métaux comme le chrome et/ou le nickel, qui le transforment en acier
inoxydable. 

*Exemple : la visserie, les boite de conserve…*

<br>

##### b. La fonte
Elle est obtenue en incorporant plus de 2% de carbone dans le fer et est par conséquent plus lourde que l'acier. En fusion, il se moule facilement. 

*Exemple : le mobilier urbain comme les fontaines ou les bancs, les radiateurs…*

<br>

##### c. L'aluminium

L'aluminium est un métal naturel de couleur grise blanchâtre. Il présente l'avantage de résister à la corrosion et d'être très léger, il se moule facilement et se travaille sans dificulté. 


*Exemple : la feuille de "papier alu”, le canettes de soda…*


**Le Zamac** *(aliage aluminium-zinc)* : Très utilisé dans la fabrication de pièces de décoration où pour le modélisme.

**Le Duralumin** *(aliage aluminium, cuivre et magnésium)* : Il est très rigide (d'où son nom). Il est utisé notamment pour fabriquer des pièces d'horlogerie.


<br>


##### d. Le cuivre

Métal de couleur rouge, qui conduit particulièrement bien la chaleur. Très utilisé, notamment pour les circuits éléctriques ou la cuisine *(casseroles)*.

<br>

##### e. Le laiton *(aliage cuivre-zinc)*
Il se moule très facilement et donne des pièces très précises, comme les robinets ou les instruments de musiques. 

<br>

##### f. Le bronze *(aliage cuivr-étain)*
Initialement de couleur jaune, il se patine avec le temps et est utlisé pour la sculpture où ils fabrication des cloches.

Pour les plus curieux, consulter le lien suivant traitant du cuivre et de ses allages : [*ici*](https://www.futura-sciences.com/planete/dossiers/geologie-cuivre-premier-metal-travaille-homme-779/)

<br> 


##### g. Le zinc
Sa principale utiisation et dans la galvanisation des acier : on dépose une mince couche de
zinc en surface de l'acier pour le protéger de la corrosion. Il est utilisé notamment pour les revêtements des gouttières métaliques ou les paraboles.

<br>


##### h. L'étain
Il est utiisé principalement pour le revêtement de tôles fines d'acer, pour létamage (traitement de
surface qui consiste à appliquer une couche d'étain sur une pièce métalique). C'est un des
constituants des boîtes de conserve, fabriquées en “fer blanc" (acier + couche d'étan).

<br>


### 1.3 Les matériaux minéraux
Ils ne contiennent pas d'atomes de carbone. Ce sont des roches, des céramiques où des verres.

<br>


#### 1.3.1 Les céramiques
À l'origine fabriquées à partr d’argile (poteries), ce sont aussi des matériaux obtenus à partir
de terre où de sable cuit. Elles comprennent la faïence (carelage, vissele, le grès (vaisselle)
et la porcelaine (isolant, vaisselle).

Les céramiques sont dures, résistantes à la chaleur mais fragiles. Aujourd'hui les céramiques
techniques ont des qualités exceptionnelles et entrent dans la composition d’un grand nombre
d’objets (tables de cuisson, réacteurs d'avions, revêtements spéciaux…).


<br>

#### 1.3.2 Les verres
Les verres sont des matériaux très anciens élaborés à partir d sable. Obtenus par fusion de la
silice, on peut leur ajouter des additifs afin de modifier leurs caractéristiques (exemple : l'ajout
de plomb permet d'obtenir le crital) et modifier aussi leurs couleurs (par ajout d'oxydes métalliques).

Le verre est un matériau très dur mais très fragile. Il est utlisé dans la fabrication de bouteilles
et de vitres, mais également de “laine” de verre (par fusion, on obtient alors un matériau isolant
de consistance laineuse).
<br>

### 1.4 Les matériaux innovants

#### 1.4.1 Les matériaux composites
Par définition, ce sont des matériaux constitués d'au moins deux matériaux non miscibles
mais ayant une forte capacté d'adhésion. Le nouveau matériau à des propriétés mécaniques que les matériaux le constituant n'ont pas. Il sont le plus souvent couteux à fabriquer, car de haute technologie (exemple : le kevlar, les fibres de carbone). Il existe cependant des composites
naturels comme l'os.

<br>

#### 1.4.2 Les autres matériaux d'avenir
Les cristaux liquides, les nanomatériaux, les agro-matériaux ou les textles inteligents sont le plus en plus utilisés…

<br>



<!----
## 2. Propriétés physiques des matériaux



Il existe une multitude de propriétés physiques caractéristiques (ou non) des matériaux. L'objectif du chapitre n'est pas d'en dresser une liste interminable, mais de savoir quelles propriétés peuvent orienter le choix vers un matériau plutôt qu'un autre.

### 2.1 Masse volumique

### 2.2 Élasticité

### 2.3 Dureté 

### 2.4 Compressibilité

### 2.5 Conductivité éléctrique

### 2.6 Conductivité thermique

### 2.7 Propriété magnétique

### 2.8 Aptitude au formage

### 2.9 Température de fusion



## 3. Choix raisonné de matériaux

---->