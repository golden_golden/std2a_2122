---
title: Bienvenue
description: Ce site comporte les cours de Physique chimie pour le première STD2A.
---

# Bienvenue
 
Ce site présente les cours de Physique chimie pour le niveau première STD2A. Il a été conçu pour les élèves du lycée Bossuet à Condom.

Si vous êtes élèves, ce site vous permettra : de mettre votre classeur à jour, rattraper les cours que vous auriez manqués, vous exercer, avoir accès aux contenus multimédias diffusées en classe, consulté les sujets et les corrigés des DM et même pour les plus motivés à avoir accès à des contenues supplémentaires …


<center>
<a href="https://golden_golden.gitlab.io/std2a_2122/">
<img src="assets_extra/images/qr_code.svg" alt="Qr code du site" style="width:150px;"/>
</a>
</center>
